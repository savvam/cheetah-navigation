/**
 * @file Planner.cpp
 * @brief ROS wrapper for planning (RRT)
 * @author Savva Morozov <savva@mit.edu>
 * @date 20 Jan 2021
 */


#include "cheetah_navigation/Planner.h"

namespace cheetah {

Planner::Planner(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, grid_map::GridMap * global_map, tf2_ros::Buffer *tf_buffer)
: nh_(nh), nhp_(nhp), status_(status), global_map_(global_map), tf_buffer_(tf_buffer)
{

  // grab the parameters
  initParams();

  pub_increment_req_ = nh_.advertise<std_msgs::Empty>( "increment_req", 1, true);
  sub_increment_req_ = nh_.subscribe("/cheetah/increment_req", 1, &Planner::incrementCommittedTrajectoryCallback, this);
  sub_commit_maintenance_req_ = nh_.subscribe("/cheetah/commit_maintenance_req", 1, &Planner::commitMaintenanceCallback, this);

  user_says_go_sub_ = nh_.subscribe("/cheetah/planner_user_go", 1, &Planner::userGoCallback, this);

  // start the subscribers, publishers, timers
  pub_trajectory_ = nh_.advertise<nav_msgs::Path>( "trajectory", 1, true);
  pub_improved_trajectory_ = nh_.advertise<nav_msgs::Path>( "improved_trajectory", 1, true);

  pub_traj_increment_ = nh_.advertise<nav_msgs::Path>( "trajectory_increment", 1, true);
  pub_left_increment_ = nh_.advertise<nav_msgs::Path>( "left_increment", 1, true);
  pub_right_increment_ = nh_.advertise<nav_msgs::Path>( "right_increment", 1, true);

  pub_cmd_ = nh_.advertise<geometry_msgs::Vector3Stamped>( "vel_cmd", 5, true);
  pub_event_ = nh_.advertise<cheetah_msgs::event>( "events", 1, true);
 
  pub_committed_trajectory_ = nh_.advertise<nav_msgs::Path>( "committed_trajectory", 1, true);
  pub_left_tube_ = nh_.advertise<nav_msgs::Path>( "left_tube", 1, true);
  pub_right_tube_ = nh_.advertise<nav_msgs::Path>( "right_tube", 1, true);
  goal_sub_ = nh_.subscribe("/move_base_simple/goal", 1, &Planner::updateGoalPosition, this);
  jump_callback_sub_ = nh_.subscribe("/cheetah/lcm/jump_callback", 1, &Planner::callbackFromJump, this);
  jump_computed_sub_ = nh_.subscribe("/cheetah/lcm/jump_computed_callback", 1, &Planner::callbackFromJumpComputed, this);


  // logging
  pub_time_deltas_ = nh_.advertise<cheetah_msgs::deltaTime>( "delta_time", 1, true);
  pub_cp_ = nh_.advertise<cheetah_msgs::constrainedPolygon>( "con_poly", 1, true);
  pub_cfp_ = nh_.advertise<cheetah_msgs::constrainedFeasiblePolygon>( "con_feas_poly", 1, true);
  pub_robot_pose_ = nh_.advertise<cheetah_msgs::robotPose>( "robot_pose_jumps", 1, true);
  pub_map_ = nh_.advertise<cheetah_msgs::map>("grid_map_from_planner", 1, true);
  pub_trajs_ = nh_.advertise<cheetah_msgs::robotXYTrajectory>("trajs", 1, true);



  goal_pub_ = nh_.advertise<visualization_msgs::Marker>("goal_location", 1);
  debug_marker_pub_ = nh_.advertise<visualization_msgs::Marker>("debug_marker", 1);

  goal_ = grid_map::Position(0.0, 0.0);

  // make it so that we can store numNodes nodes
  trajectory_.reserve(50);
  leftTube_.reserve(50); rightTube_.reserve(50);
  trajLines_.reserve(50); leftLines_.reserve(50); rightLines_.reserve(50); 

  // I am using nodes_ as a usual array, the only feature of a vector used is that I reserve at init
  nodes_.reserve(p_.numNodes);
  // init the nodes
  Node::map_ = global_map_;
  Node::p_ = &p_;

  tree1_.reserve(p_.numNodes);
  tree2_.reserve(p_.numNodes);
  for(int i = 0; i < p_.numNodes; i++){
    nodes_[i] = Node();
    tree1_[i] = Node();tree1_[i].ind_ = i; tree1_[i].pathTypeToFather_ = 'x';
    tree2_[i] = Node();tree2_[i].ind_ = i; tree2_[i].pathTypeToFather_ = 'x';
  }

  srv_trajFollowing_ = nh_.advertiseService("follow", &Planner::follower, this);
  srv_floatArrSrv_ = nh_.advertiseService("floatArrSrv", &Planner::floatArrService, this);

  srv_get_terrain_ = nh_.advertiseService("terrain", &Planner::getTerrain, this);

  followerTimer_ = nh_.createTimer(ros::Duration( p_.followerPeriod ), std::bind(&Planner::trajectoryFollowing, this));
  publishGoalMarker();

  nhp_.param<float>("max_v_delta", max_v_delta_, 0.1);
  nhp_.param<float>("max_yaw_delta", max_yaw_delta_, 0.1);
  LCM_comm::zeroOut();
  LCM_comm::zeroOut();


  grabSVMData();
  ROS_INFO("SVM TABLE ACQUIRED");

  std::cout.setf(std::ios::fixed,std::ios::floatfield);
  std::cout.precision(4);

}

void Planner::publishRobotTraj(std::vector<Eigen::Vector2d> traj, std::string name){
  cheetah_msgs::robotXYTrajectory msg;
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = name;
  std_msgs::Float32MultiArray x_array; x_array.data.clear();
  std_msgs::Float32MultiArray y_array; y_array.data.clear();
  for (int i = 0; i < traj.size(); i++){
    x_array.data.push_back( traj[i](0) );
    y_array.data.push_back( traj[i](1) );
  }
  msg.x_traj = x_array;
  msg.y_traj = y_array;
  pub_trajs_.publish(msg);
}

void Planner::publishEvent(std::string name, int num){
  if (!p_.quiet)
    std::cout << "publishing event.\n";
  cheetah_msgs::event eventMsg;
  eventMsg.header.stamp = ros::Time::now();
  eventMsg.header.frame_id = name;
  eventMsg.event_num = num;
  pub_event_.publish(eventMsg);
}

void Planner::publishTheMap(){
  cheetah_msgs::map aMap;
  aMap.header.stamp = ros::Time::now();
  std_msgs::Float32MultiArray x, y, height, cluster;
  x.data.clear(); y.data.clear(); height.data.clear(); cluster.data.clear();

  Eigen::Vector2d pos;
  for (grid_map::GridMapIterator iterator(*global_map_); !iterator.isPastEnd(); ++iterator) {
    // valid height
    if ( global_map_->isValid( *iterator, "elevation")){
      global_map_->getPosition((*iterator), pos);
      x.data.push_back( pos(0) + global_map_->getResolution()/2 );
      y.data.push_back( pos(1) + global_map_->getResolution()/2 );
      height.data.push_back( global_map_->at("elevation", *iterator) );
      cluster.data.push_back( global_map_->at("cluster_tag", *iterator) );
    }
  }
  aMap.x = x;
  aMap.y = y;
  aMap.height = height;
  aMap.cluster = cluster;
  pub_map_.publish(aMap);
}

void Planner::publishDeltaTime(std::string name, float deltaTimeSecs){
  cheetah_msgs::deltaTime dtMsg;
  dtMsg.header.stamp = ros::Time::now();
  dtMsg.header.frame_id = name;
  dtMsg.deltaTimeSecs = deltaTimeSecs;
  pub_time_deltas_.publish(dtMsg);
}

void Planner::publishRobotPose(std::string what, Eigen::Vector2d pos, float yaw){
  cheetah_msgs::robotPose rpMsg;
  rpMsg.header.stamp = ros::Time::now();
  rpMsg.header.frame_id = "world";
  rpMsg.x = pos(0);
  rpMsg.y = pos(1);
  rpMsg.yaw = yaw;
  rpMsg.what = what;
  pub_robot_pose_.publish(rpMsg);
}



// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// trajectory generation



bool Planner::sampleAnRRTConnectTrajectory(const grid_map::Position origin, std::vector<grid_map::Position> * trajectory, std::vector<char> * types, float * trajCost){
  // NOTE: 
  // types stores node->pathTypeToFather.
  // with that in mind, the way we use types in incrementCommittedTrajectory is opposit:
  // it's pathTypeToSon there. Since the last node in the commited trajectory has no son,
  // we arbitrarily set its walkability to w, walking.



  // assumptions: 
  //    -   nodes are already reset
  //    -   smbd already checked that origin is not in the vicinity of the goal
  //    -   trajectory is a pointer to an empty std vector

  const ros::Time methodStartTime(ros::Time::now());

  // origin in the origin for tree1
  // goal_ is the origin for tree2
  tree1_[0].setAsOrigin(origin);
  tree2_[0].setAsOrigin(goal_);
  bool firstTree = false;

  if (!tree1_[0].checkIfAcceptable() || !tree2_[0].checkIfAcceptable() ){
    ROS_ERROR("------------------------------------------------------------");
    ROS_ERROR("INITIAL POSITION OR GOAL ARE NOT IN TRAVERSABLE SPACE - CAN'T RUN RRT CONNECT");
    ROS_ERROR("------------------------------------------------------------");
    return false;
  }

  // indexers for both
  int i1 = 1;
  int i2 = 1;
  int tempI = 1;

  std::vector<Node> * tree1 = & tree2_;
  std::vector<Node> * tree2 = & tree1_;

  while ( i1 < p_.numNodes-1 && i2 < p_.numNodes-1 ){
    
    if ( (ros::Time::now() - methodStartTime).toSec() > p_.singleTrajTimeout ){
      ROS_ERROR("RRT CONNECT HAS BEEN RUNNING FOR MORE THAN %F SECONDS, EXITTING", p_.singleTrajTimeout);
      resetTreesUpToK(i1, i2);
      return false;
    }
    if (!p_.quiet && !p_.quiet_rrt){
      std::cout << "-------------------\n";
      debugBothTrees(firstTree,i1,i2);
    }
    int counter = 0;
    // swap trees
    tempI = i1; i1 = i2; i2 = tempI;
    if (!firstTree){
      tree1 = &tree1_; tree2 = &tree2_;
    }
    else {
      tree1 = &tree2_; tree2 = &tree1_;
    }
    firstTree = !firstTree;


    // sample until 
    while (counter < p_.maxSampleAttempts){
      // -----------------------------------------
      // FIRST: sample randomly
      (*tree1)[i1].sampleRandomPoint();

      // -----------------------------------------
      // SECOND: find the nearest neighbor
      int nbr = 0;
      // std::cout << "nn\n";
      // TO DO: is removing too close a good idea? i think so, bearing in mind that i am shortcutting.
      // bool too_close = 
      findNearestNeighbor(tree1, & (*tree1)[i1], i1, & nbr );
      
      // sample a too close node
      // if (too_close){ counter += 1; continue; }

      float min_dist = (*tree1)[i1].distanceTo( & (*tree1)[nbr] );

      // -----------------------------------------
      // THIRD: steer along the direciton but no longer than thee steering distance
      if (min_dist > p_.steerDistance)
        (*tree1)[i1].steer( &(*tree1)[nbr], p_.steerDistance );

      // -----------------------------------------
      // FOURTH: check if acceptable (i.e. point itself doesn't break constraints / is in free space)
      // TO DO: acceptability checks that are considerate of nan space?
      // std::cout << "acceptable\n";
      if ( (*tree1)[i1].checkIfAcceptable() ){ 
      
        // -----------------------------------------
        // FIFTH: check traversability
        TrajectorySegment segment = TrajectorySegment();
        // std::cout << "\t\tcall 1 to checkTraversabilityFrom; counter " << counter << "\n";
        bool traversability = (*tree1)[i1].checkTraversabilityFrom( &(*tree1)[nbr], &segment );

        if ( traversability ){ // perfect, the span is traversable! 
          if ( segment.isJump() ){ 
            // std::cout << "making a jump\n";
            (*tree1)[i1+1].makeAJump( &(*tree1)[nbr], &(*tree1)[i1], &segment);
            i1+=1;
            // std::cout << "done\n";
          }
          else
            (*tree1)[i1].makeAWalk( &(*tree1)[nbr] );

          // SIXTH: do the RRT connect
          bool firstNode = true;
          while(i2 < p_.numNodes-1){
            bool reached = false;
            
            // find nearest neighbor from the other tree
            
            (*tree2)[i2].copyPosFrom(&(*tree1)[i1]); // i2 is already acceptable
            
            int other_nbr = 0;

            // neighbor from 2nd tree; i should just pass a point to the array

            if (firstNode){
              findNearestNeighbor( tree2, & (*tree2)[i2], i2, &other_nbr );
              firstNode = false;
            }
            else{
              other_nbr = i2-1;
              int tempOtherNbr = 0;
              findNearestNeighbor( tree2, & (*tree2)[i2], i2, &tempOtherNbr );
              if (tempOtherNbr != other_nbr && !p_.quiet && !p_.quiet_rrt )
                ROS_WARN("oi, closest neighbor isn't the previously added node, that's odd. closest is %i while -1 is %i.", tempOtherNbr, other_nbr);
            }
            
            float dist_to_nbr = (*tree2)[i2].distanceTo( & (*tree2)[other_nbr] );

            // std::cout << "steer for connect\n";
            // initialize a node point in the other tree
            if (dist_to_nbr > p_.steerDistance)
              (*tree2)[i2].steer( &(*tree2)[other_nbr], p_.steerDistance );
            else
              reached = true;
            
            // check traversability
            // std::cout << "trav for connect\n";
            TrajectorySegment other_segment = TrajectorySegment();
            traversability = (*tree2)[i2].checkTraversabilityFrom( &(*tree2)[other_nbr], &other_segment );
            if (!traversability){
              (*tree2)[i2].reset();
              if (!p_.quiet && !p_.quiet_rrt)
                std::cout << " CONNECT breaking due to collision\n";
              break; // that was a collision, stop the connect function
            }
            else{
              if (!p_.quiet && !p_.quiet_rrt)
                std::cout << "adding a node through rrt connect\n";
              // traversable, let's see which one
              // TO DO: JUMP and forward / backward checking. 
              if ( other_segment.isJump() ){ 
                // std::cout << "making a connect jump\n";
                (*tree2)[i2+1].makeAJump( &(*tree2)[other_nbr], &(*tree2)[i2], &other_segment);
                i2+=1;
                // std::cout << "done connect jump\n";
              }
              else
                (*tree2)[i2].makeAWalk( &(*tree2)[other_nbr] );

              // done: the two trees reached each other, i2 = i1, let's exit
              if (reached){
                if (!p_.quiet && !p_.quiet_rrt){
                  std::cout << "we reached!\n";
                  debugBothTrees(firstTree,i1+1,i2+1);
                }

                if (!firstTree){
                  tempI = i1; i1 = i2; i2 = tempI;
                }
                // path chunk 1
                std::vector<grid_map::Position> path1, path2; path1.reserve(25);path2.reserve(25);
                std::vector<char> types1, types2; types1.reserve(25); types2.reserve(25);
                types1.push_back('w'); // arbitrarily set pathTypeToSon of final node to Walk
                types2.push_back('w'); // arbitrarily set pathTypeToSon of final node to Walk
                float cost1, cost2;
                cost1 = populateVectorTrajectory( &tree1_[i1], &path1, &types1 );
                cost2 = populateVectorTrajectory( &tree2_[i2], &path2, &types2 );
                // *trajCost = populateVectorTrajectory( &nodes_[nbr], trajectory, types );
                // BOTH PATHS ARE REVERSED RIGHT NOW
                // AND THE RESULTANT PATH NEEDS TO REMAIN REVERSD
                types->push_back('w'); // arbitrarily set pathTypeToSon of final node to Walk
                for (int i=path2.size()-1; i>0; i-- ){
                  trajectory->push_back( path2[i] );
                  types->push_back( types2[i] );
                }
                trajectory->push_back( path2[0] );
                for (int i=1;i<path1.size();i++){
                  trajectory->push_back( path1[i] );
                  types->push_back( types1[i] );
                }
                if (!p_.quiet && !p_.quiet_rrt){
                  // return the path
                  std::cout << "-------------------\n";
                  std::cout << "PRINTING TRAJECTORY in reverse order\n";
                  *trajCost = cost1+cost2;
                  for (int i = 0; i < trajectory->size(); i++ )
                    std::cout << (*types)[i] << " ";
                  std::cout << "\n";
                  for (int i = 0; i < trajectory->size(); i++ )
                    std::cout << (*trajectory)[i].transpose() << " ";
                  std::cout << "\n";
                  std::cout << "-------------------\n";
                  ROS_INFO("FOUND A TRAJECTORY! - sampled i1 %i and i2 %i nodes", i1, i2);
                }
                resetTreesUpToK(i1, i2);
                shortcut(trajectory, types);

                return true;
              }
              i2+=1;
            }
          }
          // ok that's it we're done here
          i1 += 1;
          break; // get out of the while loop
        }
      }
      counter += 1;
    }
    if (counter == p_.maxSampleAttempts){
      if (!p_.quiet && !p_.quiet_rrt)
        ROS_WARN("BREAK DUE TO COUNTER - can't sample a new node");
      break;
    }
  }
  if (i1 == p_.numNodes-1 || i2 == p_.numNodes-1 && !p_.quiet && !p_.quiet_rrt)
    ROS_WARN("BREAK because FINISHED - sampled i1 %i and i2 %i nodes", i1, i2);
  // BEAR IN MIND: if we break, the last point is unused

  // -----------------------------------------
  // find the nearest neighbor to the goal
  if (!firstTree){
    tempI = i1; i1 = i2; i2 = tempI;
  }
  int nbr = 0; // index of the neighbor
  float min_dist = tree1_[0].distanceTo( goal_ );

  for(int j = 1; j < i1; j++){
    if ( tree1_[j].pathTypeToFather_!='x' ){
      float temp = tree1_[j].distanceTo( goal_ );
      if ( temp < min_dist ){
        min_dist = temp;
        nbr = j;
      }
    }
    else{
      
      ROS_WARN("PATH TYPE TO FATHER IN TREE1 IS X, NOT SUPPOSED TO HAPPEN");
    }
  }
  if (!p_.quiet && !p_.quiet_rrt)
    ROS_INFO("Couldn't get to the goal, but we got the next best thing!");
  // TO DO: clean this up  

  types->push_back('w'); // arbitrarily set pathTypeToSon of final node to Walk
  if (!p_.quiet && !p_.quiet_rrt){
    std::cout << "-------------------\n";
    std::cout << "PRINTING TRAJECTORY in reverse order\n";
    *trajCost = populateVectorTrajectory( &tree1_[nbr], trajectory, types );
    std::cout << "\n";

    for (int i = 0; i < trajectory->size(); i++ ){
      std::cout << (*types)[i] << " ";
    }
    std::cout << "\n";
    std::cout << "-------------------\n";
  }
  
  if (!p_.quiet && !p_.quiet_rrt)
    ROS_INFO("RRT Planner took %f seconds to sample %i nodes.", (ros::Time::now() - methodStartTime).toSec(), i1+i2);
  resetTreesUpToK(i1,i2);
  return false;
}

void Planner::shortcut(std::vector<grid_map::Position> * trajectory, std::vector<char> * types){
  // remember, this trajectory is backwards
  if (!p_.quiet && !p_.quiet_rrt)
    std::cout << "shortcutting trajectory; original length is " << trajectory->size() << "\t";
  if (trajectory->size()<3){
    if (!p_.quiet && !p_.quiet_rrt)
      std::cout << "trajectory too short, nothing to shortcut\n";
    return;
  }
  int i = trajectory->size()-3;
  while ( i >= 0){
    int j = i+1;
    // TO DO: does directionality matter?
    // std::cout << "entering while loop" << i << " " << j << "\n";
    if((*types)[i+1]!='j'){
      while ( j+1 < trajectory->size() && Node::checkWalkabilityBtwn( (*trajectory)[i], (*trajectory)[j+1] ) ){
        i+=1;
        // std::cout << "erasing; " << i << " " << j << "\n";
        trajectory->erase(trajectory->begin()+j);
        types->erase(types->begin()+j);
        // std::cout << "erased " << i << " " << j << "\n";
        // std::cout << "traj size is now " << trajectory->size() << "\n";
        j+=1;
      }
    }
    i-=1;
    // std::cout << "switched to next " << i << " " << j << "\n";
  }
  if (!p_.quiet && !p_.quiet_rrt)
    std::cout << "final length is " << trajectory->size() << "\n";
}

void Planner::debugBothTrees(bool firstTree, int i1, int i2){
  if (firstTree){
    std::cout << "at i1 " << i1 << " and i2 " << i2 << "\n";
    for(int y = 0; y < std::max(i1,i2); y++)
      std::cout << y%10 << " ";
    std::cout << "\n";

    for(int y = 0; y < i1; y++)
      std::cout << tree1_[y].pathTypeToFather_ << " ";
    std::cout << "\n";
    std::cout << "- ";
    for(int y = 1; y < i1; y++)
      std::cout << tree1_[y].father_->ind_ << " ";
    std::cout << "\n";
    for(int y = 0; y < i1; y++)
      std::cout << int(global_map_->at("cluster_tag", tree1_[y].index_)) << " ";
    std::cout << "\n";
    std::cout << "\n";

    for(int y = 0; y < i2; y++)
      std::cout << tree2_[y].pathTypeToFather_ << " ";
    std::cout << "\n";
    std::cout << "- ";
    for(int y = 1; y < i2; y++)
      std::cout << tree2_[y].father_->ind_ << " ";
    std::cout << "\n";
    for(int y = 0; y < i2; y++)
      std::cout << int(global_map_->at("cluster_tag", tree2_[y].index_)) << " ";
    std::cout << "\n";
    if (tree1_[i1].pathTypeToFather_!='x')
      ROS_WARN("ODD: i1 of tree1 should be x, not %c", tree1_[i1].pathTypeToFather_);
    if (tree2_[i2].pathTypeToFather_!='x')
      ROS_WARN("ODD: i2 of tree2 should be x, not %c", tree2_[i2].pathTypeToFather_);
  }
  else{
    std::cout << "at i1 " << i2 << " and i2 " << i1 << "\n";
    for(int y = 0; y < std::max(i1,i2); y++)
      std::cout << y%10 << " ";
    std::cout << "\n";

    for(int y = 0; y < i2; y++)
      std::cout << tree1_[y].pathTypeToFather_ << " ";
    std::cout << "\n";
    std::cout << "- ";
    for(int y = 1; y < i2; y++)
      std::cout << tree1_[y].father_->ind_ << " ";
    std::cout << "\n";
    for(int y = 0; y < i2; y++)
      std::cout << int(global_map_->at("cluster_tag", tree1_[y].index_)) << " ";
    std::cout << "\n";
    std::cout << "\n";
    
    for(int y = 0; y < i1; y++)
      std::cout << tree2_[y].pathTypeToFather_ << " ";
    std::cout << "\n";
    std::cout << "- ";
    for(int y = 1; y < i1; y++)
      std::cout << tree2_[y].father_->ind_ << " ";
    std::cout << "\n";
    for(int y = 0; y < i1; y++)
      std::cout << int(global_map_->at("cluster_tag", tree2_[y].index_)) << " ";
    std::cout << "\n";
    if (tree1_[i2].pathTypeToFather_!='x')
      ROS_WARN("ODD: i1 of tree1 should be x, not %c", tree1_[i2].pathTypeToFather_);
    if (tree2_[i1].pathTypeToFather_!='x')
      ROS_WARN("ODD: i2 of tree2 should be x, not %c", tree2_[i1].pathTypeToFather_);
  }
  
}

bool Planner::findNearestNeighbor( std::vector<Node> * tree, Node * node, int upToMaxIndex, int * nnIndex ){
  bool too_close = false;
  float min_dist = node->distanceTo( & (*tree)[0] );
  int nbr = 0; // index of the neighbor

  for(int j = 1; j < upToMaxIndex; j++){
    float temp = node->distanceTo( & (*tree)[j] );

    if ( temp < min_dist ){
      min_dist = temp;
      nbr = j;

      // this point was sampled too close
      // the idea is that i don't wanna sample points too close to each other - this leads to very short edges
      // short edges are bad for adjusting trajectories + inefficient for growing trees
      if (min_dist < p_.minEdgeLength){
        too_close = true;
        break;
      }
    }  
  }
  *nnIndex = nbr;
  return too_close;
}

void Planner::incrementCommittedTrajectoryCallback( std_msgs::Empty msg){
  incrementCommittedTrajectory(false);
}

bool Planner::incrementCommittedTrajectory( bool firstTime ){
  // assumptions: 
  //    -   trajectory_ stores the trajectory we are actually interested in (?)
  //    -   committed_ is the index of the node we are currently commited up to

  // TO DO: 
  //    -   decide wethere to change a trajectory or not based on safety / speed cost metric ?
  //    -   send a callback to run the gausian filter on the map?

  // Potential inputs:
  
  //------------------------------------------------------
  // ZEROTH: set up data containers, perform error checking
  ROS_INFO("-------------------------------------------");
  ROS_INFO("\t\t\tRECEIVED INCREMENT REQUEST");

  publishEvent("traj_req_received", 0);
  finished_traj_ = false;
  const ros::Time incrementStartTime(ros::Time::now());
  // TO DO: send a callback request to get gradients HERE

  //          ERROR handling
  if (Node::distanceBtwn( global_map_->getPosition(), goal_) < p_.goalRadius){
    if (firstTime)
      ROS_ERROR("BAD BEHAVIOR: TRAJECTORY INCREMENT REQ although AT the GOAL FIRST TIME");
    else
      ROS_ERROR("BAD BEHAVIOR: TRAJECTORY INCREMENT REQ although AT the GOAL during TRAJ");
    incrementRequestSent_ = false;
    return false;
  }

  // temporary containers
  float trajCost = -1;
  bool trajAcquired = false;
  std::vector<grid_map::Position> traj; traj.reserve(25);
  std::vector<char> types; types.reserve(25);

  // containers for the best trajectory
  float bestCost = -1;
  bool bestAcquired = false;
  std::vector<grid_map::Position> bestTraj; bestTraj.reserve(25);
  std::vector<char> bestTypes; bestTypes.reserve(25);


  //------------------------------------------------------
  // FIRST: get numRRTattempts RRT trajectories, pick the best one out of the bunch. 
  // trajectory is returned (picked) the moment a node close to the goal is acquired.

  for (int i = 0; i < p_.numRRTattempts; i++) {
    // TO DO JUMP: sample AnRRT trajectory should return not just nodes
    if (firstTime)
      trajAcquired = sampleAnRRTConnectTrajectory( global_map_->getPosition(), & traj, & types, & trajCost );
    else
      trajAcquired = sampleAnRRTConnectTrajectory( trajectory_[committed_], & traj, & types, & trajCost );

    // acquired trajectory is not to the goal but it is a trajectory 
    // we don't have a proper trajectory yet; and whatever we got here is better than what we got now
    if (!trajAcquired && trajCost > 0 && !bestAcquired && (bestCost < 0 || trajCost < bestCost) ){
      bestTraj = traj; bestCost = trajCost; bestTypes = types;
    } // case 2: best hasn't been acquired yet - we want it
    else if ( trajAcquired && !bestAcquired ){
      bestTraj = traj; bestCost = trajCost; bestAcquired = trajAcquired; bestTypes = types;
    } // case 3: best has been acquired but whatever we got now is actually better
    else if ( trajAcquired && bestAcquired && (trajCost < bestCost) ) {
      bestTraj = traj; bestCost = trajCost; bestTypes = types;
    }
    // ckean up for the next round
    trajCost = -1;
    traj.clear();
    types.clear();
  }
  if (!p_.quiet)
    ROS_INFO("finished searching for trajectories");
  if (!trajAcquired){
    ROS_ERROR("COULDN'GET A TRAJECTORY, TRY A DIFFERENT GOAL POSITION");
    return false;
  }


  // we will use traj container in the end - i need to reverse the order
  for (int i = bestTraj.size()-1; i >=0; i-- ){
    traj.push_back( bestTraj[i] );
    types.push_back( bestTypes[i] );
  }

  if (!p_.quiet){
    for (int i = 0; i < traj.size(); i++ ){
      std::cout << bestTypes[i] << "\t";
    }
    std::cout << "\n";

    for (int i = 0; i < traj.size(); i++ ){
      std::cout << types[i] << "._.";
    }
    std::cout << "\n";
    ROS_INFO("finished pushing the trajectory into traj");

    //               ERROR CHECKING
    // the nodes now go forwards, first node should match the committed node
    if ( (!firstTime && Node::distanceBtwn( traj[0], trajectory_[committed_] ) > 0.01 ) ||
          (firstTime &&  Node::distanceBtwn( traj[0], global_map_->getPosition() ) > 0.01 )  ) {
      ROS_ERROR("FIRST NODE IN TRAJ IS NOT THE COMMITTED WTF");
      if (firstTime)
        std::cout << "committed (init pos)\t" << (global_map_->getPosition()).transpose() <<"\n";
      else
        std::cout << "committed\t" << trajectory_[committed_].transpose() <<"\n";
      for (int i = 0; i < traj.size(); i++ )
        std::cout << traj[i] << "\t";
      std::cout << "\n";
      ROS_ERROR("FIRST NODE IN TRAJ IS NOT THE COMMITTED WTF");
    }
  }



  


  //------------------------------------------------------
  // SECOND: insert more nodes into this trajectory
  // it's an RRT trajectory with shortcutting, there are really long nodes, we wanna cut them up

  // inserting more nodes into the trajectory
  std::vector<grid_map::Position> initialTrajectory = traj;

  // this should be ok with 2 node trajectoires in theory
  // addExtraVectorsIntoTrajectory(&traj, &types);

  if (!p_.quiet)
    ROS_INFO("finished adding extra vertices into trajectory");


  //------------------------------------------------------
  // THIRD: improve the trajectory
  // use the gradient descent method to move the nodes further away from the obstacles

  // if (bestTraj.size() > 2){
  //   // these depend on large trajectoris

  //   // get gradients - this should be a callback earlier
  //   getGradients();
  //   if (!p_.quiet)
  //     ROS_INFO("finished getting gradients");

  //   improveTrajectoryOnce(p_.p1, &traj, &types); 
  //   improveTrajectoryOnce(p_.p2, &traj, &types);
  //   improveTrajectoryOnce(p_.p3, &traj, &types);
  //   improveTrajectoryOnce(p_.p4, &traj, &types);
  //   improveTrajectoryOnce(p_.p5, &traj, &types);
  //   if (!p_.quiet)
  //     ROS_INFO("finished improving trajectory");
  // }


  //------------------------------------------------------
  // FOURTH: put the safety tube around the trajectory
  // std::vector<grid_map::Position> leftTube; leftTube.reserve(traj.size());
  // std::vector<grid_map::Position> rightTube; rightTube.reserve(traj.size());
  // // assumption: we are incementing, not building from start
  // // TO DO JUMP: how does safety tube around jump sections look?
  // if (firstTime)
  //   findTubeAroundTrajectory( &traj, &types, &leftTube, &rightTube, false );
  // else
  //   findTubeAroundTrajectory( &traj, &types, &leftTube, &rightTube, true );

  // if (!p_.quiet)
  //   ROS_INFO("finished making the tube");
  int num_jumps = 0;
  for (int i = 0; i < types.size(); i++)
    if (types[i] == 'j')
      num_jumps += 1;

  // if (!p_.quiet && p_.verboseTrajectory){
  //   ROS_INFO("-----------------");
  //   ROS_INFO("NEW TRAJECTORY PIECE: ");
  //   for (int i = 0; i < traj.size(); i++){
  //     std::cout << "Node #" << i << " \t next edge is a " << types[i] << " \t node is " << traj[i].transpose() << "\n";
  //   }
  //   ROS_INFO("-----------------");
  //   if (num_jumps == 0)
  //     ROS_WARN("there are NO JUMPS in trajectory");
  //   else
  //     ROS_WARN("there are %i JUMPS in trajectory", num_jumps);

  //   ROS_INFO("-----------------");
  // }
  
  //------------------------------------------------------
  // FIFTH: insert the acquired trajectory into the committed trajectory
  // TO DO:   
  //    -   should this be done selectively, i.e. check if acquired traj is better than existing?
  if (firstTime)
    committed_ = 0;


  trajectoryAccess_.lock();
  if (!firstTime){
    // delete the elements past committed and including committed
    trajectory_.erase( trajectory_.begin() + committed_, trajectory_.end() );
    nextEdgeType_.erase( nextEdgeType_.begin() + committed_, nextEdgeType_.end() );
    leftTube_.erase( leftTube_.begin() + committed_, leftTube_.end() );
    rightTube_.erase( rightTube_.begin() + committed_, rightTube_.end() );
  }
  else{
    trajectory_.clear();
    nextEdgeType_.clear();
    leftTube_.clear();
    rightTube_.clear();
  }

  for (int i = 0; i < traj.size(); i++){
    trajectory_.push_back(traj[i]);
    nextEdgeType_.push_back(types[i]);
    // leftTube_.push_back(leftTube[i]);
    // rightTube_.push_back(rightTube[i]);
  }

  trajectoryLength_ = trajectory_.size();
  if (!p_.quiet)
    ROS_INFO("finished populating the proper trajectory");

  // getTubeLines();

  //------------------------------------------------------
  // SIXTH: change the committed point to a point ahead

  // set committed to a point some distance away
  float dist = 0;
  int newCommit = committed_;
  while ( newCommit < trajectory_.size()-1 ){
    dist = dist + Node::distanceBtwn( trajectory_[newCommit], trajectory_[newCommit+1] );
    newCommit += 1;
    if ( dist > p_.commitIncrement )
      break;
  }
  // in singleTrajectory mode we commit until the very end
  if (p_.singleTrajectoryMode)
    committed_ = trajectory_.size()-1;

  committed_ = newCommit;
  if (!p_.quiet){
    ROS_INFO("------------");
    for (int i = 0; i < nextEdgeType_.size(); i++)
      std::cout << nextEdgeType_[i] << " ";
    std::cout << "\n";
  }


  Eigen::Vector2d bestJumps;
  float distPastObst;
  if (num_jumps != 0){
    if ( findBestJump( &bestJumps, &distPastObst ) ){
      // it's a teporary solution but it will do for now
      // in reality, i need a much better data structure that describes trajectories
      jumpAngles_ = bestJumps;
      distPastObst_ = distPastObst;
    }
    else{
      trajectory_.clear(); nextEdgeType_.clear();
      trajectoryLength_ = trajectory_.size();
      committed_ = trajectory_.size()-1;
      leftTube_.clear(); rightTube_.clear();
      // zero everything out
      trajLines_.clear(); leftLines_.clear(); rightLines_.clear();
    }
  }

  trajectoryAccess_.unlock();

  const ros::Time incrementEndTime(ros::Time::now());

  // publishing
  {
    publishEvent("pub_traj", 1);
    publishRobotTraj(trajectory_, "des_traj");
    publishRobotTraj(leftTube_, "left_tube");
    publishRobotTraj(rightTube_, "right_tube");
    publishTheMap();

    publishDeltaTime( "trajFound", (incrementEndTime-incrementStartTime).toSec() );

    publishTrajectory(&pub_improved_trajectory_, &trajectory_, trajectoryLength_);
    publishTrajectory(&pub_left_tube_, &leftTube_, trajectoryLength_);
    publishTrajectory(&pub_right_tube_, &rightTube_, trajectoryLength_);
    publishGoalMarker();
  }

  
  ROS_INFO("\t\t\tINCREMENT TOOK %f s", (incrementEndTime-incrementStartTime).toSec() );
  ROS_INFO("-------------------------------------------");
  incrementRequestSent_ = false;
  return true;
}

void Planner::trajectoryFollowingTest(float walkDistance, float des_orientation){
  auto [poseAcquired, pos, yaw] = getRobotPose();
  if (!poseAcquired){
    ROS_ERROR("COULDN'T GET THE STATE TRANSFORM!");
    return;
  }
  p_.sendJumpExtendedRequest = false;

  Eigen::Vector2d walkParl(1,0); walkParl = Geometry::rotate(walkParl, yaw);
  Eigen::Vector2d walkPerp = Geometry::rotate(walkParl, M_PI/2); 

  trajectoryAccess_.lock();

  trajectory_.clear();
  leftTube_.clear();
  rightTube_.clear();
  nextEdgeType_.clear();

  trajectory_.push_back( pos );
  trajectory_.push_back( pos + walkParl * walkDistance - walkPerp*0.15 );
  trajectory_.push_back( pos + walkParl * walkDistance + walkParl * 1.0 - walkPerp*0.15 );
  trajectory_.push_back( pos + walkParl * walkDistance + walkParl * 2.0 - walkPerp*0.15 );
  goal_ = pos + walkParl * walkDistance + walkParl * 2.0;

  leftTube_.push_back( pos + walkPerp*0.4);
  leftTube_.push_back( pos + walkParl * walkDistance + walkPerp*0.4 - walkPerp*0.15 );
  leftTube_.push_back( pos + walkParl * walkDistance + walkParl * 1.0 + walkPerp*0.4- walkPerp*0.15 );
  leftTube_.push_back( pos + walkParl * walkDistance + walkParl * 2.0 + walkPerp*0.4- walkPerp*0.15 );

  rightTube_.push_back( pos - walkPerp*0.4);
  rightTube_.push_back( pos + walkParl * walkDistance - walkPerp*0.4- walkPerp*0.15 );
  rightTube_.push_back( pos + walkParl * walkDistance + walkParl * 1.0 - walkPerp*0.4- walkPerp*0.15 );
  rightTube_.push_back( pos + walkParl * walkDistance + walkParl * 2.0 - walkPerp*0.4- walkPerp*0.15 );
  
  nextEdgeType_.push_back('w');
  nextEdgeType_.push_back('j');
  nextEdgeType_.push_back('w');
  nextEdgeType_.push_back('w');

  trajectoryLength_ = trajectory_.size();
  committed_ = trajectory_.size()-1;

  trajLines_.clear();
  leftLines_.clear();
  rightLines_.clear();
  for(int i = 0; i < trajectoryLength_-1; i++){
    trajLines_[i] = Geometry::lineThroughTwoPoints( trajectory_[i], trajectory_[i+1] );
    leftLines_[i] = Geometry::lineThroughTwoPoints( leftTube_[i], leftTube_[i+1] );
    rightLines_[i] = Geometry::lineThroughTwoPoints( rightTube_[i], rightTube_[i+1] );
    float trajLen = 0.0;
    for(int i = 1; i < trajectory_.size(); i++){
      trajLen += Geometry::distBtwn( trajectory_[i-1], trajectory_[i] );
    }
    ROS_WARN("--------------------------------");
    ROS_WARN("TRAJECTORY LENGTH IS %f", trajLen);
    ROS_WARN("--------------------------------");
  }

  jumpAngles_ = Eigen::Vector2d( Geometry::getProperAngle(yaw+des_orientation), Geometry::getProperAngle(yaw+des_orientation) );
  distPastObst_ = 0.5;

  trajectoryAccess_.unlock();

  publishTrajectory(&pub_improved_trajectory_, &trajectory_, trajectoryLength_);
  publishTrajectory(&pub_left_tube_, &leftTube_, trajectoryLength_);
  publishTrajectory(&pub_right_tube_, &rightTube_, trajectoryLength_);
  publishGoalMarker();
  publishDebugMarker( pos + walkParl * walkDistance );

}

void Planner::commitMaintenanceCallback( std_msgs::Empty msg ){
  if (!p_.singleTrajectoryMode)
    commitMaintenance();
  // else
  //   ROS_WARN("SKIPPING COMMIT MAINTENANCE REQUEST");
}

void Planner::commitMaintenance(){
  if (p_.followerModeOn){
    ROS_INFO("RECEIVED TRAJECTORY MAINTENANCE REQUEST");

    // get our current location
    auto [poseAcquired, pos, yaw] = getRobotPose();
    if (!poseAcquired){
      ROS_ERROR("PLANNER COULDN'T GET THE STATE TRANSFORM!");
      return;
    }
    
    // geometry_msgs::TransformStamped odom_to_base;
    // try{
    //   odom_to_base = tf_buffer_->lookupTransform("odom", "base_link", ros::Time(0) ); // ros::Time(0): get most recent
    // }
    // catch (tf2::TransformException &ex) {
    //   // either something is properly wrong, or it's one of the first few measurements
    //   ROS_ERROR("PLANNER COULDN'T GET THE STATE TRANSFORM:\n%s",ex.what());
    //   return;
    // }
    // Eigen::Vector2d pos( odom_to_base.transform.translation.x, odom_to_base.transform.translation.y );


    trajectoryAccess_.lock();
    int segment = 0; // trajectory segment we are in
    if ( ! Geometry::isRight( leftTube_[0], rightTube_[0], pos )  ){
      // we are not before the 0th segment (the zeroth mid-line)
      while( segment + 1 < trajectoryLength_ && Geometry::isLeft( leftTube_[segment+1], rightTube_[segment+1], pos ) ){
        // find the segment we are in
        segment++;
      }
    }
    // if we are on the last node - there's nothing left to do
    if ( segment == trajectoryLength_-1) {
      trajectoryAccess_.unlock();
      return;
    }
    int initialSegment = segment;

    // found the segment; let's check traversability of each consequent node until committed
    while (segment < committed_){ 
      if ( nextEdgeType_[segment] == 'w' && Node::checkWalkabilityBtwn(trajectory_[segment], trajectory_[segment+1]) )
        segment += 1;
      else if ( nextEdgeType_[segment] == 'j' && Node::checkJumpabilityBtwn(trajectory_[segment], trajectory_[segment+1]) )
        segment += 1;
      else
        break;
    }
    // all the node are fine, trajectory is still good
    if (segment == committed_){
      trajectoryAccess_.unlock();
      return;
    }
    else{
      ROS_WARN("TRAJECTORY MAINTENANCE NECESSARY, SENDING INCREMENT REQUEST");
      // found an error - send a request to fix the trajectory
      if(initialSegment == segment){
        // replan from segment, stop the robot
        p_.followerModeOn = false;
        LCM_comm::sendVelCMD(0,0,0);
      }
      else if ( initialSegment >= segment - 2 && segment > 0 ){
        segment -= 1;
        // replan from segment -1, stop the robot
        p_.followerModeOn = false;
        LCM_comm::sendVelCMD(0,0,0);
      }
      else{
        segment -=1;
        //replan from segment, but don't stop the robot
      }
      committed_ = segment;
      trajectoryAccess_.unlock();
      if ( incrementCommittedTrajectory(false) ){
        p_.followerModeOn = true;
      }
      else{
        ROS_ERROR("ROBOT STOPPED BECAUSE CAN'T REPLAN");
      }
    }
  }
}

// ----------------------------------------------------------------------------
// jump handling
bool Planner::findBestJump( Eigen::Vector2d * resAngles, float * distPastObst ){
  // asuming single trajectory mode for now, not sure of it's necessary though; 
  // we aren't doing any multy stage plannign for the paper anyway though
  if ( p_.singleTrajectoryMode ){
    const ros::Time jumpAllowanceStartTime(ros::Time::now());




    // --------------------------------------------------------------------------------------
    // PART 1: find the jump edge
    // assuming that the trajectory ONLY has one jump edge
    int jumpIndex = -1;
    for (int i = 0; i < nextEdgeType_.size(); i++ ){
      if (nextEdgeType_[i] == 'j'){
        if (jumpIndex != -1) {
          ROS_ERROR("MORE THAN ONE JUMP EDGE IN THE TRAJECTORY");
          return false;
        }
        jumpIndex = i;
      }
    }
    if ( jumpIndex == 0 ){
      ROS_ERROR("FIRST EDGE IS JUMP - THAT'S WRONG");
      return false;
    }
    // get the jump vertices
    Eigen::Vector2d jumpStart = trajectory_[jumpIndex];
    Eigen::Vector2d jumpFinish = trajectory_[jumpIndex+1];

    // parl and perp
    float originalJumpDist = (jumpFinish-jumpStart).norm();
    grid_map::Position parl = (jumpFinish - jumpStart) /  originalJumpDist ;
    grid_map::Position perp = Geometry::rotate(parl, M_PI/2);

    // get the angles: angle of the direction of the jump, angle of the previous edge, angle of the next edge
    float jumpDirAngle = Geometry::angleToVector( parl ) ;
    float prevDirAngle, nextDirAngle;
    prevDirAngle = Geometry::angleToVector( trajectory_[jumpIndex]-trajectory_[jumpIndex-1] );
    if ( jumpIndex+2 == trajectory_.size() )
      nextDirAngle = jumpDirAngle;
    else
      nextDirAngle = Geometry::angleToVector( trajectory_[jumpIndex+2]-trajectory_[jumpIndex+1] );





    // --------------------------------------------------------------------------------------
    // PART 2: find the border point
    grid_map::Position boundaryPoint;

    // find the first non-cluster point going in parl direction from the jump start
    grid_map::Index tempInd;
    if (! global_map_->getIndex(jumpStart, tempInd) ){
      ROS_ERROR("JUMP START CAN'T GET INDEX?");
      return false;
    }
    float cluster = global_map_->at("cluster_tag", tempInd);
    grid_map::Position temp = jumpStart;
    // can be indexed, valid elevation, and belongs to same cluster - increment
    while ( global_map_->getIndex(temp, tempInd) && global_map_->isValid( tempInd, "elevation" ) && Node::belongsToCluster( tempInd, cluster ))
      temp += parl * global_map_->getResolution();
    if ( (temp-jumpStart).norm() > originalJumpDist ){ // throw an error if distance too high
      ROS_ERROR("BORDER POINT ACQUISITION ERROR 1");
      return false;
    }
    boundaryPoint = temp/2;

    // find the first non-cluster point going in -parl direction from the jump finish
    if (! global_map_->getIndex(jumpFinish, tempInd) ){
      ROS_ERROR("JUMP FINISH CAN'T GET INDEX?");
      return false;
    }
    cluster = global_map_->at("cluster_tag", tempInd);
    temp = jumpFinish;
    // can be indexed, valid elevation, and belongs to same cluster - increment
    while ( global_map_->getIndex(temp, tempInd) && global_map_->isValid( tempInd, "elevation" ) && Node::belongsToCluster( tempInd, cluster ))
      temp -= parl * global_map_->getResolution();
    if ( (temp-jumpFinish).norm() > originalJumpDist ){ // throw an error if distance too high
      ROS_ERROR("BORDER POINT ACQUISITION ERROR 2");
      return false;
    }
    boundaryPoint += temp/2;
    // boundary point acquired - publish a debug marker just to see it
    publishDebugMarker( boundaryPoint );
    // print some stuff
    if (!p_.quiet)
      std::cout << "debugging\n" << "jumpStart\t" << jumpStart.transpose() << "\tboundaryPoint\t" << boundaryPoint.transpose() << "\tjumpFinish\t" << jumpFinish.transpose() << "\n";


    publishRobotPose("jump_boundary", boundaryPoint, jumpDirAngle);

 
    // -------------------------------------------
    // PART 3: setting up lists
    float angleResolution = 2*p_.angleRange / (p_.numAngles-1);

    // float initialAngles[p_.numAngles];
    // float finalAngles[p_.numAngles];
    std::vector<float> initialAngles;
    std::vector<float> finalAngles;

    // bool acceptedInitial[p_.numAngles];
    // bool acceptedFinal[p_.numAngles];
    std::vector<bool> acceptedInitial;
    std::vector<bool> acceptedFinal;
 
    // Eigen::Vector2d initialRanges[p_.numAngles];
    // Eigen::Vector2d finalRanges[p_.numAngles];
    std::vector<Eigen::Vector2d> initialRanges;
    std::vector<Eigen::Vector2d> finalRanges;

    // final and initial angles are a range of angles, all biased by jumpDirAngle
    // note that when searching for these angles in the table, we will need to subtract jumpDirAngle
    for (int i=0; i < p_.numAngles; i++){
      // all the initial and final angles shall be proper
      initialAngles.push_back(Geometry::getProperAngle(-p_.angleRange + angleResolution * i + jumpDirAngle));
      finalAngles.push_back(initialAngles[i]);

      acceptedInitial.push_back(false);
      acceptedFinal.push_back(false);
      
      initialRanges.push_back(Eigen::Vector2d(-1,-1));
      finalRanges.push_back(Eigen::Vector2d(-1,-1));

    }




    // -------------------------------------------
    // PART 4: parse through each angle through each distance

    // number of points to be checked
    int numDistPnts = int((p_.maxDistFromObstacle-p_.minDistToObstacle)/p_.rangeDistResolution)+1;

    // INITIAL FIRST
    // these are acceptable initial locations
    bool acceptableInitLoc[ numDistPnts ];
    // create a list of points to see if locations are even acceptable
    for(int i=0; i < numDistPnts; i++)
      acceptableInitLoc[i] = Node::checkIfAcceptable( boundaryPoint - (p_.minDistToObstacle+p_.rangeDistResolution*i)*parl );

    // parse angles, for each angle - find appropriate ranges

    // for each angle
    float angle, startRange, endRange;
    Eigen::Vector2d pos;
    std::vector< Eigen::Vector2i > angleSets; angleSets.reserve(3); int angleSetInited = -1;

    // go through all initial distances and initial angles, determine which of them are feasible
    for( int angleIndex = 0; angleIndex < p_.numAngles; angleIndex++ ){
      // find the first acceptable distance
      angle = initialAngles[angleIndex];

      // if angle is not sufficiently close to the previous edge angle:
      if ( Geometry::properAngleDiff( prevDirAngle, angle ) > p_.angleToEdges  ){
        if (!p_.quiet)
          std::cout << "removed angle " << angle << " because it was too far from prevDirAngle " << prevDirAngle << "\n";
        // that angle doesn't work, it's too far from the edge angle
        acceptedInitial[angleIndex] = false;
        initialRanges[angleIndex] = Eigen::Vector2d(0,0);

        if (angleSetInited >= 0){
          // angle set was inited, but now it's broken - add an angle set
          angleSets.push_back( Eigen::Vector2i( angleSetInited, angleIndex-1 ) );
          angleSetInited = -1;
        }

        continue; // move on to the next point
      }

      int i = -1;
      while ( true ){
        i += 1;
        if (i == numDistPnts)
          break;
        startRange = p_.minDistToObstacle+p_.rangeDistResolution*i;
        pos = boundaryPoint - startRange * parl;
        if (! acceptableInitLoc[i] || ! Node::robotIsFreeOfObstaclesAt( pos, angle ) )
          continue;
        else 
          break; // found the first acceptable point where a robot can stand
      }
      if (i == numDistPnts){ // we got to the very end
        // hence this angle has no acceptable configurations
        // std::cout << "angle " << angle << " has nothing at initial\n";
        acceptedInitial[angleIndex] = false;
        initialRanges[angleIndex] = Eigen::Vector2d(0,0);

        if (angleSetInited >= 0){
          // angle set was inited, but now it's broken - add an angle set
          angleSets.push_back( Eigen::Vector2i( angleSetInited, angleIndex-1 ) );
          angleSetInited = -1;
        }

        continue; // move on to the next point
      }
      // we are not at the very end yet! we found the start, let's find the end
      startRange = p_.minDistToObstacle+p_.rangeDistResolution*i;//-0.001;

      // angle set was not inited, but now it was started - init the angle set
      if (angleSetInited < 0)
        angleSetInited = angleIndex;

      // now we should be in the range of acceptable distances; find the first unacceptable one
      while (i < numDistPnts){
        i += 1;
        endRange = p_.minDistToObstacle+p_.rangeDistResolution*i;
        pos = boundaryPoint - endRange * parl;
        if ( acceptableInitLoc[i] && Node::robotIsFreeOfObstaclesAt( pos, angle ) )
          continue;
        else
          break;
      }
      i -= 1;
      endRange = p_.minDistToObstacle+p_.rangeDistResolution*i;//+0.001;
      
      acceptedInitial[angleIndex] = true;
      initialRanges[angleIndex] = Eigen::Vector2d(startRange, endRange);
    }
    if (angleSetInited >= 0){
      // angle set was inited, but now it's broken - add an angle set
      angleSets.push_back( Eigen::Vector2i( angleSetInited, p_.numAngles-1 ) );
      angleSetInited = -1;
    }

    // ensuring we have a single angleset
    {
      if ( angleSets.size() == 0 ){
        // there are no acceptable initial angles - our efforts are fruitless
        // we are done finding the feasibl angles
        if (!p_.quiet){
          std::cout << "ACCEPTABLE INITIAL LOC\n";
          for(int i=0; i<numDistPnts; i++)
            std::cout << acceptableInitLoc[i] << "\t";
          std::cout << "\n";
          ROS_ERROR("----------------------------------------------------------------------------");
          ROS_ERROR( "NO POINT SATISFIES INITIAL CONSTRAINTS" );
          ROS_ERROR("----------------------------------------------------------------------------");
        }
        return false;
      }
      else if ( angleSets.size() >= 2  ){
        if (!p_.quiet){
          ROS_INFO("----------------------------------------------------------------------------");
          ROS_INFO("WE HAVE MULTIPLE INITIAL ANGLE SETS");
        }
        // we have multiple initial angle sets - gotta fix that
        int bestAngleSet = -1;
        float bestAngleDiff = 1000.0;
        for (int i = 0; i < angleSets.size(); i++){
          if (!p_.quiet)
            ROS_INFO("\tANGLE SET #%i IS from %f to %f", i, initialAngles[ angleSets[i](0) ]-jumpDirAngle, initialAngles[angleSets[i](1)]-jumpDirAngle );
          int newAngle = 0.5*(initialAngles[angleSets[i](0)] + initialAngles[angleSets[i](1)]);
          if ( Geometry::properAngleDiff( prevDirAngle, newAngle ) < bestAngleDiff ){
            bestAngleDiff = Geometry::properAngleDiff( prevDirAngle, newAngle );
            bestAngleSet = i;
          }
        }
        if (!p_.quiet){
          ROS_INFO("\tBEST ANGLE SET IS %i, from %f to %f", bestAngleSet, initialAngles[angleSets[bestAngleSet](0)]-jumpDirAngle, initialAngles[angleSets[bestAngleSet](1)]-jumpDirAngle );
          // ROS_INFO("ZEROING OUT THE REST");
          ROS_INFO("----------------------------------------------------------------------------");
        }
        // we found our favorite angle set
        // zero out all other angle ranges / sets
        if (false){
          for (int i = 0; i < angleSets.size(); i++){
            if (i != bestAngleSet){
              for (int j = angleSets[i](0); j <= angleSets[i](1); j++ ){
                acceptedInitial[j] = false;
                initialRanges[j] = Eigen::Vector2d(0.0,0.0);
              }
            }
          }
        }
      }
      else{
        if (!p_.quiet){
          ROS_INFO("----------------------------------------------------------------------------");
          ROS_INFO("WE HAVE A SINGLE INITIAL ANGLE SETS");
          ROS_INFO("\tBEST ANGLE SET IS %i, from %f to %f", 0, initialAngles[angleSets[0](0)]-jumpDirAngle, initialAngles[angleSets[0](1)]-jumpDirAngle );
          ROS_INFO("----------------------------------------------------------------------------");
        }
      }
    }

    // AND NOW THE FINAL location 
    bool acceptableFinalLoc[ numDistPnts ];
    // parse distances to see if locations are even acceptable
    angleSets.clear(); angleSetInited = -1;
    for(int i=0; i < numDistPnts; i++){
      if (p_.minDistToObstacle+p_.rangeDistResolution*i < p_.minDistFromObstacle)
        acceptableFinalLoc[i] = false;
      else
        acceptableFinalLoc[i] = Node::checkIfAcceptable( boundaryPoint + (p_.minDistToObstacle+p_.rangeDistResolution*i)*parl );
    }

    // go through all terminal distances and terminal angles, determine which of them are feasible
    for( int angleIndex = 0; angleIndex < p_.numAngles; angleIndex++ ){
      // find the first acceptable distance
      angle = finalAngles[angleIndex];

      // if angle is not sufficiently close to the previous edge angle:
      if ( Geometry::properAngleDiff( nextDirAngle, angle ) > p_.angleToEdges ){
        if (!p_.quiet)
          std::cout << "removed angle " << angle << " because it was too far from nextDirAngle " << nextDirAngle << "\n";
        // that angle doesn't work, it's too far from the edge angle
        acceptedFinal[angleIndex] = false;
        finalRanges[angleIndex] = Eigen::Vector2d(0,0);

        if (angleSetInited >= 0){
          // angle set was inited, but now it's broken - add an angle set
          angleSets.push_back( Eigen::Vector2i(angleSetInited, angleIndex-1) );
          angleSetInited = -1;
        }

        continue; // move on to the next point
      }


      int i = -1;
      while ( true ){
        i += 1;
        if (i == numDistPnts)
          break;
        startRange = p_.minDistToObstacle+p_.rangeDistResolution*i;
        pos = boundaryPoint + startRange * parl;
        if (startRange < p_.minDistFromObstacle)
          continue;
        if (! acceptableFinalLoc[i] || ! Node::robotIsFreeOfObstaclesAt( pos, angle ) )
          continue;
        else
          break;
      }
      if (i == numDistPnts){
        // this angle has not acceptable configurations
        acceptedFinal[angleIndex] = false;
        finalRanges[angleIndex] = Eigen::Vector2d(0,0);
        if (angleSetInited >= 0){
          // angle set was inited, but now it's broken - add an angle set
          angleSets.push_back( Eigen::Vector2i(angleSetInited, angleIndex-1) );
          angleSetInited = -1;
        }
        continue;
      }
      startRange = p_.minDistToObstacle+p_.rangeDistResolution*i; // - 0.001;

      // angle set was not inited, but now it was started - init the angle set
      if (angleSetInited < 0)
        angleSetInited = angleIndex;

      // find the second acceptable distance
      while (i < numDistPnts){
        i += 1;
        endRange = p_.minDistToObstacle+p_.rangeDistResolution*i;
        pos = boundaryPoint + endRange * parl;
        if ( acceptableFinalLoc[i] && Node::robotIsFreeOfObstaclesAt( pos, angle ) )
          continue;
        else
          break;
      }
      i -= 1;
      endRange = p_.minDistToObstacle+p_.rangeDistResolution*i; // + 0.001;

      acceptedFinal[angleIndex] = true;
      finalRanges[angleIndex] = Eigen::Vector2d(startRange, endRange);
    }
    if (angleSetInited >= 0){
      // angle set was inited, but now it's broken - add an angle set
      angleSets.push_back( Eigen::Vector2i( angleSetInited, p_.numAngles-1 ) );
      angleSetInited = -1;
    }

    // ensuring we have a single angleset for terminal
    {
      if ( angleSets.size() == 0 ){
        // there are no acceptable initial angles - our efforts are fruitless
        if (!p_.quiet){
          std::cout << "ACCEPTABLE FINAL LOC\n";
          for(int i=0; i<numDistPnts; i++)
            std::cout << acceptableFinalLoc[i] << "\t";
          std::cout << "\n";
          ROS_ERROR("----------------------------------------------------------------------------");
          ROS_ERROR( "NO POINT SATISFIES FINAL CONSTRAINTS" );
          ROS_ERROR("----------------------------------------------------------------------------");
        }
        return false;
      }
      else if ( angleSets.size() >= 2 ){
        if (!p_.quiet){
          ROS_INFO("----------------------------------------------------------------------------");
          ROS_INFO("WE HAVE MULTIPLE FINAL ANGLE SETS");
        }
        // we have multiple initial angle sets - gotta fix that
        int bestAngleSet = -1;
        float bestAngleDiff = 1000.0;
        for (int i = 0; i < angleSets.size(); i++){
          if (!p_.quiet)
            ROS_INFO("\tANGLE SET #%i IS from %f to %f", i, finalAngles[angleSets[i](0)]-jumpDirAngle, finalAngles[angleSets[i](1)]-jumpDirAngle );
          int newAngle = 0.5*(finalAngles[angleSets[i](0)] + finalAngles[angleSets[i](1)]);
          if ( Geometry::properAngleDiff( nextDirAngle, newAngle ) < bestAngleDiff ){
            bestAngleDiff = Geometry::properAngleDiff( nextDirAngle, newAngle );
            bestAngleSet = i;
          }
        }
        if (!p_.quiet){
          ROS_INFO("\tBEST ANGLE SET IS %i, from %f to %f", bestAngleSet, finalAngles[angleSets[bestAngleSet](0)]-jumpDirAngle, finalAngles[angleSets[bestAngleSet](1)]-jumpDirAngle );
          // ROS_INFO("ZEROING OUT THE REST");
          ROS_INFO("----------------------------------------------------------------------------");
        }
        // we found our favorite angle set
        // zero out all other angle ranges / sets
        if (false){
          for (int i = 0; i < angleSets.size(); i++){
            if (i != bestAngleSet){
              for (int j = angleSets[i](0); j <= angleSets[i](1); j++ ){
                acceptedFinal[j] = false;
                finalRanges[j] = Eigen::Vector2d(0.0,0.0);
              }
            }
          }
        }
      }
      else{
        if (!p_.quiet){
          ROS_INFO("----------------------------------------------------------------------------");
          ROS_INFO("WE HAVE A SINGLE FINAL ANGLE SETS");
          ROS_INFO("\tBEST ANGLE SET IS %i, from %f to %f", 0, finalAngles[angleSets[0](0)]-jumpDirAngle, finalAngles[angleSets[0](1)]-jumpDirAngle );
          ROS_INFO("----------------------------------------------------------------------------");
        }
      }
    }

    // logging the stuff

    // debugging
    if (!p_.quiet)
    {
      // we are done finding the feasibl angles
      std::cout << "ACCEPTABLE INITIAL LOC\n";
      for(int i=0; i<numDistPnts; i++)
        std::cout << acceptableInitLoc[i] << "\t";
      std::cout << "\n";

      std::cout << "ACCEPTABLE FINAL LOC\n";
      for(int i=0; i<numDistPnts; i++)
        std::cout << acceptableFinalLoc[i] << "\t";
      std::cout << "\n";

      // debugging:
      ROS_INFO("------------------------------------------------");
      ROS_INFO("JUMP RESULTS");
      ROS_INFO("------------------------------------------------");
      std::cout << "OFFSET ANGLE IS " << jumpDirAngle << "\n";
      ROS_INFO("INITIAL");
      std::cout << "ANGL ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << initialAngles[i]-jumpDirAngle << "\t";
        // std::cout << int(std::round(initialAngles[i]*180/M_PI)) << "\t";
      std::cout << "\n";
      std::cout << "YES  ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << acceptedInitial[i] << "   \t";
      std::cout << "\n";
      std::cout << "INIT ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << initialRanges[i](0) << "\t";
      std::cout << "\n";
      std::cout << "TERM ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << initialRanges[i](1) << "\t";
      std::cout << "\n\n";

      ROS_INFO("FINAL");
      std::cout << "ANGL ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << finalAngles[i]-jumpDirAngle << "\t";
        // std::cout << int(std::round(finalAngles[i]*180/M_PI)) << "\t";
      std::cout << "\n";
      std::cout << "YES  ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << acceptedFinal[i] << "   \t";
      std::cout << "\n";
      std::cout << "INIT ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << finalRanges[i](0) << "\t";
      std::cout << "\n";
      std::cout << "TERM ";
      for(int i=0; i<p_.numAngles; i++)
        std::cout << finalRanges[i](1) << "\t";
      std::cout << "\n\n";
      ROS_INFO("------------------------------------------------");
    }





    // -------------------------------------------------------------------
    // PART 5: get height
    float height = global_map_->atPosition("elevation", jumpFinish) - global_map_->atPosition("elevation", jumpStart);

    alglib::real_1d_array c; c.setlength(5);
    c(0) = height; c(1) = (boundaryPoint-jumpStart).norm(); c(2) = 0.0; c(3) = 0.0; c(4) = 0.0;

    if (! Node::evaluateTerrainAt(jumpStart, jumpDirAngle, c, &c, true, false) ){
      ROS_ERROR("COULDN'T EVALUATE TERRAIN - CAN'T GET HEIGHT");
      return false;
    }
      
    if (!p_.quiet)
      std::cout << "\t\tTERRAIN HEIGHT\t" << c(0) << "\n";
    height = c(0);





    // -------------------------------------------------------------------
    // PART 6: FIND THE BEST ANGLE
    if (!p_.quiet)
      ROS_INFO("----------------------------------------------------------------------------");

    float yawInit, distInit, yawTerm, distTerm;
    float yawInitBest, distInitBest, yawTermBest, distTermBest;
    mapbox::geometry::polygon<double> bestPolygon;
    

    float bestMetric = -1;

    // for each terminal angle
    for (int finalAngleIndex = 0; finalAngleIndex < p_.numAngles; finalAngleIndex++){
      // if that angle has not accepted distance - skip it
      if ( !acceptedFinal[finalAngleIndex] ){
        continue;
      }
        
      yawTerm = finalAngles[finalAngleIndex]; // actual angle

      for (int finalRangeIndex = 0;
        finalRangeIndex <= int(std::round((finalRanges[finalAngleIndex](1)-finalRanges[finalAngleIndex](0)) /p_.rangeDistResolution));
        finalRangeIndex++ ){
        
        distTerm = finalRanges[finalAngleIndex](0)+p_.rangeDistResolution*finalRangeIndex;

        mapbox::geometry::polygon<double> polygon;
        if ( ! findPolygon( jumpDirAngle, prevDirAngle, yawTerm, distTerm, height, acceptedInitial, initialAngles, initialRanges, & polygon )) {
          continue; 
        }
        // get the best point
        mapbox::geometry::point<double> newPoint = mapbox::polylabel(polygon, 0.01);
        distInit = newPoint.y;
        yawInit = (newPoint.x+jumpDirAngle) * p_.yawVSDistScalingFactor; 
        double newMetric = mapbox::detail::pointToPolygonDist(newPoint, polygon);
        float radYaw, radDist;
        radDist = newMetric;
        radYaw = newMetric * p_.yawVSDistScalingFactor;

        if (!p_.quiet){
          std::cout << "thisPoint = [" << distInit << "," << yawInit-jumpDirAngle << "," << radDist << "," << radYaw << "];\n";
          std::cout << "\ndistInit, yawInit, radDist, radYaw\n";
        }
        if (p_.log_inside) {

          cheetah_msgs::constrainedFeasiblePolygon cfpMsg;
          cfpMsg.header.stamp = ros::Time::now();
          cfpMsg.distTerm = distTerm;
          cfpMsg.yawTerm = yawTerm-jumpDirAngle;
          cfpMsg.jumpDirAngle = jumpDirAngle;
          cfpMsg.height = height;
          
          std_msgs::Float32MultiArray dist_array; dist_array.data.clear();
          std_msgs::Float32MultiArray yaw_array; yaw_array.data.clear();
          for (int i = 0; i < polygon[0].size(); i++ ){
            yaw_array.data.push_back( polygon[0][i].x );
            dist_array.data.push_back( polygon[0][i].y );
          }
          cfpMsg.polygon_dist = dist_array;
          cfpMsg.polygon_yaw = yaw_array;
          cfpMsg.robustYawInit = yawInit-jumpDirAngle;
          cfpMsg.robustDistInit = distInit;
          cfpMsg.metric = newMetric;
          cfpMsg.metricScalingFactor = p_.yawVSDistScalingFactor;
          cfpMsg.isBest = false;
          pub_cfp_.publish(cfpMsg);
        }

        // GREAT, we have now acquired a new point - optimal for given terminal conditions
        // find the best point overall
        if ( bestMetric < 0 ){
          if (!p_.quiet)
            std::cout << "THIS WAS THE BEST POINT\n";
          bestMetric = newMetric;
          distInitBest = distInit; yawInitBest = yawInit; distTermBest = distTerm; yawTermBest = yawTerm;
          bestPolygon = polygon;
        }
        else if ( newMetric < bestMetric - p_.robustnessMetricTieRad ){
          continue;
        }
        else if( newMetric < bestMetric + p_.robustnessMetricTieRad   ){
          // tie breaker
          // pointNow IS: termAngle, termRange, initAngle, initRange, height
          // yawInit += jumpDirAngle;
          // yawTerm += jumpDirAngle;
          float curr_tie_score = Geometry::properAngleDiff( yawInitBest, prevDirAngle ) + Geometry::properAngleDiff( yawTermBest, nextDirAngle );
          float proposed_tie_score = Geometry::properAngleDiff( yawInit, prevDirAngle ) + Geometry::properAngleDiff( yawTerm, nextDirAngle );
          if (!p_.quiet){
            std::cout << "CHECKING TIES:\t" << "prevDirAngle " << prevDirAngle << "\tnextDirAngle " << nextDirAngle << "\n";
            std::cout << "CURR BEST:\t" << "initAngle " << yawInitBest << "\tfinalAngle " << yawTermBest << "\tscore " << curr_tie_score <<"\n";
            std::cout << "PROP BEST:\t" << "initAngle " << yawInit << "\tfinalAngle " << yawTerm << "\tscore " << proposed_tie_score <<"\n";
          }
          if ( proposed_tie_score < curr_tie_score ){
            if (!p_.quiet)
              std::cout << "PROPOSED SCORE ACCEPTED! TIE UPDATED\n";
            bestMetric = newMetric;
            distInitBest = distInit; yawInitBest = yawInit; distTermBest = distTerm; yawTermBest = yawTerm;
            bestPolygon = polygon;
          }
        }
        else{
          if (!p_.quiet)
            std::cout << "THIS WAS THE BEST POINT\n";
          bestMetric = newMetric;
          distInitBest = distInit; yawInitBest = yawInit; distTermBest = distTerm; yawTermBest = yawTerm;
          bestPolygon = polygon;
        }
        if (!p_.quiet)
          std::cout << "\n\n";
      }
    }
    if (bestMetric < 0){
      ROS_ERROR("BEST METRIC IS NONE - COULDN'T FIND A SINGLE POLYGON");
      return false;
    }
    // fill in - as part of inputs
    * resAngles = Eigen::Vector2d( yawInitBest, yawTermBest );
    * distPastObst = distTermBest;
    distToObst_ = distInitBest;
    height_ = height;

    const ros::Time jumpAllowanceEndTime(ros::Time::now());

    ROS_INFO("----------------------------------------------------------------------------");
    ROS_INFO("WE FOUND A POINT MATE");
    ROS_INFO("ACTUAL ANGLES: init dist %f init yaw %f final dist %f final yaw %f metric %f", distInitBest, 180/M_PI*(yawInitBest), distTermBest, 180/M_PI*(yawTermBest), bestMetric );
    ROS_INFO("RELATIVE ANGLES: init dist %f init yaw %f final dist %f final yaw %f metric %f", distInitBest, 180/M_PI*(yawInitBest-jumpDirAngle), distTermBest, 180/M_PI*(yawTermBest-jumpDirAngle), bestMetric );
    ROS_INFO("----------------------------------------------------------------------------");

    if (p_.log_inside){
      cheetah_msgs::constrainedFeasiblePolygon cfpMsg;
      cfpMsg.header.stamp = ros::Time::now();
      cfpMsg.distTerm = distTermBest;
      cfpMsg.yawTerm = yawTermBest-jumpDirAngle;
      cfpMsg.jumpDirAngle = jumpDirAngle;
      cfpMsg.height = height;
      
      std_msgs::Float32MultiArray dist_array; dist_array.data.clear();
      std_msgs::Float32MultiArray yaw_array; yaw_array.data.clear();
      for (int i = 0; i < bestPolygon[0].size(); i++ ){
        yaw_array.data.push_back( bestPolygon[0][i].x );
        dist_array.data.push_back( bestPolygon[0][i].y );
      }
      cfpMsg.polygon_dist = dist_array;
      cfpMsg.polygon_yaw = yaw_array;

      cfpMsg.robustYawInit = yawInitBest-jumpDirAngle;
      cfpMsg.robustDistInit = distInitBest;
      cfpMsg.metric = bestMetric;
      cfpMsg.metricScalingFactor = p_.yawVSDistScalingFactor;
      cfpMsg.isBest = true;
      pub_cfp_.publish(cfpMsg);
    }

    if (p_.log_inside){
      publishConstraintPolygon( jumpDirAngle, true, acceptedInitial, initialAngles, initialRanges);
      publishConstraintPolygon( jumpDirAngle, false, acceptedFinal, finalAngles, initialRanges);
    }






    // PART 7: EDIT THE TRAJECTORY ACCORDING TO THE SOLUTION
    // move trajectory points from the border points ever so slightly
    if (!p_.quiet){
      ROS_INFO("\t\tCALCULATING BEST JUMP TOOK %f s", (jumpAllowanceEndTime-jumpAllowanceStartTime).toSec() );
      ROS_INFO( "MOVING TRAJECTORY POINTS" );
      ROS_INFO( "INIT POINT is %f m from the obstacle, TERMINAL POINT is %f m from the obstacle.", distInitBest*p_.distBackOffMultiplier, distTermBest*p_.distBackOffMultiplier );
      ROS_INFO( "It used to be %f m for init, %f m for terminal.", (trajectory_[jumpIndex]-boundaryPoint).norm(), (trajectory_[jumpIndex+1]-boundaryPoint).norm() );
      std::cout << "prev init point: " << trajectory_[jumpIndex].transpose() << "\tnew init point: ";
    }
    trajectory_[jumpIndex] = boundaryPoint - parl * distInitBest * p_.distBackOffMultiplier;
    if (!p_.quiet){
      std::cout << trajectory_[jumpIndex].transpose() << "\n";
      std::cout << "prev final point: " << trajectory_[jumpIndex+1].transpose() << "\tnew final point: ";
    }
    trajectory_[jumpIndex+1] = boundaryPoint + parl * distTermBest * p_.distBackOffMultiplier;
    if (!p_.quiet)
      std::cout << trajectory_[jumpIndex+1].transpose() << "\n";

    shortcut(&trajectory_, &nextEdgeType_);


    addExtraVectorsIntoTrajectory(&trajectory_, &nextEdgeType_);

    //------------------------------------------------------
    // THIRD: improve the trajectory
    // use the gradient descent method to move the nodes further away from the obstacles
    if (trajectory_.size() > 2){
      // these depend on large trajectoris
      // get gradients - this should be a callback earlier
      getGradients();

      improveTrajectoryOnce(p_.p1, &trajectory_, &nextEdgeType_); 
      improveTrajectoryOnce(p_.p2, &trajectory_, &nextEdgeType_);
      improveTrajectoryOnce(p_.p3, &trajectory_, &nextEdgeType_);
      improveTrajectoryOnce(p_.p4, &trajectory_, &nextEdgeType_);
      improveTrajectoryOnce(p_.p5, &trajectory_, &nextEdgeType_);
    }

    // adjusting the trajectory
    trajectoryLength_ = trajectory_.size();
    committed_ = trajectory_.size()-1;
    leftTube_.clear();
    rightTube_.clear();
    findTubeAroundTrajectory( &trajectory_, &nextEdgeType_, &leftTube_, &rightTube_, false );

    if (!p_.quiet){
      ROS_INFO("------------ TRAJECTORY IS");
      for (int i = 0; i < nextEdgeType_.size(); i++)
        std::cout << nextEdgeType_[i] << " ";
      std::cout << "\n";
      ROS_INFO("------------ TRAJECTORY IS");
    }

    const ros::Time findBestEnd(ros::Time::now());
    ROS_INFO("\tBEST JUMP TOOK %f s", (findBestEnd-jumpAllowanceStartTime).toSec() );

    

    return true;

  }
  ROS_ERROR("ATTEMPTING TO CALCULUTE JUMP ALLOWANCE IN NON-SINGLE TRAJECTORY MODE, NEEDS WORK");
  return false;
}

bool Planner::findPolygon( float jumpDirAngle, float prevDirAngle, float yawTerm, float distTerm, float height, std::vector<bool> acceptedInitial, std::vector<float> initialAngles, std::vector<Eigen::Vector2d> initialRanges, mapbox::geometry::polygon<double> * polygon ){
  // ACTUAL ANGLE IS PASSED HERE for yawterm and for initialAngles
  std::vector<Eigen::Vector2d> resRanges; resRanges.reserve(p_.numAngles);
  float yawInit, distInit;
  if (!p_.quiet){
    std::cout << "---------\n";
    std::cout << "yawTerm " << yawTerm << "    distTerm " << distTerm << "\n";
    std::cout << "scaling factor is " << p_.yawVSDistScalingFactor << "\n";
    // std::cout << "polygonRanges\n";
    std::cout << "polygonRanges = [";
  }

  for (int initYawIndex = 0; initYawIndex < p_.numAngles; initYawIndex++){
    if ( ! acceptedInitial[initYawIndex]){
      resRanges.push_back( Eigen::Vector2d(-1,-1) ); continue;
    }
    int rangeIndex = 0;
    int maxRangeIndex = int(std::round((initialRanges[initYawIndex](1)-initialRanges[initYawIndex](0))/p_.rangeDistResolution));
    yawInit = initialAngles[initYawIndex];
    distInit = initialRanges[initYawIndex](0);
    // for evaluatesvm, the angle must be betwen -pi and pi
    while ( rangeIndex <= maxRangeIndex && !evaluateSVM( distInit, (yawInit-jumpDirAngle), distTerm, yawTerm-jumpDirAngle, height, false ) ){
      rangeIndex += 1; distInit += p_.rangeDistResolution;
    }
    // no point was evaluated to true
    if (rangeIndex > maxRangeIndex){
      resRanges.push_back(Eigen::Vector2d(-1,-1)); continue;
    }
    float rangeInit = distInit;
    while ( rangeIndex <= maxRangeIndex && evaluateSVM( distInit, yawInit-jumpDirAngle, distTerm, yawTerm-jumpDirAngle, height, false ) ){
      rangeIndex += 1; distInit += p_.rangeDistResolution;
    }
    distInit -= p_.rangeDistResolution;
    resRanges.push_back(Eigen::Vector2d(rangeInit-0.001, distInit+0.001));
    // std::cout << "[angle: " << initialAngles[initYawIndex] << "; range: " << rangeInit-0.001 << ", " << distInit+0.001 << "]\n";
    // std::cout << "[" << initialAngles[initYawIndex] << ", " << rangeInit-0.001 << ", " << distInit+0.001 << "], ";
    if (!p_.quiet){
      std::cout << "[" << initialAngles[initYawIndex] << "," << rangeInit-0.001 << "];";
      std::cout << "[" << initialAngles[initYawIndex] << "," << distInit+0.001 << "];";
    }
  } // we got ranges

  if (!p_.quiet)
    std::cout << "];\n";

  // make sure that we chose a single polygon
  std::vector<Eigen::Vector2i> polygonRanges; polygonRanges.reserve(3);
  int inited = -1;
  for (int ind = 0; ind < p_.numAngles; ind++){
    if (inited < 0 && resRanges[ind](0) > 0 )
      inited = ind;
    else if (inited >= 0 && resRanges[ind](0) < 0){
      polygonRanges.push_back( Eigen::Vector2i(inited, ind-1) );
      inited = -1;
    }
  }
  if (inited >= 0)
    polygonRanges.push_back( Eigen::Vector2i(inited, p_.numAngles-1) );

  int bestPoly = 0;
  if ( polygonRanges.size() == 0 ){
    if (!p_.quiet)
      std::cout << "NULL POLYGON RANGE, can't find polygon\n";
    return false;
  }
  else if ( polygonRanges.size() > 1 ){
    bestPoly = -1;
    float bestTieMetric = 100;
    for (int i = 0; i < polygonRanges.size(); i++) {
      float newTieMetric = 0.5 * (initialAngles[polygonRanges[i](0)] + initialAngles[polygonRanges[i](1)]);
      newTieMetric = Geometry::properAngleDiff(newTieMetric, prevDirAngle);
      if (newTieMetric < bestTieMetric){
        bestTieMetric = newTieMetric; bestPoly = i;
      }
    }
  }

  // fill in the polygon
  int minAngleIndex = polygonRanges[bestPoly](0);
  int maxAngleIndex = polygonRanges[bestPoly](1);

  mapbox::geometry::linear_ring<double> ring; ring.reserve(20);
  ring.push_back( {  (initialAngles[minAngleIndex]-jumpDirAngle) / p_.yawVSDistScalingFactor, resRanges[minAngleIndex](0)  } );
  if (!p_.quiet){
    std::cout << "polygonPoints = [[" << (initialAngles[minAngleIndex]-jumpDirAngle) / p_.yawVSDistScalingFactor << "," << resRanges[minAngleIndex](0) << "]";
    for ( int i = minAngleIndex; i <= maxAngleIndex; i++  ){
      ring.push_back( {  (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor, resRanges[i](1)  } );
      std::cout << ";[" << (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor << "," << resRanges[i](1) << "]";
    }
    for ( int i = maxAngleIndex; i >= minAngleIndex; i--  ){
      ring.push_back( {  (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor, resRanges[i](0)  } );
      std::cout << ";[" << (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor << "," << resRanges[i](0) << "]";
    }
    std::cout << "];\n\n";
  }
  else{
    for ( int i = minAngleIndex; i <= maxAngleIndex; i++  )
      ring.push_back( {  (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor, resRanges[i](1)  } );
    for ( int i = maxAngleIndex; i >= minAngleIndex; i--  )
      ring.push_back( {  (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor, resRanges[i](0)  } );
  }
  if ( ring.size() <= 3 ){
    return false;
  }
  polygon->push_back(ring);
  // POLYGON CONTAINS ANGLE WRT THE OBSTACLE: i.e., jumpDirAngle subtracted

  return true;
}


void Planner::publishConstraintPolygon( float jumpDirAngle, bool takeoff, std::vector<bool> acceptedInitial, std::vector<float> initialAngles, std::vector<Eigen::Vector2d> initialRanges){
  // make sure that we chose a single polygon
  std::vector<Eigen::Vector2i> polygonRanges; polygonRanges.reserve(3);
  int inited = -1;
  for (int ind = 0; ind < p_.numAngles; ind++){
    if (inited < 0 && acceptedInitial[ind] )
      inited = ind;
    else if (inited >= 0 && !acceptedInitial[ind] ){
      polygonRanges.push_back( Eigen::Vector2i(inited, ind-1) );
      inited = -1;
    }
  }
  if (inited >= 0)
    polygonRanges.push_back( Eigen::Vector2i(inited, p_.numAngles-1) );

  cheetah_msgs::constrainedPolygon cp;
  cp.takeoff = takeoff;
  cp.jumpDirAngle = jumpDirAngle;
  cp.totalPolygons = polygonRanges.size();

  std_msgs::Float32MultiArray dist_array;
  std_msgs::Float32MultiArray yaw_array;
  int numV = 0;

  for (int polyIndex = 0; polyIndex < polygonRanges.size(); polyIndex++ ){
    dist_array.data.clear(); yaw_array.data.clear(); numV = 0;
    int minAngleIndex = polygonRanges[polyIndex](0);
    int maxAngleIndex = polygonRanges[polyIndex](1);

    yaw_array.data.push_back( (initialAngles[minAngleIndex]-jumpDirAngle) / p_.yawVSDistScalingFactor );
    dist_array.data.push_back( initialRanges[minAngleIndex](0) );
    numV+=1;

    for ( int i = minAngleIndex; i <= maxAngleIndex; i++  ){
      yaw_array.data.push_back( (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor );
      dist_array.data.push_back( initialRanges[i](1) );
      numV+=1;
    }
    for ( int i = maxAngleIndex; i >= minAngleIndex; i--  ){
      yaw_array.data.push_back( (initialAngles[i]-jumpDirAngle) / p_.yawVSDistScalingFactor );
      dist_array.data.push_back( initialRanges[i](0) );
      numV+=1;
    }
    cp.polygon_dist = dist_array;
    cp.polygon_yaw = yaw_array;
    cp.header.stamp = ros::Time::now();
    cp.index = polyIndex;
    pub_cp_.publish(cp);
  }
  if (takeoff) ROS_INFO("PUBLISHED %i CONSTRAINED POLYGONGS FOR TAKEOFF", int(polygonRanges.size()) );
  else ROS_INFO("PUBLISHED %i CONSTRAINED POLYGONGS FOR LANDING", int(polygonRanges.size()) );
}

// QUAD IS: termAngle, termRange, initAngle, initRange
bool Planner::evaluateSVM( float distInit, float yawInit, float distTerm, float yawTerm, float height, bool verbose ) {
  
  // get SVM index from height
  // int index = 0;
  // while( svm_.Height[index] < height && index < svm_.N )
  //   index += 1;
  // float score = 0.0;
  // for(int i = 0; i < svm_.Alpha[index].size(); i++ )
  //   score += svm_.Alpha[index][i] * svm_.SVLabels[index][i] * rbfKernel( svm_.SV[index][i]/svm_.Scale[index],  quad/svm_.Scale[index] );
  // score = score + svm_.Bias[index];

  Eigen::Matrix<float, 1, 5> data;
  data(0) = distInit; data(1) = yawInit; data(2) = distTerm; data(3) = yawTerm; data(4) = height;

  float score = 0.0;
  for(int i = 0; i < svm_.Alpha.size(); i++ )
    score += svm_.Alpha[i] * svm_.SVLabels[i] * rbfKernel( svm_.SV[i]/svm_.Scale,  data/svm_.Scale );
  score = score + svm_.Bias;
  if (verbose)
    std::cout << "score is " << score << "\n";
  if (score > 0)
    return true;
  return false;
}

float Planner::rbfKernel( Eigen::Matrix<float, 1, 5> p1, Eigen::Matrix<float, 1, 5> p2 ){
  return exp( -pow( (p1-p2).norm(), 2));
}


// ----------------------------------------------------------------------------
// post acquisition trajectory improvement

void Planner::addExtraVectorsIntoTrajectory( std::vector<grid_map::Position> * trajectory, std::vector<char> * types  ){
  // ------------------------------
  // Part 1:  increment trajectory

  int trajectory_size = trajectory->size();
  int i = 0;
  float dist;
  if (!p_.quiet){
    while ( i < trajectory_size-1 ){
      ROS_INFO("while i %i traj_size %i", i, trajectory_size-1);
      // we don't add nodes into jumps
      if (types->at(i) == 'w'){
        dist = Node::distanceBtwn( trajectory->at(i), trajectory->at(i+1) );

        ROS_INFO("%i out of %i is walk node", i, trajectory_size-1);
        if ( dist < p_.minEdgeLength ){
          ROS_INFO("Node %i dist %f case min", i, dist);
        }
        else if (dist < p_.singleEdgeLimit ){
          ROS_INFO("Node %i dist %f case 1 edge", i, dist);
        }

        else if ( dist < p_.doubleEdgeLimit ){
          // split edge in two, add 1 node:
          ROS_INFO("Node %i dist %f case 2 edge", i, dist);
          Eigen::Vector2d m = (trajectory->at(i) + trajectory->at(i+1)) / 2;
          trajectory->insert( trajectory->begin() + i+1, m );
          types->insert( types->begin() + i+1, 'w' ); // push the types in
          i+=1;
          trajectory_size += 1;
        }
        else {
          ROS_INFO("Node %i dist %f case 3 edge", i, dist);
          // split edge in three, add two nodes:
          Eigen::Vector2d m1 = trajectory->at(i) * 2/3 + trajectory->at(i+1) * 1/3;
          Eigen::Vector2d m2 = trajectory->at(i) * 1/3 + trajectory->at(i+1) * 2/3;
          trajectory->insert( trajectory->begin()+ i+1, 2, m1 );
          types->insert( types->begin()+ i+1, 2, 'w' ); // push the types in
          trajectory->at(i+2) = m2;
          i+=2;
          trajectory_size += 2;
        }
        i++;
      }
      else if ( types->at(i) == 'j' ){
        ROS_INFO("%i out of %i is jump node, skipping", i, trajectory_size-1);
        i++;
      }
      else{
        ROS_ERROR("%i out of %i is WEIRD, skipping", i, trajectory_size-1);
        i++;
      }
    }
    ROS_INFO("----------");
    ROS_INFO("TOTAL EDGES: from 0 to %i", int(trajectory->size()-1) );

    // ------------------------------
    // Part 2:  debugging

    for(int j = 0; j < trajectory->size()-1; j++ ){
      dist = Node::distanceBtwn( trajectory->at(j), trajectory->at(j+1) );
      if (dist < p_.minEdgeLength){
        ROS_ERROR("EDGE %i DIST %f TOO SMALL", j, dist);
      }
      else if (dist > p_.maxEdgeLength){
        ROS_ERROR("EDGE %i DIST %f TOO LARGE", j, dist);
      }
      else{
        ROS_INFO("EDGE %i DIST %f IS OK", j, dist);
      }
    }
    if (trajectory_size != int(trajectory->size())){
      ROS_ERROR("TRAJ SIZE ISN'T ACTUAL SIZE");
      trajectory_size = trajectory->size();
    }

    // ------------------------------
    // Part 3: remove small edges from the end

    if (trajectory_size >= 2){
      i = trajectory_size;
      while ( (i-2) >= 0 && Node::distanceBtwn( trajectory->at(i-2), trajectory->at(i-1) ) < p_.minEdgeLength ){
        i--;
      }
      if (i < trajectory_size){
        ROS_WARN("THE LAST %i EDGES ARE TOO SMALL, REMOVING LAST %i NODES", trajectory_size-i, trajectory_size-i );
        trajectory->erase( trajectory->begin() + i, trajectory->end() );
        types->erase( types->begin() + i, types->end() );
      }
    }
    ROS_INFO("----------");
  }
  else {
    while ( i < trajectory_size-1 ){
      // we don't add nodes into jumps
      if (types->at(i) == 'w'){
        dist = Node::distanceBtwn( trajectory->at(i), trajectory->at(i+1) );

        if ( p_.singleEdgeLimit <= dist && dist < p_.doubleEdgeLimit ){
          // split edge in two, add 1 node:
          Eigen::Vector2d m = (trajectory->at(i) + trajectory->at(i+1)) / 2;
          trajectory->insert( trajectory->begin() + i+1, m );
          types->insert( types->begin() + i+1, 'w' ); // push the types in
          i+=1;
          trajectory_size += 1;
        }
        else if ( p_.doubleEdgeLimit <= dist ){
          // split edge in three, add two nodes:
          Eigen::Vector2d m1 = trajectory->at(i) * 2/3 + trajectory->at(i+1) * 1/3;
          Eigen::Vector2d m2 = trajectory->at(i) * 1/3 + trajectory->at(i+1) * 2/3;
          trajectory->insert( trajectory->begin()+ i+1, 2, m1 );
          types->insert( types->begin()+ i+1, 2, 'w' ); // push the types in
          trajectory->at(i+2) = m2;
          i+=2;
          trajectory_size += 2;
        }
        i++;
      }
      else{
        i++;
      }
    }
    // ------------------------------
    // Part 2:  debugging
    for(int j = 0; j < trajectory->size()-1; j++ )
      dist = Node::distanceBtwn( trajectory->at(j), trajectory->at(j+1) );
    if (trajectory_size != int(trajectory->size()))
      trajectory_size = trajectory->size();

    // ------------------------------
    // Part 3: remove small edges from the end
    if (trajectory_size >= 2){
      i = trajectory_size;
      while ( (i-2) >= 0 && Node::distanceBtwn( trajectory->at(i-2), trajectory->at(i-1) ) < p_.minEdgeLength ){
        i--;
      }
      if (i < trajectory_size){
        trajectory->erase( trajectory->begin() + i, trajectory->end() );
        types->erase( types->begin() + i, types->end() );
      }
    }
  }

}

void Planner::getGradients(){
  // convert cluster tag into a opencv matrix

  // note that cluster_tag should not have any nans
  // int nancounter = 0;
  // for (grid_map::GridMapIterator iterator(*global_map_); !iterator.isPastEnd(); ++iterator){
  //   if ( ! global_map_->isValid(*iterator, "cluster_tag") || global_map_->at("cluster_tag", *iterator) != global_map_->at("cluster_tag", *iterator) ){
  //     nancounter++;
  //   }
  // }
  // if (nancounter != 0){
  //   ROS_ERROR("WTF WHY DOES CLUSTER TAG HAVE A NAN %i", nancounter);
  //   ROS_ERROR("WTF WHY DOES CLUSTER TAG HAVE A NAN %i", nancounter);
  //   ROS_ERROR("WTF WHY DOES CLUSTER TAG HAVE A NAN %i", nancounter);
  // }
  const ros::Time copyingStart(ros::Time::now());

  cv::Mat cluster_cv(cv::Size(global_map_->getSize().x(), global_map_->getSize().y()), CV_32F);
  for (grid_map::GridMapIterator iterator(*global_map_); !iterator.isPastEnd(); ++iterator) {
    if (! global_map_->isValid(*iterator, "cluster_tag") )
      cluster_cv.at<float>( (*iterator)(0), (*iterator)(1) ) = 0.0;
    else
      cluster_cv.at<float>( (*iterator)(0), (*iterator)(1) ) = std::min( global_map_->at("cluster_tag", *iterator), (float) 1.0 );
  }
  const ros::Time copyingEnd(ros::Time::now());

  // apply gaussian blur; passing sigmaX, simgaY defines the kernel

  cv::GaussianBlur(cluster_cv, cluster_cv, cv::Size(0,0), p_.sd, p_.sd );

  cv::Mat grad_x, grad_y;

  // compute gradients
  cv::Sobel( cluster_cv, grad_x, CV_32F, 1, 0, 3);
  cv::Sobel( cluster_cv, grad_y, CV_32F, 0, 1, 3);

  const ros::Time opencvEnd(ros::Time::now());

  // put the back in the global map; this is done because i'm lazy and wanna run a grid map iterator
  // TO DO: can iterate over opencv matrix partion that would have been cool

  for (grid_map::GridMapIterator iterator(*global_map_); !iterator.isPastEnd(); ++iterator) {
    global_map_->at("gx", *iterator) = grad_y.at<float>( (*iterator)(0), (*iterator)(1) );
    global_map_->at("gy", *iterator) = grad_x.at<float>( (*iterator)(0), (*iterator)(1) );
    global_map_->at("cluster_gauss", *iterator) = cluster_cv.at<float>( (*iterator)(0), (*iterator)(1) );
  }
  const ros::Time backEnd(ros::Time::now());
  if (!p_.quiet){
    ROS_INFO("To Opencv Took %f s", (copyingEnd - copyingStart).toSec() );
    ROS_INFO("Opencv Took %f s", (opencvEnd - copyingEnd).toSec() );
    ROS_INFO("From Opencv Took %f s", (backEnd - opencvEnd).toSec() );
  }

  // grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(cluster_cv, "cluster_gauss", *global_map_);
  // grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(grad_x, "gx", *global_map_);
  // grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(grad_y, "gy", *global_map_);

}

void Planner::improveTrajectoryOnce(float multiplier, std::vector<Eigen::Vector2d> * trajectory, std::vector<char> * types){
  // assumptions
  // the trajectory is stored in nodeTrajectory, first to last element
  // use trajectoryLen to get length

  // don't move the first and last points
  // TO DO: do we wanna move the last point? i think so
  
  if (trajectory->size() > 2){
    Eigen::Vector2d prevPoint, thisPoint, nextPoint;
    Eigen::Vector2d prevPerp, nextPerp, direction, temp;
    float gradCount = 0.0;
    bool colin = true;

    // TO DO: there must be a check that ensures that this is only done for non jump edges
    if( p_.verboseImprovement && !p_.quiet)
      std::cout << "\t\ttrajLen is " << trajectory->size() << " let's move some.\n";
    int numSkipsDueToJumps = 0;
    for (int i = 1; i < trajectory->size()-1; i++ ){
      

      // edge from i-1 to i and from i to i+1 must be walkable
      if ( ! ( types->at(i-1) == 'w' && types->at(i) == 'w' )  ){
        numSkipsDueToJumps++;
        continue;
      }

      // preparation
      prevPoint = trajectory->at(i-1);
      thisPoint = trajectory->at(i);
      nextPoint = trajectory->at(i+1);

      // get the direction pointing to the left from the point
      prevPerp = Geometry::getPerp(prevPoint, thisPoint);
      nextPerp = Geometry::getPerp(thisPoint, nextPoint);
      direction = prevPerp + nextPerp;
      direction = direction / direction.norm();
      
      //---------------------------------------------
      // polygon rubbish

      // make a polygon, count things up
      grid_map::Polygon p1; p1.setFrameId( global_map_->getFrameId());

      temp = thisPoint + Geometry::getParl(thisPoint, nextPoint) * std::min(p_.countUpRad, Geometry::distBtwn(thisPoint, nextPoint)/2) 
                       + nextPerp * p_.robot_width/2;  p1.addVertex(temp);

      temp -= nextPerp * p_.robot_width; p1.addVertex(temp);

      colin = Geometry::colinear(prevPoint, thisPoint, nextPoint);
      if ( colin )
        temp = thisPoint - direction * p_.robot_width/2; 
      else
        temp = Geometry::lineIntersection( Geometry::lineThroughTwoPoints(prevPoint - prevPerp * p_.robot_width/2, 
                                                                          thisPoint - prevPerp * p_.robot_width/2),
                                           Geometry::lineThroughTwoPoints(thisPoint - nextPerp * p_.robot_width/2, 
                                                                          nextPoint - nextPerp * p_.robot_width/2) );
      p1.addVertex(temp);

      temp = thisPoint + Geometry::getParl(thisPoint, prevPoint) *  std::min(p_.countUpRad, Geometry::distBtwn(prevPoint, thisPoint)/2) 
                       - prevPerp * p_.robot_width/2; p1.addVertex(temp);

      temp += prevPerp * p_.robot_width; p1.addVertex(temp);

      if ( colin )
        temp = thisPoint + direction * p_.robot_width/2; 
      else
        temp = Geometry::lineIntersection( Geometry::lineThroughTwoPoints(prevPoint + prevPerp * p_.robot_width/2, 
                                                                          thisPoint + prevPerp * p_.robot_width/2),
                                           Geometry::lineThroughTwoPoints(thisPoint + nextPerp * p_.robot_width/2, 
                                                                          nextPoint + nextPerp * p_.robot_width/2) );
      p1.addVertex(temp);


      // polygon rubbish over
      //---------------------------------------------

      // count it up
      gradCount = 0.0;
      int numPolygonPoints = 0;
      for (grid_map::PolygonIterator iterator(*global_map_, p1); !iterator.isPastEnd(); ++iterator){
        // it's a directional derivative
        numPolygonPoints++;
        gradCount += global_map_->at("gx", *iterator) * direction(0) + global_map_->at("gy", *iterator) * direction(1);
      }

      // std::cout << "\tPoint " << i << " gradCount before " << gradCount << " polygon points " << numPolygonPoints << "\n"; 

      // adjust the mutiplier
      if( !p_.quiet && p_.verboseImprovement)
        std::cout << "\t" << gradCount << "\t";
      gradCount = std::copysign( std::max( (float) fabs( gradCount), p_.minCountVal ), gradCount ) ; // min value
      if( !p_.quiet && p_.verboseImprovement)
        std::cout << gradCount << "\t";
      gradCount = multiplier * gradCount * global_map_->getResolution(); // scale by resolution and multiplier
      if( !p_.quiet && p_.verboseImprovement)
        std::cout << gradCount << "\t";
      gradCount = std::copysign(std::min( fabs(gradCount), p_.maxCountTimes * global_map_->getResolution()  ), gradCount); // max value
      if( !p_.quiet && p_.verboseImprovement)
        std::cout << gradCount << "\n";



      // this is the new point
      thisPoint = trajectory->at(i) - direction * gradCount;
      // std::cout << "\tPoint " << i << " proposed move " << gradCount << " in direction " << direction.transpose() << "\n"; 
      // check walkability
      if ( Node::checkWalkabilityBtwn(prevPoint, thisPoint, (i == 1) ) && Node::checkWalkabilityBtwn(thisPoint, nextPoint) ){
        // great, still walkable, update the location
        if( !p_.quiet && p_.verboseImprovement)
          std::cout << "\t!!!Point " << i+1 << " moved by " << gradCount << "\n"; 
        trajectory->at(i) = thisPoint;
      }
      else{
        // not walkable, trying a smaller multiplier
        thisPoint = trajectory->at(i) - direction * gradCount / 2;
        if ( Node::checkWalkabilityBtwn(prevPoint, thisPoint, (i == 1) ) && Node::checkWalkabilityBtwn(thisPoint, nextPoint) ){
          if( !p_.quiet && p_.verboseImprovement)
            std::cout << "\t!!!Point " << i+1 << " moved by " << gradCount/2 << "\n"; 
          // great, this new one is walkable, update the location
          trajectory->at(i) = thisPoint;
        }
      }

    } // for loop across all the points
    if( !p_.quiet)
      ROS_WARN("In improving the trajectory I made %i skips due to jumps", numSkipsDueToJumps);

  } // if more than 2


} 

// ----------------------------------------------------------------------------
// trajectory handling utility functions

void Planner::publishTrajectory(ros::Publisher * publisher, std::vector<Eigen::Vector2d> * trajectory, int trajLen ){
  nav_msgs::Path traj = nav_msgs::Path();
  grid_map::Index index;
  traj.header.frame_id = "world";
  traj.header.stamp = ros::Time::now();

  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "world";
  pose.pose.orientation.x = 0;
  pose.pose.orientation.y = 0;
  pose.pose.orientation.z = 0;
  pose.pose.orientation.w = 1;

  // put every node into the trajectory, start to end
  for (int i = 0; i < trajLen; i++){
    // TO DO: also uncivilized
    pose.pose.position.x = (*trajectory)[i](0);
    pose.pose.position.y = (*trajectory)[i](1);
    
    if ( global_map_->getIndex((*trajectory)[i], index) && global_map_->isValid( index, p_.layer))
      pose.pose.position.z = global_map_->atPosition(p_.layer, (*trajectory)[i]) + 0.1;
    else
      pose.pose.position.z = 0.5;
    
    traj.poses.push_back(pose);
  }

  publisher->publish(traj);
}

void Planner::resetTreesUpToK(int i1, int i2){
  for(int i = 0; i <= i1; i++)
    tree1_[i].reset();

  for(int i = 0; i <= i2; i++)
    tree2_[i].reset();
}

float Planner::populateVectorTrajectory( Node * node, std::vector<grid_map::Position> * trajectory, std::vector<char> * types ){
  // return total distance of the trajectory
  // populate trajectory vector with Eigen::Vector2d of positions
  if (!p_.quiet)
    std::cout << node->ind_ << " "; 
  
  trajectory->push_back( node->pos_ );
  types->push_back( node->pathTypeToFather_ );
  // check if node has a father, procede
  if (node->father_)
    return node->distanceTo(node->father_) + populateVectorTrajectory(node->father_, trajectory, types);
  return 0.0;
}

// ----------------------------------------------------------------------------
// tube related

void Planner::getTubeLines(){
  trajLines_.clear();
  leftLines_.clear();
  rightLines_.clear();
  for(int i = 0; i < trajectoryLength_-1; i++){
    trajLines_[i] = Geometry::lineThroughTwoPoints( trajectory_[i], trajectory_[i+1] );
    leftLines_[i] = Geometry::lineThroughTwoPoints( leftTube_[i], leftTube_[i+1] );
    rightLines_[i] = Geometry::lineThroughTwoPoints( rightTube_[i], rightTube_[i+1] );
  }
}

// unused
void Planner::scaleVelocity(Eigen::Vector2d * v, float tubeRad){
  *v /= v->norm();
  *v *= maxSpeedFromTube(tubeRad);
  // if (tubeRad < 0.3){
  //   *v *= p_.maxV1;
  //   // res *= 0.4;
  // }
  // else if (tubeRad < 0.4){
  //   *v *= p_.maxV2;
  //   // res *= 0.6;
  // }
  // else if (tubeRad < 0.5){
  //   *v *= p_.maxV3; 
  //   // res *= 0.8;
  // }
  // else {
  //   *v *= p_.maxV4;
  //   // res *= 1.0;
  // }
}

float Planner::maxSpeedFromTube(float tubeRad){
  if (tubeRad < 0.26){
    return p_.maxV1;
  }
  else if (tubeRad < 0.36){
    return p_.maxV2;
  }
  else if (tubeRad < 0.46){
    return p_.maxV3; 
  }
  else {
    return p_.maxV4;
  }
}

void Planner::findTubeAroundTrajectory(std::vector<grid_map::Position> * trajectory, 
                                       std::vector<char> * types, 
                                       std::vector<grid_map::Position> * leftTube,
                                       std::vector<grid_map::Position> * rightTube,
                                       bool useOriginTube){

  // assumptions:
  //     -    leftTube, rightTube are empty and sized up

  // ---------------------------------------------
  // Part 1
  // declare arrays
  int trajLength = trajectory->size();
  float lPrevRad[trajLength];
  float lMeRad[trajLength];
  float finalRad[trajLength];

  // ---------------------------------------------
  // Part 2
  // get perpendicular directions, they will prove to be useful
  Eigen::Vector2d v_perp[trajLength];
  Eigen::Vector2d v_perp_last = Geometry::getPerp( trajectory->at(0), trajectory->at(1) );

  v_perp[0] = v_perp_last;
  for(int i=1; i < trajLength-1; i++){
    v_perp[i] = v_perp_last;
    v_perp_last = Geometry::getPerp( trajectory->at(i), trajectory->at(i+1) );
    v_perp[i] = v_perp[i] + v_perp_last;
    v_perp[i] /= v_perp[i].norm();
  }
  v_perp[trajLength-1] = v_perp_last;

  // ---------------------------------------------
  // Part 3
  // run zero order hold

  // notice, we are going one before
  float lRes, rRes, rMeRad, rPrevRad;
  bool lResHalved, rResHalved;
  bool originIsANaN = Node::checkIfElevationIsNan(trajectory->at(0));
  // Eigen::Vector2d cand1, cand2;
  for (int i = 0; i < trajLength-1; i++){
    lMeRad[i] = p_.robot_width/2;
    lPrevRad[i+1] = p_.robot_width/2;
    rMeRad = p_.robot_width/2;
    rPrevRad = p_.robot_width/2;

    lRes = global_map_->getResolution();
    rRes = global_map_->getResolution();
    lResHalved = false; rResHalved = false;

    // TO DO: remove (?)
    // nan handling:
    // if i == 0 and the origin point is not valid (i.e. NaN)
    // set radius to Origin Rad, and move on
    if ( i == 0 && originIsANaN){
      lMeRad[i] = p_.originRad;
      lPrevRad[i+1] = p_.originRad;
      continue;
    } 

    // if this is a jumpe edge - set tube radius to be a particular constant
    if (types->at(i) == 'j'){
      lMeRad[i] = p_.jumpTubeRadius;
      lPrevRad[i+1] = p_.jumpTubeRadius;
      continue;
    }

    while (true){
      // do left
      if ( Node::checkTraversabilityOverLine( // check that new candidate points are traversable
        trajectory->at(i) + v_perp[i] * (lMeRad[i] + lRes), 
        trajectory->at(i+1) + v_perp[i+1] * (lPrevRad[i+1] + lRes)    ) ){
        lMeRad[i] += lRes; lPrevRad[i+1] += lRes; // propogate further with each step
      }
      else{
        if ( !lResHalved ){  // if reoslution not halved yet - half it
          lResHalved = true; lRes /= 2; 
        } // if already halved - stop
        else{ break; }
      }

      // do right
      if ( Node::checkTraversabilityOverLine( // check that new candidate points are traversable
        trajectory->at(i) - v_perp[i] * (rMeRad + rRes), 
        trajectory->at(i+1) - v_perp[i+1] * (rPrevRad + rRes)    ) ){
        rMeRad += rRes; rPrevRad += rRes; // propogate further with each step
      }
      else{
        if ( !rResHalved ){  // if reoslution not halved yet - half it
          rResHalved = true; rRes /= 2; 
        }
        else{ // if already havled - stop; right broke, insert data back into left
          lMeRad[i] = rMeRad;
          lPrevRad[i+1] = rPrevRad;
          break;
        }
      }
    }
  }

  // ---------------------------------------------
  // Part 4
  // first order adjustment - from 0-order to piecewise linear

  finalRad[0] = lMeRad[0];
  finalRad[trajLength-1] = lPrevRad[trajLength-1];
  Eigen::Vector2d c1l, c2l, c1r, c2r;

  float tempRad;
  for(int i = 1; i < trajLength-1; i++){
    // case 1
    if (lMeRad[i] > lPrevRad[i] + 0.001){
      //radius of the next segment is larger than the radius of the previus segment
      // we ae gonna be increasing the tube of i from lPrevRad up to possibly lMeRad
      // this is not effected by jumps

      // we need to fix the previous edge
      tempRad = lPrevRad[i];

      lRes = global_map_->getResolution();
      lResHalved = false;
      while(tempRad < lMeRad[i]){
        c1l = trajectory->at(i-1) + v_perp[i-1] * lMeRad[i-1];
        c2l = trajectory->at(i) + v_perp[i] * (tempRad + lRes );

        c1r = trajectory->at(i-1) - v_perp[i-1] * lMeRad[i-1];
        c2r = trajectory->at(i) - v_perp[i] * (tempRad + lRes );

        // the case with i == 1 requries special treatment
        if ( i == 1 && useOriginTube ){
          c1l = leftTube_[committed_];
          c1r = rightTube_[committed_];
        }
        if ( Node::checkTraversabilityOverLine( c1l, c2l, (i == 1 && originIsANaN) ) &&
             Node::checkTraversabilityOverLine(c1r, c2r, (i == 1 && originIsANaN) ) ){
          tempRad += lRes;
        }
        else{
          if (! lResHalved ){ lResHalved = true;  lRes /= 2; }
          else { break; }
        }
      } // while
      finalRad[i] = tempRad;
    }
    else if ( lMeRad[i] < lPrevRad[i] - 0.001 ){
      // the last segment edge has higher radius that the next segment edge
      // we need to fix the next edge - increase it's first bound
      // no nan handling necessary

      // this is effected by jumps
      if (types->at(i) == 'j'){
        finalRad[i] = lMeRad[i];
      }
      else{
        tempRad = lMeRad[i];

        lRes = global_map_->getResolution();
        lResHalved = false;
        while(tempRad < lPrevRad[i]){

          c1l = trajectory->at(i) + v_perp[i] * (tempRad + lRes);
          c2l = trajectory->at(i+1) + v_perp[i+1] * lPrevRad[i+1];

          c1r = trajectory->at(i) - v_perp[i] * (tempRad + lRes);
          c2r = trajectory->at(i+1) - v_perp[i+1] * lPrevRad[i+1];

          if ( Node::checkTraversabilityOverLine( c1l, c2l ) && Node::checkTraversabilityOverLine(c1r, c2r) ){
            tempRad += lRes;
          }
          else{
            if (! lResHalved ){ lResHalved = true;  lRes /= 2; }
            else { break; }
          }
        } // while
        finalRad[i] = tempRad;
      }
    }
    else{
      finalRad[i] = lMeRad[i];
    } 
  }

  // ---------------------------------------------
  // populate the left and right tubes
  if (useOriginTube){
    leftTube->push_back(leftTube_[committed_]);
    rightTube->push_back(rightTube_[committed_]);
  }
  else{
    leftTube->push_back(trajectory->at(0) + v_perp[0] * finalRad[0]);
    rightTube->push_back(trajectory->at(0) - v_perp[0] * finalRad[0]);
  }
  for( int i = 1; i < trajLength; i++){
    leftTube->push_back(trajectory->at(i) + v_perp[i] * finalRad[i]);
    rightTube->push_back(trajectory->at(i) - v_perp[i] * finalRad[i]);
  }
}

// ----------------------------------------------------------------------------
// jumping related

std::tuple<bool, Eigen::Vector2d, float> Planner::getRobotPose(){
  geometry_msgs::TransformStamped odom_to_base;
  try{
    odom_to_base = tf_buffer_->lookupTransform("world", "base_link", ros::Time(0) ); // ros::Time(0): get most recent
  }
  catch (tf2::TransformException &ex) {
    // either something is properly wrong, or it's one of the first few measurements
    ROS_ERROR("PLANNER COULDN'T GET THE STATE TRANSFORM:\n%s",ex.what());
    return std::make_tuple(false, Eigen::Vector2d(0,0), 0.0);
  }
  // TO DO: does this work???
  if ( (ros::Time::now() -  odom_to_base.header.stamp).toSec() > 0.3 ){
    ROS_ERROR("ACQUIRED ROBOT'S POSE, BUT POSE IS TOO OLD - IS STATE ESTIMATOR (T265 CAMERA) ON?");
    return std::make_tuple(false, Eigen::Vector2d(0,0), 0.0);
  }


  // extracting yaw from the quaternion
  Eigen::Vector2d pos( odom_to_base.transform.translation.x, odom_to_base.transform.translation.y );
  tf2::Quaternion quat(
        odom_to_base.transform.rotation.x,
        odom_to_base.transform.rotation.y,
        odom_to_base.transform.rotation.z,
        odom_to_base.transform.rotation.w);
  tf2::Matrix3x3 mat(quat);
  double roll, pitch, yaw; // don't care about roll / pitch
  mat.getRPY(roll, pitch, yaw);
  return std::make_tuple(true, pos, yaw);
}


void Planner::evaluateTerrainAroundRobot(float angle){
  const ros::Time evalStartTime(ros::Time::now());
  // angle is the direction of the jump - relative to robot's yaw.
  auto [poseAcquired, pos, yaw] = getRobotPose();
  if (!poseAcquired){
    ROS_ERROR("COULDN'T ACQUIRE ROBOT'S POSE");
    return;
  }
  
  // DISTANCE INITIALIZATION
  grid_map::Position parl = Eigen::Vector2d(1,0); parl = Geometry::rotate( parl, angle );
  // find the first non-cluster point going in parl direction from the jump start
  grid_map::Index tempInd;
  if (! global_map_->getIndex(pos, tempInd) ){ ROS_ERROR("CAN'T GET POS INDEX?"); return; }
  float cluster = global_map_->at("cluster_tag", tempInd);
  grid_map::Position temp = pos;
  // can be indexed, valid elevation, and belongs to same cluster - increment
  while ( global_map_->getIndex(temp, tempInd) && global_map_->isValid( tempInd, "elevation" ) && Node::belongsToCluster( tempInd, cluster )){
    temp += parl * global_map_->getResolution();
    if ( (temp-pos).norm() >= 0.65 )
      break;
  }
  float dist_initialization = std::max( float(0.25), float((temp-pos).norm()) ) ;

  alglib::real_1d_array c; c.setlength(5);
  c(0) = 0.1; c(1) = dist_initialization; c(2) = 0.0; c(3) = 0.0; c(4) = 0.0;
  float newJumpAngle = yaw+angle;
  Geometry::makeAngleProper(newJumpAngle);
  if (!p_.quiet)
    ROS_WARN("THE ANGLES ARE yaw %f angle %f newJumpAngle %f", yaw*180/M_PI, angle*180/M_PI, newJumpAngle*180/M_PI);

  if (! Node::evaluateTerrainAt(pos, newJumpAngle, c, &c, false, true) )
    return;
  for (int i = 0; i < 6; i++){
    std::cout << "at iter " << i-1 << " the angle is " << newJumpAngle << " c is " << c.tostring(3).c_str() << "\n";
    newJumpAngle -= atan(c[2]);
    if (i <= 4)
      if(!Node::evaluateTerrainAt(pos, newJumpAngle, c, &c, false, true))
        return;
    else if (i == 5) // remove the floor past borderline on the last
      if (! Node::evaluateTerrainAt(pos, newJumpAngle, c, &c, true, true))
        return; 
  }
  newJumpAngle -= atan(c[2]);

  float obs_height = c(0);
  float dist_to_obs = c(1);
  float yaw_init = Geometry::getProperAngle(yaw-newJumpAngle);
  float yaw_terminal = Geometry::getProperAngle(jumpAngles_(1)-newJumpAngle);

  if ( c(0) < 0.05  || c(1) < 0.1 || c(1) > 0.9 || fabs(c(0)-height_) > 0.035 ){
    ROS_ERROR("BAD HEIGHT or BAD DIST TO OBST");
    obs_height = height_;
    dist_to_obs = distToObst_;
    yaw_init = jumpAngles_(0);
    yaw_terminal = jumpAngles_(1);
  }
  const ros::Time evalEndTime(ros::Time::now());

  
  float dist_past_obs_lb = distPastObst_;
  float dist_past_obs_ub = distPastObst_;
  
  

  LCM_comm::sendJump3DInfo( obs_height, dist_to_obs, dist_past_obs_ub, dist_past_obs_ub, yaw_init, yaw_terminal );
  publishEvent("jump_req_savva", 4);
  publishDeltaTime( "jumpRequest", (evalEndTime-evalStartTime).toSec() );

  ROS_WARN("SENT A JUMP REQUEST height: %f dist_to_obst: %f past_lb: %f past_ub: %f yaw_init: %f yaw_term: %f", obs_height, dist_to_obs, dist_past_obs_lb, dist_past_obs_ub, yaw_init, yaw_terminal );
  ROS_WARN("NOTE: sending upper bound for the lower bound to simplify optimization");
  ROS_WARN("YAW INITIAL IS %f, robot yaw is %f, newJumpAngle %f", yaw_init*180/M_PI, yaw*180/M_PI, newJumpAngle*180/M_PI);
  ROS_WARN("EVALUATION TOOK %f seconds", (evalEndTime-evalStartTime).toSec());

}

void Planner::jumpRequestExtended(int jumpSegment){
  if (plannerHasControlOfTheCheetah_){
    publishEvent("jump_req_tracker", 3);
    ROS_WARN("PLANNER IS SURRENDERING CONTROL OVER THE COMMANDS DUE TO JUMPING");

    // just in case alright
    LCM_comm::zeroOut();
    LCM_comm::zeroOut();

    // lose control of the cheetah
    plannerHasControlOfTheCheetah_ = false;
    user_says_go_ = false;

    // wait; notice that at this point the controller can't send commands, 
    // so the only trajectory following thread is this one
    ros::Time temp = ros::Time::now();
    while ( (ros::Time::now() - temp).toSec() < p_.jump_wait_before ){
      // do literally nothing
    }
    ROS_INFO("Waited for the jump for %f seconds", p_.jump_wait_before);

    float jumpDirAngle = Geometry::angleToVector( trajectory_[jumpSegment+1] - trajectory_[jumpSegment] );

    evaluateTerrainAroundRobot(jumpDirAngle);
  }
}

void Planner::callbackFromJump(std_msgs::Empty msg){
  if (task_state_ == 'j'){ 
    publishEvent("jump_finished", 6);

    publishRobotPose( "landing_des", trajectory_[task_segment_+1], jumpAngles_(1)  );

    auto [poseAcquired, pos, yaw] = getRobotPose();
    if (!poseAcquired){
      ROS_ERROR("COULDN'T ACQUIRE ROBOT'S POSE");
      return;
    }
    publishRobotPose( "landing_actual", pos, yaw  );

    // first wait a little
    ros::Time temp = ros::Time::now();
    while ( (ros::Time::now() - temp).toSec() < p_.jump_wait_after ){
      // do literally nothing
    }

    ROS_INFO("Waited after the jump for %f", p_.jump_wait_after);
    plannerHasControlOfTheCheetah_ = true;
    task_state_ = 'w';
    xy_integrator_(0) = 0.0; xy_integrator_(1) = 0.0; phi_integrator_ = 0.0;

    task_segment_ += 1;
    ROS_WARN("------- Task state is WALKING %i", task_segment_);
    ROS_WARN("-----------------------------");
    ROS_WARN("PLANNER IS ISSUING COMMANDS AGAIN");
    if (task_segment_ >=trajectoryLength_){
      plannerHasControlOfTheCheetah_ = false;
      ROS_WARN("-----------------------------");
      ROS_WARN("JUMP WAS LAST THING");
      ROS_WARN("------- Task state is NOTHING %i", task_segment_);
      ROS_WARN("-----------------------------");
      ROS_WARN("PLANNER IS NOT ISSUING COMMANDS");
    }
  }
  else{
    ROS_ERROR("I WAS NOT SUPPOSED TO RECEIVE A JUMP CALLBACK");
    ROS_ERROR("I WAS NOT SUPPOSED TO RECEIVE A JUMP CALLBACK");
    ROS_ERROR("I WAS NOT SUPPOSED TO RECEIVE A JUMP CALLBACK");
  }
}

void Planner::callbackFromJumpComputed(std_msgs::Empty msg){
  std::cout << "planner received callback from jump computed\n";
  publishEvent("jump_req_matt", 5);
}


// ----------------------------------------------------------------------------
// trajectory tracking command related

Eigen::Vector3d Planner::getDesiredCMD(bool * needFixing){
  *needFixing = true;
  // -------------------------------------------
  // FIRST
  // determine current Cheetah state
  auto [poseAcquired, pos, yaw] = getRobotPose();
  if (!poseAcquired){
    ROS_ERROR("COULDN'T ACQUIRE ROBOT'S POSE");
    return Eigen::Vector3d(0,0,0);
  }

  // -------------------------------------------
  // init some variables
  int segment = std::max(task_segment_-2, 0); // trajectory segment we are in
  bool right = true; // are we to the left or to the right of that trajectory segment?
  bool insideTube = false; // are we inside the tube?
  // segment i corresponds to trajectory_ i to i+1; there are therefore n-1 segment, where n- length of trajectory

  // -------------------------------------------
  // SECOND
  // determine segment / right / insideTube
  {
    // we are before the 0th segment (the zeroth mid-line); let's project on the zeroth midline
    if ( Geometry::isRight( leftTube_[0], rightTube_[0], pos )  )
      pos = Geometry::perpOnLine( Geometry::lineThroughTwoPoints( leftTube_[0], rightTube_[0] ), pos );
    // find the segment we are in
    while( segment + 1 < trajectoryLength_ && Geometry::isLeft( leftTube_[segment+1], rightTube_[segment+1], pos ) )
      segment++;

    if (segment <= trajectoryLength_-2){
      // check which side we are on
      if ( Geometry::isLeft(trajectory_[segment], trajectory_[segment+1], pos ) )
        right = false;
      // check if we are inside the tube
      if (right && Geometry::isLeft( rightTube_[segment], rightTube_[segment+1], pos ) )
        insideTube = true;
      else if (!right && Geometry::isRight(leftTube_[segment], leftTube_[segment+1], pos ) )
        insideTube = true;
    }
    else if (segment == trajectoryLength_-1){
      if ( Geometry::isLeft(trajectory_[trajectoryLength_-2], trajectory_[trajectoryLength_-1], pos ) )
        right = false;
    }
  }

  // -------------------------------------------
  // THIRD:
  // determine the task; task descriptions:
  {
    // determine the current thing we should be doing
    // trajectory-following task resolution
    // 'w' - walking, whether with the next edge or without
    // 'k' - PD to the point from which we are going to jump
    // 'j' - waiting for the jump callback to be received
    // 'p' - PD to the next point

    // allowed transitions: 
    // w -> k, in case the next edge is an jump edge
    // k -> j, in case we are at the spot
    // j -> w, produced by the callback
    // w -> p, in case trajectory length is running out
    // p -> w, in case trajectory length is actually not running out

    // task segment updates:
    // walking: whenever needed
    // next edge is a jump edge: task segment to current, don't do anything else, updated by somebody else
  }

  // update task segment from walking
  if (task_state_ == 'w' && segment > task_segment_ && segment != trajectoryLength_-1 && nextEdgeType_[ segment ] != 'j' )
    if (task_segment_ < trajectoryLength_-1 && nextEdgeType_[ task_segment_+1 ] != 'j' )
      task_segment_ = segment;
  
  // if (task_state_ == 'w' && segment < task_segment_ && segment >=0){
  //   ROS_WARN("FUNKY CASE");
  //   task_segment_ = segment;
  //   return getCommandFromTask(pos, right, yaw);
  // }

  // currently walking, next edge is a jump edge, and we are close enough to the next edge
  if ( task_state_ == 'w' && nextEdgeType_[ segment+1 ] == 'j' ){
    std::cout << "seg " << segment << " ts " << task_segment_ << " next edge is a jump, currently walking, distance is: ";
    float distToNext_temp = Geometry::distBtwn( Geometry::perpOnLine( trajLines_[segment], pos ), trajectory_[segment+1] ) / p_.atDistToNext;
    std::cout << distToNext_temp << "\t";
    if (distToNext_temp < 1){
      std::cout << " close enough, switching to jump PD\n";
      task_state_ = 'k';
      xy_integrator_(0) = 0.0; xy_integrator_(1) = 0.0; phi_integrator_ = 0.0;
      task_segment_ = segment;
      ROS_WARN("------- Task state is PD TO JUMP %i", task_segment_);
      ROS_WARN("-----------------------------");
      PDTrackingStart_ = ros::Time::now();
    }
    else
      std::cout << " not close enough, keep walking\n";
  }
  else if ( task_state_ == 'w' && segment == trajectoryLength_-2 ){
    std::cout << "seg " << segment << " ts " << task_segment_ << "; walking, running out of trajectory?\n";
    float distToNext_temp = Geometry::distBtwn( Geometry::perpOnLine( trajLines_[segment], pos ), trajectory_[segment+1] ) / p_.atDistToNext;
    if (distToNext_temp < 1){
      task_state_ = 'p';
      xy_integrator_(0) = 0.0; xy_integrator_(1) = 0.0; phi_integrator_ = 0.0;
      task_segment_ = trajectoryLength_-2;
      ROS_WARN("------- Task state is WALKING TO FINAL POINT %i", task_segment_);
      ROS_WARN("-----------------------------");
    }
    else{
      std::cout << "no, still got more walking to do";
    }
  }
  // currently walking towards the jump point, and we are close enough to the point to issue a jump request
  else if (task_state_ == 'k'){
    float phi_desired = jumpAngles_(0);
    // check if we are at the point and facing in the right direciton - if so, stop
    if ( Node::distanceBtwn( pos, trajectory_[task_segment_+1]) < p_.jumpStopDistRadius && fabs(yaw -  phi_desired) < p_.jumpStopAngleRadius ){
      task_state_ = 'j';
      ROS_INFO("TRACKING ERROR");
      std::cout << "[" << fabs(phi_desired - yaw) << ", " <<  (trajectory_[task_segment_+1] - pos).norm() << "]\n";
      task_segment_ += 1;
      ROS_WARN("------- Task state is WAITING FOR JUMP %i", task_segment_);
      ROS_WARN("-----------------------------");
      Eigen::Vector3d cmd(0,0,0); // *needFixing = false;

      // great, we are there, let Matt know
      if (p_.sendJumpExtendedRequest){
        publishRobotPose( "takeoff_actual", pos, yaw );
        publishRobotPose( "takeoff_des", trajectory_[task_segment_], phi_desired );
        jumpRequestExtended(task_segment_);
      }
      
      if (p_.verboseFollowerEssential)
        ROS_INFO_THROTTLE(0.5, "At the JUMP, ZERO velocity");
      return cmd;
    }
    else if ( p_.reset_integrators && Node::distanceBtwn( pos, trajectory_[task_segment_+1]) < p_.jumpStopDistRadius ){
      ROS_INFO_THROTTLE(0.5, "RESET the position integrator"); xy_integrator_(0) = 0.0; xy_integrator_(1) = 0.0; 
    }
    else if ( p_.reset_integrators && fabs(yaw -  phi_desired) < p_.jumpStopAngleRadius ){
      ROS_INFO_THROTTLE(0.5, "RESET the yaw integrator"); phi_integrator_ = 0.0;
    }
    // task: wait for jump callback
  }

  return getCommandFromTask(pos, right, yaw);
}

Eigen::Vector3d Planner::getCommandFromTask(Eigen::Vector2d pos, bool right, float yaw){
  Eigen::Vector3d cmd(0,0,0);
  Eigen::Vector2d v(0,0);

  if (task_state_ == 'p' || task_state_ == 'k'){
    // PD to the next point

    // zero PD because within a deadband of final point
    if (task_state_ == 'p' && Node::distanceBtwn( pos, trajectory_[trajectoryLength_-1]) < p_.proportionalDeadBand ){
      // PD but zero velocity
      cmd(0) = 0.0; cmd(1) = 0.0; cmd(2) = 0.0;
      xy_integrator_(0) = 0.0; xy_integrator_(1) = 0.0; phi_integrator_ = 0.0;
      if (!finished_traj_){
        finished_traj_ = true;
        publishEvent("traj_finished", 7);
      }
      if (p_.verboseFollowerEssential)
        ROS_INFO_THROTTLE(0.5, "At the goal, ZERO velocity");
    }
    else{
      // non-zero PD
      float phi_desired; 
      // a really simple solution
      if ( task_state_ == 'k' )
        phi_desired = jumpAngles_(0);
      else if (task_state_ == 'p')
        phi_desired = Geometry::angleToVector( trajectory_[trajectoryLength_-1] - trajectory_[trajectoryLength_-2] );
      if (task_state_ == 'p')
        ROS_WARN_THROTTLE(0.5, "phi_desired: %f, x_desired: %f, y_desired: %f", phi_desired, trajectory_[task_segment_+1](0), trajectory_[task_segment_+1](1) );

      if (p_.verboseFollowerEssential){
        if (task_state_ == 'p')
          ROS_INFO_THROTTLE(0.5, "At the goal, NON-ZERO velocity, ts: %i", task_segment_);
        else if ( Node::distanceBtwn( pos, trajectory_[task_segment_+1]) > p_.jumpStopDistRadius &&
          fabs(yaw -  phi_desired) > p_.jumpStopAngleRadius )
          ROS_INFO_THROTTLE(0.5, "At the Jump, non-zero PD, too far position and angle, ts: %i", task_segment_);
        else if (Node::distanceBtwn( pos, trajectory_[task_segment_+1]) > p_.jumpStopDistRadius)
          ROS_INFO_THROTTLE(0.5, "At the Jump, non-zero PD, too far position, ts: %i", task_segment_);
        else if (fabs(yaw -  phi_desired) > p_.jumpStopAngleRadius)
          ROS_INFO_THROTTLE(0.5, "At the Jump, non-zero PD, too far angle, ts: %i", task_segment_);
      }

      // desired angle is segment+1's orientation
      // THIS IS CORRECT, DON'T FIX IT; properAngleDiff is not the way
      float angleDelta = phi_desired - yaw;
      if ( fabs(angleDelta) > M_PI){
        if (angleDelta > 0)
          angleDelta += - 2*M_PI;
        else
          angleDelta += 2*M_PI;
      }

      cmd(2) = angleDelta * p_.yawScalingFactor / p_.yawTimeConstant + phi_integrator_;
      phi_integrator_ += angleDelta * p_.yawIntegratorGain;
      if (phi_integrator_ > p_.integratorYawMax) phi_integrator_ = p_.integratorYawMax;
      if (phi_integrator_ < -p_.integratorYawMax) phi_integrator_ = -p_.integratorYawMax;

      v = (trajectory_[task_segment_+1] - pos) * p_.proportionalGain + xy_integrator_;
      xy_integrator_ += (trajectory_[task_segment_+1] - pos) * p_.integratorGain;
      if (xy_integrator_(0) > p_.integratorXmax) xy_integrator_(0) = p_.integratorXmax;
      if (xy_integrator_(0) < -p_.integratorXmax) xy_integrator_(0) = -p_.integratorXmax;

      if (xy_integrator_(1) > p_.integratorYmax) xy_integrator_(1) = p_.integratorYmax;
      if (xy_integrator_(1) < -p_.integratorYmax) xy_integrator_(1) = -p_.integratorYmax;

      v = Geometry::rotate(v, -yaw);
      cmd(0) = v(0); cmd(1) = v(1); 
      
      ROS_INFO_THROTTLE(2.0, "\tIntegrators: phi %f, x %f, y %f.", phi_integrator_, xy_integrator_(0), xy_integrator_(1));
    }
  }
  else if (task_state_ == 'j'){
    if (!p_.sendJumpExtendedRequest){
      cmd(0) = 0; cmd(1) = 0; cmd(2) = 0;
    }
    else
      ROS_ERROR("NOT SUPPOSED TO BE HERE IN J");
  }
  else if (task_state_ == 'w'){
    // -------------------------------------------
    // THIRD
    // determine control

    // lateral towards the trajectory 
    float tubeRad, distToTraj;
    Eigen::Vector2d towardsTrajectory(0,0);
    distToTraj = Geometry::distToLine(trajLines_[task_segment_], pos);
    if (right)
      tubeRad = Geometry::tubeSizeAt( trajLines_[task_segment_], rightLines_[task_segment_], pos );
    else
      tubeRad = Geometry::tubeSizeAt( trajLines_[task_segment_], leftLines_[task_segment_], pos );
    tubeRad = std::max(p_.minTubeRad, tubeRad);
    distToTraj = std::min(distToTraj, tubeRad) - 0.001;
    

    if (distToTraj > p_.lateralDeadBand){
      towardsTrajectory = Geometry::getPerp( trajectory_[task_segment_], trajectory_[task_segment_+1] ) * distToTraj / tubeRad * p_.lateralScalingFactor; 
      if (!right)
        towardsTrajectory *= -1;
      if (right)
        ROS_INFO_THROTTLE(0.5, "I'M TO THE RIGHT, WALKING LEFT %i", task_segment_);
      else
        ROS_INFO_THROTTLE(0.5, "I'M TO THE LEFT, WALKING RIGHT %i", task_segment_);
    }
    

    // forward
    Eigen::Vector2d forward = Geometry::getParl( trajectory_[task_segment_], trajectory_[task_segment_+1] );
    Eigen::Vector2d next(0,0);
    Eigen::Vector2d desiredAngleVector = forward;
    float distToNext = Geometry::distBtwn( Geometry::perpOnLine( trajLines_[task_segment_], pos ), trajectory_[task_segment_+1] ) / p_.atDistToNext;
    int secondTrajPoint = std::min(task_segment_+2, trajectoryLength_ -1 );
    if (distToNext < 1){
      // if we are close to the next vector - do the fancy turning
      next = Geometry::getParl( trajectory_[secondTrajPoint-1], trajectory_[secondTrajPoint] );
      desiredAngleVector = (distToNext) * forward + (1-distToNext) * next ;

      forward *= distToNext; // scale forward
      next *= (1 - distToNext);
    }

    forward *= maxSpeedFromTube(tubeRad) * std::max( (tubeRad-distToTraj) / tubeRad , p_.scaleForward);
    next *= maxSpeedFromTube(tubeRad) * std::max( (tubeRad-distToTraj) / tubeRad , p_.scaleForward);

    v = towardsTrajectory + forward + next;
    v = Geometry::rotate(v, -yaw);
    cmd(0) = v(0); cmd(1) = v(1); 

    // THIS IS CORRECT, DON'T FIX IT; properAngleDiff is not the way
    float phi_desired = Geometry::angleToVector(desiredAngleVector);
    if ( fabs(phi_desired - yaw) > M_PI){
      if (phi_desired - yaw > 0)
        cmd(2) = (phi_desired - yaw - 2*M_PI) * p_.yawScalingFactor / p_.yawTimeConstant;
      else
        cmd(2) = (phi_desired - yaw + 2*M_PI) * p_.yawScalingFactor / p_.yawTimeConstant;
    }
    else
      cmd(2) = (phi_desired - yaw) * p_.yawScalingFactor / p_.yawTimeConstant;

    
    if (p_.verboseFollowerEssential){
      if (distToNext < 1)
        ROS_INFO_THROTTLE(0.5, "Following along, w/ next edge, ts: %i", task_segment_);
      else
        ROS_INFO_THROTTLE(0.5, "Following along one edge, ts: %i", task_segment_);
    }

    //check if we need to update the trajectory follower
    if (!p_.singleTrajectoryMode){
      if ( task_segment_ >= committed_ - 2 && !incrementRequestSent_){
        // update only if distance from committed to the goal is small
        if ( Node::distanceBtwn( trajectory_[committed_], goal_ ) > p_.goalRadius ){
          //send just one request not a thousand
          incrementRequestSent_ = true;
          // this needs to be a callback 
          std_msgs::Empty msg;
          pub_increment_req_.publish(msg);
        }
      }
    }
    // done
  }
  else{
    ROS_ERROR("unknown command type, ts: %i", task_segment_);
    std::cout << task_state_ << "\n";
  }
  return cmd;

}

void Planner::trajectoryFollowing(){
  if (p_.followerModeOn && plannerHasControlOfTheCheetah_){
    const ros::Time followerStart(ros::Time::now());
    trajectoryAccess_.lock();
    Eigen::Vector3d cmd(0,0,0);
    bool needFixing = true;
    try{
      cmd = getDesiredCMD(&needFixing);
    }
    catch(...){
      ROS_ERROR("ERROR ACQUIRING VELOCITY COMMAND, COMMANDING 0");
      LCM_comm::zeroOut();
      cmd = Eigen::Vector3d(0,0,0);
    }
    trajectoryAccess_.unlock();

    fixCMD(cmd, needFixing);

    if(p_.verboseFollower)
      std::cout << "cmd is\t" << last_cmd_(0) << "\t" << last_cmd_(1) << "\t" << last_cmd_(2) << "\n";

    LCM_comm::sendVelCMD(last_cmd_(0), last_cmd_(1), last_cmd_(2));
    geometry_msgs::Vector3Stamped cmdMsg;
    cmdMsg.header.stamp = ros::Time::now();
    cmdMsg.header.frame_id = "base_link";
    cmdMsg.vector.x = last_cmd_(0);
    cmdMsg.vector.y = last_cmd_(1);
    cmdMsg.vector.z = last_cmd_(2);
    pub_cmd_.publish( cmdMsg );

    const ros::Time followerEnd(ros::Time::now());
  }
}

void Planner::fixCMD(Eigen::Vector3d cmd, bool needFixing){
  if (needFixing){
    last_cmd_(0) = last_cmd_(0) * lpf_.x + cmd(0) * (1-lpf_.x);
    last_cmd_(1) = last_cmd_(1) * lpf_.y + cmd(1) * (1-lpf_.y);
    last_cmd_(2) = last_cmd_(2) * lpf_.yaw + cmd(2) * (1-lpf_.yaw);
  }
  else
    last_cmd_ = cmd;
}

// ----------------------------------------------------------------------------
// ros services and callbacks

bool Planner::follower(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res){
  if (!p_.followerModeOn){
    if (!LCM_comm::good()){
      ROS_ERROR("Something is off with LCM comm");
      return false;
    }
    if (!LCM_comm::isArmed()){
      ROS_WARN("CHEETAH NOT ARMED YET");
      return false;
    }

    publishEvent("follower_on", 2);
    p_.followerModeOn = true;
    plannerHasControlOfTheCheetah_ = true;
    task_state_ = 'w';
    task_segment_ = 0;
    ROS_INFO("STARTING TRAJECTORY FOLLOWER");
    
    LCM_comm::zeroOut();
    LCM_comm::zeroOut();

    status_->record_robot_path = true;

    if (p_.stop_map_recording_upon_follower)
      status_->update_global_map = false;
  }
  else{
    ROS_INFO("STOPPING TRAJECTORY FOLLOWER");
    if (LCM_comm::isArmed()){
      ROS_WARN("CHEETAH STILL ARMED");
    }
    p_.followerModeOn = false;
    LCM_comm::zeroOut();
    LCM_comm::zeroOut();


    if (p_.stop_map_recording_upon_follower)
      status_->update_global_map = true;

  }
      
  res.message = "COMPLETED";
  res.success = true;
  return true;
}

void Planner::userGoCallback(std_msgs::Bool msg){
  if (msg.data){
    user_says_go_ = true;
    ROS_WARN("USER SAYS GO");
  }
  else{
    user_says_go_ = false;
    ROS_WARN("USER SAYS DON'T GO");
  }
}

// ----------------------------------------------------------------------------
// goal update relatd

void Planner::publishGoalMarker(){
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.header.stamp = ros::Time::now();
  marker.ns = "goal";
  marker.id = 0;
  marker.type = visualization_msgs::Marker::CYLINDER;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = goal_(0);
  marker.pose.position.y = goal_(1);
  marker.pose.position.z = 0.0;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.03;
  marker.scale.y = 0.03;
  marker.scale.z = 1.0;
  marker.color.r = 1.0f;
  marker.color.g = 0.0f;
  marker.color.b = 0.0f;
  marker.color.a = 1.0;
  goal_pub_.publish(marker);

}

void Planner::publishDebugMarker( Eigen::Vector2d position ){
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.header.stamp = ros::Time::now();
  marker.ns = "debug";
  marker.id = 0;
  marker.type = visualization_msgs::Marker::CYLINDER;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = position(0);
  marker.pose.position.y = position(1);
  marker.pose.position.z = 0.0;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.03;
  marker.scale.y = 0.03;
  marker.scale.z = 1.0;
  marker.color.r = 0.0f;
  marker.color.g = 0.0f;
  marker.color.b = 1.0f;
  marker.color.a = 1.0;
  debug_marker_pub_.publish(marker);

}

void Planner::updateGoalPosition( const geometry_msgs::PoseStamped &msg ) {
  // stop the current trajectory follower
  trajectoryAccess_.lock();
  bool followerWasOn = false;
  if (p_.followerModeOn){
    followerWasOn = true;
    p_.followerModeOn = false;
    LCM_comm::sendVelCMD(0,0,0);
    LCM_comm::sendVelCMD(0,0,0);
    LCM_comm::sendVelCMD(0,0,0);
  }

  goal_(0) = msg.pose.position.x;
  goal_(1) = msg.pose.position.y;
  ROS_INFO("\t\t\t\tGoal position updated!");
  publishGoalMarker();

  // clear current trajectories
  trajectory_.clear();
  leftTube_.clear(); 
  rightTube_.clear();
  trajectoryLength_ = 0;

  trajectoryAccess_.unlock();
  // request a new trajectory with the firstTime handle
  // if trajectory acquired successfully - brilliant
  if (incrementCommittedTrajectory(true)){
    ROS_INFO( "TRAJECTORY ACQUIRED" );

    if (followerWasOn){
      p_.followerModeOn = true;
      ROS_INFO("FOLLOWER RESTARTED");
    }
  }
  
}

// ----------------------------------------------------------------------------
// initialization

void Planner::initParams(){
  nhp_.param<int>("planner/numNodes", p_.numNodes, 100);
  nhp_.param<float>("planner/runPeriod", p_.runPeriod, 1.0);

  nhp_.param<float>("planner/goalRadius", p_.goalRadius, 1.0);
  nhp_.param<float>("planner/steerDistance", p_.steerDistance, 1.0);
  nhp_.param<float>("planner/originSteerDistance", p_.originSteerDistance, 1.0);

  nhp_.param<int>("planner/maxSampleAttempts", p_.maxSampleAttempts, 100);
  nhp_.param<std::string>("planner/layer", p_.layer, "elevation");

  nhp_.param<int>("planner/varianceRadius", p_.varianceRadius, 1);
  nhp_.param<float>("planner/max_node_sd", p_.max_node_sd, 0.05);
  nhp_.param<float>("planner/max_traversability_delta", p_.max_traversability_delta, 0.05);
  nhp_.param<float>("planner/side_delta", p_.side_delta, 0.1);
  nhp_.param<int>("planner/num_deltas", p_.num_deltas, 2);

  nhp_.param<bool>("planner/allowNanInRobotsRadius", p_.allowNanInRobotsRadius, true);
  nhp_.param<float>("planner/robotsRadius", p_.robotsRadius, 0.4);

  nhp_.param<float>("planner/rrtStarRadius", p_.rrtStarRadius, 0.4);
  nhp_.param<bool>("planner/allowNANs", p_.allowNANs, true);

  nhp_.param<std::string>("planner/type", p_.type, "rrt*");

  nhp_.param<bool>("planner/nonClusterWalk", p_.nonClusterWalk, false);

  nhp_.param<float>("jump/height_delta", p_.jump_height_delta, 0.4);
  nhp_.param<float>("jump/distance", p_.jump_distance, 0.4);
  nhp_.param<float>("jump/jump_over_delta", p_.jump_over_delta, 0.4);

  nhp_.param<float>("walk/front_delta", p_.walk_front_delta, 0.05);
  
  nhp_.param<float>("robot/width", p_.robot_width, 0.4);
  nhp_.param<float>("robot/length", p_.robot_length, 0.4);

  nhp_.param<float>("jump/sampling/perp", p_.jump_sampling_dist_perp, 0.4);
  nhp_.param<float>("jump/sampling/parl", p_.jump_sampling_dist_parl, 0.4);
  nhp_.param<float>("jump/sampling/angle", p_.jump_sampling_angle, 0.4);

  nhp_.param<int>("jump/sampling/num_samples", p_.num_jump_samples, 0.4);

  nhp_.param<bool>("planner/trajectoryMaintenance", p_.trajectoryMaintenance, false);
  nhp_.param<bool>("planner/setFatherWithShortCutting", p_.setFatherWithShortCutting, false);

  nhp_.param<float>("planner/maxTrajectoryMaintenanceLength", p_.maxTrajectoryMaintenanceLength, 0.5);

  nhp_.param<bool>("planner/spit_traj", p_.spit_traj, false);
  nhp_.param<std::string>("planner/save_path", p_.save_path, "~/Desktop/");

  nhp_.param<float>("planner/minEdgeLength", p_.minEdgeLength, 0.5);
  nhp_.param<float>("planner/improvement/countUpRad", p_.countUpRad, 0.5);
  nhp_.param<float>("planner/improvement/minCountVal", p_.minCountVal, 0.5);
  nhp_.param<float>("planner/improvement/maxCountTimes", p_.maxCountTimes, 0.5);

  nhp_.param<float>("planner/trajFollowing/yawScalingFactor", p_.yawScalingFactor, 0.05);
  nhp_.param<float>("planner/trajFollowing/yawTimeConstant", p_.yawTimeConstant, 0.03);
  nhp_.param<float>("planner/trajFollowing/lateralScalingFactor", p_.lateralScalingFactor, 2.0);
  nhp_.param<float>("planner/trajFollowing/lateralDeadBand", p_.lateralDeadBand, 0.05);
  nhp_.param<float>("planner/trajFollowing/forwardScalingFactor", p_.forwardScalingFactor, 0.3);
  nhp_.param<float>("planner/trajFollowing/atDistToNext", p_.atDistToNext, 0.25);
  nhp_.param<float>("planner/trajFollowing/nextScalingFactor", p_.nextScalingFactor, 0.3);
  nhp_.param<float>("planner/trajFollowing/proportionalGain", p_.proportionalGain, 0.5);
  nhp_.param<float>("planner/trajFollowing/proportionalDeadBand", p_.proportionalDeadBand, 0.05);


  nhp_.param<bool>("planner/singleTrajectoryMode", p_.singleTrajectoryMode, false);
  nhp_.param<float>("planner/trajFollowing/followerPeriod", p_.followerPeriod, 0.01);

  nhp_.param<bool>("planner/verbose/follower/explicit", p_.verboseFollower, false);
  nhp_.param<bool>("planner/verbose/improvement", p_.verboseImprovement, false);
  nhp_.param<bool>("planner/verbose/trajectory", p_.verboseTrajectory, false);

  nhp_.param<float>("planner/p1", p_.p1, 1.0);
  nhp_.param<float>("planner/p2", p_.p2, 0.5);
  nhp_.param<float>("planner/p3", p_.p3, 0.5);
  nhp_.param<float>("planner/p4", p_.p4, 0.5);
  nhp_.param<float>("planner/p5", p_.p5, 0.5);
  nhp_.param<float>("planner/sd", p_.sd, 2.0);
  nhp_.param<float>("planner/rotAngle", p_.rotAngle, 0.0);

  nhp_.param<float>("planner/maxV1", p_.maxV1, 0.5);
  nhp_.param<float>("planner/maxV2", p_.maxV2, 0.5);
  nhp_.param<float>("planner/maxV3", p_.maxV3, 0.5);
  nhp_.param<float>("planner/maxV4", p_.maxV4, 0.5);

  nhp_.param<bool>("planner/simplifiedTraversabilityCheck", p_.simplifiedTraversabilityCheck, true);

  nhp_.param<float>("planner/maxEdgeLength", p_.maxEdgeLength, 0.5);
  nhp_.param<float>("planner/minEdgeLength", p_.minEdgeLength, 0.5);
  nhp_.param<float>("planner/singleEdgeLimit", p_.singleEdgeLimit, 0.5);
  nhp_.param<float>("planner/doubleEdgeLimit", p_.doubleEdgeLimit, 0.5);
  nhp_.param<float>("planner/tripleEdgeLimit", p_.tripleEdgeLimit, 0.5);

  nhp_.param<int>("planner/numRRTattempts", p_.numRRTattempts, 1);
  nhp_.param<float>("planner/singleTrajTimeout", p_.singleTrajTimeout, 0.5);

  nhp_.param<float>("planner/originRad", p_.originRad, 0.5);
  nhp_.param<float>("planner/commitIncrement", p_.commitIncrement, 0.5);

  nhp_.param<float>("planner/maxTubeSize", p_.maxTubeSize, 0.5);

  nhp_.param<bool>("jump/allowJumpOnSameSurface", p_.allowJumpOnSameSurface, true);

  nhp_.param<bool>("planner/trajFollowing/usePDminSpeed", p_.usePDminSpeed, false);
  nhp_.param<float>("planner/trajFollowing/minPDSpeed", p_.minPDSpeed, 0.13);

  nhp_.param<float>("jump/stop/distRadius", p_.jumpStopDistRadius, 0.1);
  nhp_.param<float>("jump/stop/andgleRadius", p_.jumpStopAngleRadius, 0.1);

  nhp_.param<float>("jump/wait_before", p_.jump_wait_before, 0.5);
  nhp_.param<float>("jump/wait_after", p_.jump_wait_after, 0.5);

  nhp_.param<bool>("stop_map_recording_upon_follower", p_.stop_map_recording_upon_follower, false);

  nhp_.param<float>("jump/max_rmse", p_.jump_max_rmse, 0.05);

  


  nhp_.param<bool>("planner/verbose/follower/essential", p_.verboseFollowerEssential, false);

  nhp_.param<float>("integrator_gain_xy", p_.integratorGain, 0.01);
  nhp_.param<float>("integrator_gain_yaw", p_.yawIntegratorGain, 0.01);

  nhp_.param<float>("terrain_param/sideYDist", p_.terrain_param_sideYDist, 0.45);
  nhp_.param<float>("terrain_param/xFrontExtra", p_.terrain_param_xFrontExtra, 0.5);
  nhp_.param<float>("terrain_param/c", p_.terrain_param_c, 65.0);
  nhp_.param<float>("terrain_param/resolution", p_.terrain_param_resolution, 0.025);
  nhp_.param<float>("terrain_param/maxDiff", p_.terrain_param_maxDiff, 0.05);
  nhp_.param<float>("terrain_param/landingWeight", p_.terrain_param_landingWeight, 2.0);

  nhp_.param<bool>("RRTCONNECT", p_.RRTCONNECT, false);

  nhp_.param<float>("jump/num_samples_resolution", p_.jump_num_samples_resolution, 2.0);

  nhp_.param<float>("planner/evalTerrain_x", p_.evalTerrain_x, 2.0);
  nhp_.param<float>("planner/evalTerrain_y", p_.evalTerrain_y, 2.0);

  nhp_.param<float>("acceptableRad", p_.acceptableRad, 0.15);

  // nhp_.param<float>("jump/angleResolution", p_.angleResolution, 15.0);
  nhp_.param<float>("jump/minDistToObstacle", p_.minDistToObstacle, 0.15);
  nhp_.param<float>("jump/minDistFromObstacle", p_.minDistFromObstacle, 0.15);
  nhp_.param<float>("jump/maxDistFromObstacle", p_.maxDistFromObstacle, 0.7);
  nhp_.param<float>("jump/rangeDistResolution", p_.rangeDistResolution, 0.7);

  nhp_.param<float>("jump/angleRange", p_.angleRange, 80.0);
  p_.angleRange = p_.angleRange * M_PI/180;

  nhp_.param<float>("jump/angleToEdges", p_.angleToEdges, 80.0);
  p_.angleToEdges = p_.angleToEdges * M_PI/180;

  nhp_.param<int>("jump/numAngles", p_.numAngles, 80.0);

  nhp_.param<float>("jump/distBackOffMultiplier", p_.distBackOffMultiplier, 1.0);
  nhp_.param<float>("jump/constraintReduction", p_.constraintReduction, 0.05);

  nhp_.param<float>("jump/yawVSDistScalingFactor", p_.yawVSDistScalingFactor, 1.0);
  nhp_.param<float>("alphaShapeRadius", p_.alphaShapeRadius, 0.5);
  nhp_.param<float>("robustnessMetricTieRad", p_.robustnessMetricTieRad, 0.5);
  

  nhp_.param<bool>("jump/doTwoJumpsChecks", p_.doTwoJumpsChecks, false);

  nhp_.param<bool>("reset_integrators", p_.reset_integrators, false);

  nhp_.param<float>("lpf/x", lpf_.x, 0.1);
  nhp_.param<float>("lpf/y", lpf_.y, 0.1);
  nhp_.param<float>("lpf/yaw", lpf_.yaw, 0.1);

  nhp_.param<float>("planner/trajFollowing/scaleForward", p_.scaleForward, 0.1);


  nhp_.param<float>("jump/integratorXmax", p_.integratorXmax, 0.1);
  nhp_.param<float>("jump/integratorYmax", p_.integratorYmax, 0.1);
  nhp_.param<float>("jump/integratorYawMax", p_.integratorYawMax, 0.1);

  nhp_.param<float>("minTubeRad", p_.minTubeRad, 0.1);

  nhp_.param<bool>("planner/quiet", p_.quiet, false);
  nhp_.param<bool>("rrt/quiet", p_.quiet_rrt, false);
  nhp_.param<bool>("planner/log_inside", p_.log_inside, false);
  

  nhp_.param<std::string>("jump/tablePath", p_.tablePath, "table.csv");
  
}

bool Planner::getTerrain(cheetah_msgs::srv_float::Request &req, cheetah_msgs::srv_float::Response &res){
  // ANGLE WITH RESPECT TO THE ROBOT
  // for -60, i.e. obstacle is clockwise from the rbot, to the right:
  // rosservice call /cheetah/terrain 300 
  // for 60, i.e. obstacle is counterclockwise from the obrot, to the left:
  // rosservice call /cheetah/terrain 60
  jumpAngles_ = Eigen::Vector2d(0,0);
  distPastObst_ = 0.3;
  evaluateTerrainAroundRobot(req.data*M_PI/180);
  res.message = "SENDING TERRAIN"; 
  res.success = true;
  return true;
}

bool Planner::floatArrService(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res){
  // HERE IS HOW YOU CALL THIS THING:
  // rosservice call /cheetah/floatArrSrv "[4,0.25,1.0,0.5,0.8]"
  // rosservice call /cheetah/floatArrSrv "[0.0, 0.5, 0.0, 0.88, 0.0, 0.2]"
  auto t_start = std::chrono::high_resolution_clock::now();
  if ( req.data[0] < 1-0.1 ){
    // float distInit, float yawInit, float distTerm, float yawTerm, float height
    // rosservice call /cheetah/floatArrSrv "[0.0, 0.5, 0.0, 0.88, 0.0, 0.2]"
    evaluateSVM(req.data[1], req.data[2], req.data[3], req.data[4], req.data[5], true);
  }




  else if (req.data[0] < 2-0.1){
    // rosservice call /cheetah/floatArrSrv "[1.0, 0.75, 320]"
    float dist_in_front = req.data[1];
    float des_orientation = Geometry::getProperAngle( req.data[2] * M_PI/180 );
    trajectoryFollowingTest(dist_in_front, des_orientation);
  }



  else if (req.data[0] < 3-0.1){
    // rosservice call /cheetah/floatArrSrv "[2.0, 0.4, 0.0, 0.2]"

    float distTerm = req.data[1];
    float yawTerm = Geometry::getProperAngle(req.data[2]*M_PI/180);
    float height = req.data[3];
    float jumpDirAngle = 0.0, prevDirAngle = 0.0;
    float angleResolution = 2*p_.angleRange / (p_.numAngles-1);

    std::vector<bool> acceptedInitial; acceptedInitial.reserve(p_.numAngles);
    std::vector<float> initialAngles; initialAngles.reserve(p_.numAngles);
    std::vector<Eigen::Vector2d> initialRanges; initialRanges.reserve(p_.numAngles);
    mapbox::geometry::polygon<double> polygon;

    for (int i = 0; i < p_.numAngles; i++){
      acceptedInitial.push_back( true );
      initialAngles.push_back(Geometry::getProperAngle(-p_.angleRange + angleResolution * i + jumpDirAngle));
      initialRanges.push_back( Eigen::Vector2d( 0.2, 0.9 ) );
    }
    if (! findPolygon( jumpDirAngle, prevDirAngle, yawTerm, distTerm, height, acceptedInitial, initialAngles, initialRanges, &polygon ) ){
      ROS_ERROR("NO SOLUTION");
    }
    else{
      // get the best point
      mapbox::geometry::point<double> newPoint = mapbox::polylabel(polygon, 0.01);
      float distInit = newPoint.y;
      float yawInit = newPoint.x * p_.yawVSDistScalingFactor; 
      double newMetric = mapbox::detail::pointToPolygonDist(newPoint, polygon);
      float radYaw, radDist;
      radDist = newMetric;
      radYaw = newMetric * p_.yawVSDistScalingFactor;
      std::cout << "bestPoint = [" << distInit << "," << yawInit << "," << radDist << "," << radYaw << "];\n";
    }
  }





  auto t_end = std::chrono::high_resolution_clock::now();
  std::cout << "\nEvaluation took " << std::chrono::duration<float, std::milli>(t_end-t_start).count() << " ms" << "\n\n";

  res.message = "COMPLETED";
  res.success = true;

  return true;
}

std::vector< std::vector<float> > Planner::read_csv(std::string fileName, int rowLength){
  // NOTE: when passing file mames in ros, you must pass an absolute file path!
  // i want to be row major
  std::ifstream file(fileName);
  if(!file.is_open()) throw std::runtime_error("Could not open file");

  std::vector<std::vector<float> > dataList;
  std::string line = "";
  // Iterate through each line and split the content using delimeter
  while (getline(file, line))
  {
    std::vector<std::string> vec;
    // TO DO: that second thing shoudl really be an std::array
    std::vector<float> floatVec;
    boost::algorithm::split(vec, line, boost::is_any_of(",") );
    for(int i=0; i<vec.size();i++){
      floatVec.push_back(std::stof(vec[i]));
    }
    if ( floatVec.size() != rowLength ) throw std::runtime_error("Row length doesn't match appropriate size.");

    dataList.push_back(floatVec);
  }
  // Close the File
  file.close();
  return dataList;
}

void Planner::grabSVMData( ){
  std::string line = "";
  // i want 4 files:  
  std::ifstream fParam( p_.tablePath + "1_param.csv" );
  if(!fParam.is_open()) throw std::runtime_error("Could not open param file");
  getline(fParam, line);
  std::vector<std::string> vec;
  boost::algorithm::split(vec, line, boost::is_any_of(",") );
  svm_.Scale = std::stof(vec[0]);
  svm_.Bias = std::stof(vec[1]);
  fParam.close();

  std::ifstream fAlpha( p_.tablePath + "1_alpha.csv" );
  if(!fAlpha.is_open()) throw std::runtime_error("Could not open alpha file");
  while (getline(fAlpha, line)) {
    std::vector<std::string> vec;
    boost::algorithm::split(vec, line, boost::is_any_of(",") );
    svm_.Alpha.push_back(std::stof(vec[0]));
  }
  fAlpha.close();

  std::ifstream flabels( p_.tablePath + "1_labels.csv" );
  if(!flabels.is_open()) throw std::runtime_error("Could not open label file");
  while (getline(flabels, line)) {
    std::vector<std::string> vec;
    boost::algorithm::split(vec, line, boost::is_any_of(",") );
    svm_.SVLabels.push_back(std::stof(vec[0]));
  }
  flabels.close();

  std::ifstream fsv( p_.tablePath + "1_sv.csv" );
  if(!fsv.is_open()) throw std::runtime_error("Could not open sup-vec file");
  Eigen::Matrix<float, 1, 5> pnt;
  while (getline(fsv, line)) {
    std::vector<std::string> vec;
    boost::algorithm::split(vec, line, boost::is_any_of(",") );
    pnt(0) = std::stof(vec[0]); pnt(1) = std::stof(vec[1]); pnt(2) = std::stof(vec[2]); pnt(3) = std::stof(vec[3]); pnt(4) = std::stof(vec[4]);
    svm_.SV.push_back( pnt );
  }
  fsv.close();
}



}
