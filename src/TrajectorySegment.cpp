/**
 * @file TrajectorySegment.cpp
 * @brief RRT Trajectory Segment implementation
 * @author Savva Morozov <savva@mit.edu>
 * @date 27 Jan 2021
 */


#include "cheetah_navigation/TrajectorySegment.h"

namespace cheetah {

TrajectorySegment::TrajectorySegment(){
  inited_ = false;
}

TrajectorySegment::TrajectorySegment(grid_map::Position start, grid_map::Position finish, char type)
{
  inited_ = true;
  type_ = type;
  startPos_ = start;
  finishPos_ = finish;
}

void TrajectorySegment::set(grid_map::Position start, grid_map::Position finish, char type){
  inited_ = true;
  type_ = type;
  startPos_ = start;
  finishPos_ = finish;
}

void TrajectorySegment::swap(){
  Eigen::Vector2d temp = startPos_;
  startPos_ = finishPos_;
  finishPos_ = temp;
}

bool TrajectorySegment::inited(){
  return inited_;
}

bool TrajectorySegment::isJump(){
  if ( inited_ && type_ == 'j')
      return true;
  return false;
}

bool TrajectorySegment::isWalk(){
  if ( inited_ && type_ == 'w')
      return true;
  return false;
}

void TrajectorySegment::reset(){
  inited_ = false;
}

}
