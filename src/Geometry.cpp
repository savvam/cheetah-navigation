/**
 * @file Geometry.cpp
 * @brief ROS wrapper for planning (RRT)
 * @author Savva Morozov <savva@mit.edu>
 * @date 20 Jan 2021
 */


#include "cheetah_navigation/Geometry.h"

namespace cheetah {

Eigen::Vector2d Geometry::rotate(Eigen::Vector2d vector, float angle){
  Eigen::Vector2d res;
  res(0) = std::cos(angle) * vector(0) - std::sin(angle)*vector(1);
  res(1) = std::sin(angle) * vector(0) + std::cos(angle)*vector(1);
  return res;
}

float Geometry::distBtwn(Eigen::Vector2d p1, Eigen::Vector2d p2){
  return (p1 - p2).norm();
}

void Geometry::makeAngleProper(float & angle){
  while ( angle > M_PI )
    angle -= 2*M_PI;
  while ( angle < -M_PI )
    angle += 2*M_PI;
}

float Geometry::getProperAngle(float angle){
  float new_angle = angle;
  while ( new_angle > M_PI )
    new_angle -= 2*M_PI;
  while ( new_angle < -M_PI )
    new_angle += 2*M_PI;
  return new_angle;
}

float Geometry::properAngleDiff( float angle1, float angle2 ){
  // assuming that both angles are proper
  float diff = std::fabs(angle1 - angle2);
  if (diff <= M_PI)
    return diff;
  else if (diff < 2*M_PI)
    return 2*M_PI - diff;
  else{
    ROS_ERROR("properAngleDiff got improper angles as inputs! %f %f", angle1, angle2);
    return -57.0;
  }
}


float Geometry::norm(Eigen::Vector2d p){
  return p.norm();
}

Eigen::Vector2d Geometry::getParl(Eigen::Vector2d p1, Eigen::Vector2d p2){
  return (p2-p1) / (p2-p1).norm();
}

Eigen::Vector2d Geometry::getPerp(Eigen::Vector2d p1, Eigen::Vector2d p2){
  return rotate( (p2-p1) / (p2-p1).norm(), M_PI/2 );
}

Eigen::Vector2d Geometry::getPerpFromParl(Eigen::Vector2d parl){
  return rotate( parl, M_PI/2 );
}

Eigen::Vector3d Geometry::lineThroughTwoPoints(Eigen::Vector2d p1, Eigen::Vector2d p2){
  Eigen::Vector3d res;
  res(0) = p1(1) - p2(1); // y1-y2
  res(1) = p2(0) - p1(0); // x2-x1
  res(2) = p1(0) * p2(1) - p2(0) * p1(1); // x1y2 - x2y1
  return res;
}

bool Geometry::linesIntersect(Eigen::Vector3d line1, Eigen::Vector3d line2){
  if ( line1(1) * line2(0) - line2(1) * line1(0) < 0.001)
    return false;
  return true;
}

Eigen::Vector2d Geometry::lineIntersection(Eigen::Vector3d line1, Eigen::Vector3d line2){
  // assumption: lines intersect
  Eigen::Vector2d res;
  res(0) = ( line1(2) * line2(1) - line2(2)*line1(1) ) / ( line1(1) * line2(0) - line2(1) * line1(0) );
  res(1) = ( line2(2) * line1(0) - line1(2)*line2(0) ) / ( line1(1) * line2(0) - line2(1) * line1(0) );
  return res;
}

bool Geometry::colinear(Eigen::Vector2d p1, Eigen::Vector2d p2, Eigen::Vector2d p3){
  if ( fabs( p2(0) - p1(0)) < 0.001 ){
    if ( fabs( p3(0) - p2(0) < 0.001 ) )
      return true;
    return false;
  }
  else{
    if ( fabs( p3(0) - p2(0)) < 0.001  )
      return false;
    if ( fabs( ( p2(1)-p1(1) )/( p2(0)-p1(0) ) - ( p3(1)-p2(1) )/( p3(0)-p2(0) ) ) < 0.001 )
      return true;
    else
      return false;
  }
}



Eigen::Vector3d Geometry::linePerpThroughPoint( Eigen::Vector3d line, Eigen::Vector2d p ){
  Eigen::Vector3d perpLine;
  perpLine(0) = line(1);
  perpLine(1) = -line(0);
  perpLine(2) = line(0) * p(1) - line(1) * p(0);
  return perpLine;
}

float Geometry::distToLine( Eigen::Vector3d line, Eigen::Vector2d p ){
  return fabs( line(0)*p(0) + line(1)*p(1) + line(2) ) / std::sqrt( line(0)*line(0) + line(1)*line(1) );
}

bool Geometry::isRight( Eigen::Vector2d a, Eigen::Vector2d b, Eigen::Vector2d test ){
  return ( (b(0) - a(0))*(test(1) - a(1)) - (b(1) - a(1))*(test(0) - a(0)) ) <= 0;
}

bool Geometry::isLeft( Eigen::Vector2d a, Eigen::Vector2d b, Eigen::Vector2d test ){
  return ( (b(0) - a(0))*(test(1) - a(1)) - (b(1) - a(1))*(test(0) - a(0)) ) >= 0;
}

Eigen::Vector2d Geometry::perpOnLine( Eigen::Vector3d line, Eigen::Vector2d p){
  Eigen::Vector2d res;
  float denom = line(0)*line(0) + line(1)*line(1);
  res(0) = line(1) * ( line(1) * p(0) - line(0) * p(1) ) - line(0)*line(2);
  res(0) = res(0) / denom;
  
  res(1) = line(0) * ( -line(1) * p(0) + line(0) * p(1) ) - line(1)*line(2);
  res(1) = res(1) / denom;
  return res;
}

float Geometry::angleBetweenTwoVectors( Eigen::Vector2d a, Eigen::Vector2d b ){
  float angle = std::acos(   a.dot(b) / ( a.norm() * b.norm() )  );
  if (a(0)*b(1) - b(0)*a(1) < 0)
    angle = 2*M_PI - angle;
  return angle;
}

float Geometry::angleToVector( Eigen::Vector2d a ){
  return std::atan2( a(1), a(0) );
  // float angle = std::atan(   a(1) / a(0)  );
  // if ( a(1) > 0 && a(0) < 0 )
  //   angle += M_PI;
  // else if ( a(1) < 0 && a(0) < 0 )
  //   angle -= M_PI;
  // return angle;
}


float Geometry::tubeSizeAt( Eigen::Vector3d trajLine, Eigen::Vector3d tubeLine, Eigen::Vector2d p ){
  // Eigen::Vector3d perpLine = linePerpThroughPoint(trajLine, p);
  // Eigen::Vector2d pointOnTube = lineIntersection(perpLine, tubeLine);
  // return distToLine(trajLine, pointOnTube);
  return distToLine(trajLine, lineIntersection( linePerpThroughPoint(trajLine, p), tubeLine) );
}

float Geometry::pointToTubeRatio( Eigen::Vector3d trajLine, Eigen::Vector3d tubeLine, Eigen::Vector2d p ){
  return distToLine(trajLine, p) / tubeSizeAt(trajLine, tubeLine, p);
}

float Geometry::evalPointOnLine( Eigen::Vector2d p, Eigen::Vector3d line ){
  return line(0) * p(0) + line(1) * p(1) + line(2);
}

Eigen::Vector3d Geometry::getQPConstraintValues( Eigen::Vector2d p, Eigen::Vector3d line ){
  if ( evalPointOnLine(p, line) > 0.00001 ){
    return Eigen::Vector3d( -line(0), -line(1), line(2) );
  }
  else if ( evalPointOnLine(p, line) < -0.00001 ){
    return Eigen::Vector3d( line(0), line(1), -line(2) );
  }
  else{
    ROS_ERROR( "QP CONSTRAINT PASSED POINT ON THE LINE CAN'T EVAL" );
    return line;
  }
}

float Geometry::getDistToEllipse( float a, float b, Eigen::Vector2d p ){
  //  a - semi major axis, b - semi minor axis, 
  float px = std::fabs( p[0] );
  float py = std::fabs( p[1] );
  float t = M_PI/4;
  float x, y, ex, ey, rx, ry, qx, qy, r, q, delta_c, delta_t;
  for (int i = 0; i < 3; i++){
    x = a * std::cos(t);
    y = b * std::sin(t);
    ex = (a*a - b*b) * pow(std::cos(t), 3) / a ;
    ey = (b*b - a*a) * pow(std::sin(t), 3) / b ;
    rx = x - ex;
    ry = y - ey;
    qx = px - ex;
    qy = py - ey;
    r = sqrt( rx*rx + ry*ry );
    q = sqrt( qx*qx + qy*qy );

    delta_c = r * asin((rx*qy - ry*qx)/(r*q));
    delta_t = delta_c / sqrt(a*a + b*b - x*x - y*y);

    t += delta_t;
    t = std::min(float(M_PI/2), std::max(float(0.0), float(t) ) ); // this bastard don't change this line
  }

  return sqrt( (x-px)*(x-px) + (y-py)*(y-py) );
  // return (math.copysign(x, p[0]), math.copysign(y, p[1]));
}


} // ns cheetah