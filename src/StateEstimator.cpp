/**
 * @file StateEstimator.cpp
 * @brief ROS wrapper for state estimation, odometry, and path components
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */


#include "cheetah_navigation/StateEstimator.h"

namespace cheetah {

StateEstimator::StateEstimator(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, tf2_ros::Buffer *tf_buffer)
: nh_(nh), nhp_(nhp), status_(status), tf_buffer_(tf_buffer)
{
  initParams();
  ROS_INFO( "STARTING THE STATE ESTIMATOR" );

  if (seParams_.num_odom_sources == 0){
    ROS_INFO( "No odometry source provided - paths are not created." );
    ROS_ERROR( "OI MATE HOW EXACTLY ARE YOU GONNA RUN WITHOUT ODOMETRY?" );
  }
  else{
    ROS_INFO("%i odometry sources provided.", seParams_.num_odom_sources );
    for (unsigned i=0; i < seParams_.odom_frames.size(); i++){
      std::string frame = seParams_.odom_frames[i];
      std::string topic = seParams_.odom_topics[frame];
      sub_odom_[frame] = nh_.subscribe(topic, 1, &StateEstimator::odomCallback, this);
      pub_path_[frame] = nh_.advertise<nav_msgs::Path>( "path_" + frame , 1, true);

      ROS_INFO( "Subscribed to %s", topic.c_str());
      ROS_INFO( "Publishing a path to path_%s", frame.c_str());

      timeOfLastPathPose_[frame] = ros::Time::now();
      path_[frame] = nav_msgs::Path();
    }

    pub_odom_ = nh_.advertise<geometry_msgs::PoseStamped>( "base_link_odom", 5, true);
    timeOfLastOdom_ = ros::Time::now();

    srv_recordPath_ = nh_.advertiseService("recPath", &StateEstimator::recordPath, this);
    srv_resetPath_ = nh_.advertiseService("resetPath", &StateEstimator::resetPath, this);

    pathTimer_ = nh_.createTimer(ros::Duration( pParams_.path_publish_period ), 
                                std::bind(&StateEstimator::publishPath, this));

    nhp_.param<bool>("c1632/recT265", recT265_, false);
    // DON"T CHANGE - IRRELEVANT
    if (recT265_){
      float t265Period = 0.01;
      nhp_.param<float>("c1632/recT265_period", t265Period, 0.01);
      pub_t265_1632_ = nh_.advertise<cheetah_msgs::t265>( "t265_1632", 1, true);
      recOdomTimer_ = nh_.createTimer(ros::Duration( t265Period ), 
                                std::bind(&StateEstimator::recordOdom, this));
      ROS_INFO("Recording odometry every %f seconds", t265Period);
    }
  }

  tim_tf_broadcast_ = nh_.createTimer(ros::Duration( 0.02 ), std::bind(&StateEstimator::timerTFworld, this));

  // sub_world_ = nh_.subscribe(seParams_.vicon_pose_topic, 1, &StateEstimator::viconPoseCallback, this);
  // srv_calibrate_ = nh_.advertiseService("calibWorld", &StateEstimator::calibrateOdomToWorld, this);
  // by default, world to map transform is null

  estimatedPath_ = nav_msgs::Path();
  viconPath_ = nav_msgs::Path();
  recEstimatedTimer_ = nh_.createTimer(ros::Duration( 0.1 ),  std::bind(&StateEstimator::recordEstimatedPath, this));
  recViconTimer_ = nh_.createTimer(ros::Duration( 0.1 ),  std::bind(&StateEstimator::recordViconPath, this));
  pubEstimatedTimer_ = nh_.createTimer(ros::Duration( 1.0 ),  std::bind(&StateEstimator::publishEstimatedPath, this));
  pubViconTimer_ = nh_.createTimer(ros::Duration( 1.0 ),  std::bind(&StateEstimator::publishViconPath, this));
  pubEstimatedPath_ = nh_.advertise<nav_msgs::Path>( "estimated_path" , 1, true);
  pubViconPath_ = nh_.advertise<nav_msgs::Path>( "vicon_path" , 1, true);


  float elev; nhp_.param<float>("non_vicon_robot_elevation", elev, 0.3);
  world_to_map_.header.frame_id = "world";
  world_to_map_.child_frame_id = "map";
  world_to_map_.transform.translation.x = 0.0;
  world_to_map_.transform.translation.y = 0.0;
  world_to_map_.transform.translation.z = elev;

  world_to_map_.transform.rotation.x = 0;
  world_to_map_.transform.rotation.y = 0;
  world_to_map_.transform.rotation.z = 0;
  world_to_map_.transform.rotation.w = 1;
  timerTFworld();
  ROS_INFO( "----------");

}

void StateEstimator::timerTFworld(){
  world_to_map_.header.stamp = ros::Time::now();
  tf_broadcaster_.sendTransform(world_to_map_);
}

// ----------------------------------------------------------------------------
void StateEstimator::odomCallback(const nav_msgs::Odometry& msg) {

    // -----------------------------------------
    // FIRST: use this measurement to publish proper odometry

    ros::Time stampReceived = ros::Time::now();

    std::string frame = msg.child_frame_id;
    std::string frames_base_link = seParams_.odom_links[frame];
    // so we don't actually need to use this incoming odom message, 
    // we can use tf2 to do the trasnformation math for us
    geometry_msgs::TransformStamped odom_to_base;
    try{
      // this doesn't always yield the most-most-most recent pose; this is also sort of dumb
      odom_to_base = tf_buffer_->lookupTransform("odom", frames_base_link, ros::Time(0) ); // ros::Time(0): get most recent
    }
    catch (tf2::TransformException &ex) {
      // either something is properly wrong, or it's one of the first few measurements
      ROS_ERROR("COULDN'T GET THE ODOM TRANSFORM:\n%s",ex.what());
      return;
    }

    // use the tf2 to calculate proper odometry
    calculateOdom(odom_to_base); 

    // -----------------------------------------
    // put the odometry into the path if neccessary
    if (status_->record_path){
      // did enough time pass since the last time we recorded a measurement?
      ros::Duration since_last = ros::Time::now() - timeOfLastPathPose_[frame];
      if (since_last.toSec() > pParams_.path_increment_period){
        // convert trasnform into a pose; this loses variance data,
        // but t265 variance data us useless (static) anyway 
        geometry_msgs::PoseStamped pose;
        pose.header = odom_to_base.header;
        // TO DO: also uncivilized
        pose.pose.position.x = odom_to_base.transform.translation.x;
        pose.pose.position.y = odom_to_base.transform.translation.y;
        pose.pose.position.z = odom_to_base.transform.translation.z;

        pose.pose.orientation.x = odom_to_base.transform.rotation.x;
        pose.pose.orientation.y = odom_to_base.transform.rotation.y;
        pose.pose.orientation.z = odom_to_base.transform.rotation.z;
        pose.pose.orientation.w = odom_to_base.transform.rotation.w;

        path_[frame].header = odom_to_base.header;
        path_[frame].poses.push_back(pose);
        timeOfLastPathPose_[frame] = ros::Time::now();
      }
    }
  }

// ----------------------------------------------------------------------------

void StateEstimator::viconPoseCallback(const geometry_msgs::PoseStamped& msg){
  // std::cout << "got a vicon callback\n";
  // if (need_calibration_){
  //   need_calibration_ = false;
  //   world_to_map_.transform.translation.x = msg.pose.position.x;
  //   world_to_map_.transform.translation.y = msg.pose.position.y;
  //   world_to_map_.transform.translation.z = msg.pose.position.z;

  //   world_to_map_.transform.rotation.x = msg.pose.orientation.x;
  //   world_to_map_.transform.rotation.y = msg.pose.orientation.y;
  //   world_to_map_.transform.rotation.z = msg.pose.orientation.z;
  //   world_to_map_.transform.rotation.w = msg.pose.orientation.w;
  //   tf_broadcaster_.sendTransform(world_to_map_);
  // }
}


void StateEstimator::calibrateWorldToMap(){
  geometry_msgs::TransformStamped world_to_cheetah;
  try{
    world_to_cheetah = tf_buffer_->lookupTransform("world", seParams_.cheetah_frame, ros::Time(0) ); // ros::Time(0): get most recent
  }
  catch (tf2::TransformException &ex) {
    ROS_ERROR("COULDN'T GET THE TRANSFORM TO CHEETAH:\n%s",ex.what());
    return;
  }
  world_to_map_ = world_to_cheetah;
  world_to_map_.header.frame_id = "world";
  world_to_map_.child_frame_id = "map";
  timerTFworld();
  

  ROS_WARN("CALIBRATED WORLD TO MAP");
}

void StateEstimator::testCalibration(){
  if (need_calibration_){
    need_calibration_ = false;
    world_to_map_.transform.translation.x = 0.5;
    world_to_map_.transform.translation.y = -0.9;
    world_to_map_.transform.translation.z = 0.2;

    world_to_map_.transform.rotation.x = 0;
    world_to_map_.transform.rotation.y = 0;
    world_to_map_.transform.rotation.z = 0;
    world_to_map_.transform.rotation.w = 1;
    tf_broadcaster_.sendTransform(world_to_map_);
  }
}

void StateEstimator::recordOdom(){
  if (recT265_){
    // grab the transforms
    geometry_msgs::TransformStamped odom_to_base1, odom_to_base2;
    cheetah_msgs::t265 ros_msg;
    ros_msg.header.stamp = ros::Time::now();
    ros_msg.header.frame_id = "t265";
    ROS_WARN_THROTTLE(2.0, "PUBLISHING T265s.");

    try{
      odom_to_base1 = tf_buffer_->lookupTransform("odom", seParams_.odom_links["t1_pose_frame"], ros::Time(0) ); // ros::Time(0): get most recent
      tf2::Quaternion quat(
      odom_to_base1.transform.rotation.x,
      odom_to_base1.transform.rotation.y,
      odom_to_base1.transform.rotation.z,
      odom_to_base1.transform.rotation.w);
      tf2::Matrix3x3 mat(quat);
      double roll, pitch, yaw;
      mat.getRPY(roll, pitch, yaw);

      std_msgs::Float64MultiArray array; array.data.clear();
      array.data.push_back(odom_to_base1.transform.rotation.x); array.data.push_back(odom_to_base1.transform.rotation.y);
      array.data.push_back(odom_to_base1.transform.rotation.z); array.data.push_back(odom_to_base1.transform.rotation.w);
      ros_msg.t1_quat = array;

      array.data.clear();
      array.data.push_back(roll); array.data.push_back(pitch); array.data.push_back(yaw); 
      ros_msg.t1_rpy = array;

      array.data.clear();
      array.data.push_back(odom_to_base1.transform.translation.x); array.data.push_back(odom_to_base1.transform.translation.y); 
      array.data.push_back(odom_to_base1.transform.translation.z); 
      ros_msg.t1_pose = array;
    }
    catch (tf2::TransformException &ex) {
      ROS_ERROR_THROTTLE(2.0, "COULDN'T GET FIRST CAMERA.");
    }

    try{
      odom_to_base2 = tf_buffer_->lookupTransform("odom", seParams_.odom_links["t2_pose_frame"], ros::Time(0) ); // ros::Time(0): get most recent
      tf2::Quaternion quat(
      odom_to_base2.transform.rotation.x,
      odom_to_base2.transform.rotation.y,
      odom_to_base2.transform.rotation.z,
      odom_to_base2.transform.rotation.w);
      tf2::Matrix3x3 mat(quat);
      double roll, pitch, yaw;
      mat.getRPY(roll, pitch, yaw);

      std_msgs::Float64MultiArray array; array.data.clear();
      array.data.push_back(odom_to_base2.transform.rotation.x); array.data.push_back(odom_to_base2.transform.rotation.y);
      array.data.push_back(odom_to_base2.transform.rotation.z); array.data.push_back(odom_to_base2.transform.rotation.w);
      ros_msg.t2_quat = array;

      array.data.clear();
      array.data.push_back(roll); array.data.push_back(pitch); array.data.push_back(yaw); 
      ros_msg.t2_rpy = array;

      array.data.clear();
      array.data.push_back(odom_to_base2.transform.translation.x); array.data.push_back(odom_to_base2.transform.translation.y); 
      array.data.push_back(odom_to_base2.transform.translation.z); 
      ros_msg.t2_pose = array;
    }
    catch (tf2::TransformException &ex) {
      ROS_ERROR_THROTTLE(2.0, "COULDN'T GET SECOND CAMERA.");
    }

    // publish
    pub_t265_1632_.publish(ros_msg);
  }
}

void StateEstimator::publishEstimatedPath(){
  if (status_->record_robot_path)
    pubEstimatedPath_.publish(estimatedPath_);
}

void StateEstimator::recordEstimatedPath(){
  if (status_->record_robot_path){
    geometry_msgs::TransformStamped odom_to_rec;
    try{
      odom_to_rec = tf_buffer_->lookupTransform("world", "base_link", ros::Time(0) ); // ros::Time(0): get most recent
    }
    catch (tf2::TransformException &ex) {
      ROS_ERROR("COULDN'T GET THE REC TRANSFORM:\n%s",ex.what());
      return;
    }
    geometry_msgs::PoseStamped pose;
    pose.header = odom_to_rec.header;
    // TO DO: also uncivilized
    pose.pose.position.x = odom_to_rec.transform.translation.x;
    pose.pose.position.y = odom_to_rec.transform.translation.y;
    pose.pose.position.z = odom_to_rec.transform.translation.z;

    pose.pose.orientation.x = odom_to_rec.transform.rotation.x;
    pose.pose.orientation.y = odom_to_rec.transform.rotation.y;
    pose.pose.orientation.z = odom_to_rec.transform.rotation.z;
    pose.pose.orientation.w = odom_to_rec.transform.rotation.w;

    estimatedPath_.header = odom_to_rec.header;
    estimatedPath_.poses.push_back(pose);
  }
}

void StateEstimator::publishViconPath(){
  if (status_->vicon_inited && status_->record_robot_path)
    pubViconPath_.publish(viconPath_);
}

void StateEstimator::recordViconPath(){
  if (status_->vicon_inited && status_->record_robot_path){
    geometry_msgs::TransformStamped odom_to_rec;
    try{
      odom_to_rec = tf_buffer_->lookupTransform("world", seParams_.cheetah_frame, ros::Time(0) ); // ros::Time(0): get most recent
    }
    catch (tf2::TransformException &ex) {
      ROS_ERROR("COULDN'T GET THE REC TRANSFORM:\n%s",ex.what());
      return;
    }
    geometry_msgs::PoseStamped pose;
    pose.header = odom_to_rec.header;
    // TO DO: also uncivilized
    pose.pose.position.x = odom_to_rec.transform.translation.x;
    pose.pose.position.y = odom_to_rec.transform.translation.y;
    pose.pose.position.z = odom_to_rec.transform.translation.z;

    pose.pose.orientation.x = odom_to_rec.transform.rotation.x;
    pose.pose.orientation.y = odom_to_rec.transform.rotation.y;
    pose.pose.orientation.z = odom_to_rec.transform.rotation.z;
    pose.pose.orientation.w = odom_to_rec.transform.rotation.w;

    viconPath_.header = odom_to_rec.header;
    viconPath_.poses.push_back(pose);
  }
}



void StateEstimator::calculateOdom(const geometry_msgs::TransformStamped& msg){
  if (seParams_.use_single_odom == 0){
    // TO DO: come up with + implement multi source odometry
    ROS_ERROR("Combining odometry is not implemented yet!");
  }
  else{ 
    // pubish a transform between odom and base_link using the provided transform

    // long if statement that check if transform is for the odometry of interest:
    // odometry base link frame of __ odometry frame of __ (odometry source - 1) (-1 cause we aren't matlab lol).
    if (msg.child_frame_id == seParams_.odom_links[ seParams_.odom_frames[seParams_.use_single_odom - 1] ] ){
      geometry_msgs::TransformStamped msg2 = msg;
      msg2.child_frame_id = "base_link";
      msg2.header.stamp = ros::Time::now();
      tf_broadcaster_.sendTransform(msg2);
    }
  }

  if (status_->record_pose){
    ros::Duration since_last = ros::Time::now() - timeOfLastOdom_;
    if (since_last.toSec() > pParams_.path_increment_period){
      geometry_msgs::TransformStamped odom_to_rec;
      try{
        odom_to_rec = tf_buffer_->lookupTransform("world", "base_link", ros::Time(0) ); // ros::Time(0): get most recent
      }
      catch (tf2::TransformException &ex) {
        ROS_ERROR("COULDN'T GET THE REC TRANSFORM:\n%s",ex.what());
        return;
      }
      geometry_msgs::PoseStamped pose;
      pose.header = odom_to_rec.header;
      // TO DO: also uncivilized
      pose.pose.position.x = odom_to_rec.transform.translation.x;
      pose.pose.position.y = odom_to_rec.transform.translation.y;
      pose.pose.position.z = odom_to_rec.transform.translation.z;

      pose.pose.orientation.x = odom_to_rec.transform.rotation.x;
      pose.pose.orientation.y = odom_to_rec.transform.rotation.y;
      pose.pose.orientation.z = odom_to_rec.transform.rotation.z;
      pose.pose.orientation.w = odom_to_rec.transform.rotation.w;
      pub_odom_.publish(pose);
      timeOfLastOdom_ = ros::Time::now();
    }
  }
}

// ----------------------------------------------------------------------------

void StateEstimator::publishPath() {
  ros::Time time = ros::Time::now();
  for (unsigned i=0; i < seParams_.odom_frames.size(); i++){
    std::string frame = seParams_.odom_frames[i];
    pub_path_[frame].publish(path_[frame]);
  }
  ROS_DEBUG("Paths (timestamp %f) published.", time.toSec());
}

// ----------------------------------------------------------------------------

bool StateEstimator::recordPath(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
{
  if (req.data) {
    if (status_->record_path){
      res.message = "Already Recording Paths";
    }
    else{
      status_->record_path = true;
      res.message = "Began Recording Paths";
    }
  }
  else {
    if (!status_->record_path){
      res.message = "Already Not Recording Paths";
    }
    else{
      status_->record_path = false;
      res.message = "Stopped Recording Paths";
    }
  }
  res.success = true;
  return true;
}

// ----------------------------------------------------------------------------

void StateEstimator::initPaths(){
  for (unsigned i=0; i < seParams_.odom_frames.size(); i++){
    std::string frame = seParams_.odom_frames[i];
    timeOfLastPathPose_[frame] = ros::Time::now();
    path_[frame] = nav_msgs::Path();
  }
}

// ----------------------------------------------------------------------------

bool StateEstimator::resetPath(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
{
  if (req.data) {
    res.message = "Paths have been reset";
    initPaths();
  }
  else{
    res.message = "Wrong input";
  }
  res.success = true;
  return true;
}


bool StateEstimator::calibrateOdomToWorld(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
{
  if (req.data) {
    res.message = "Odom to world calibration has been requested";
    need_calibration_ = true;
  }
  else{
    res.message = "Wrong input";
  }
  res.success = true;
  return true;
}

// ----------------------------------------------------------------------------

void StateEstimator::initParams()
{
    // get state estimator parameters
    nhp_.param<int>("num_odom_sources", seParams_.num_odom_sources, 0.0); 
    nhp_.param<int>("use_single_odom", seParams_.use_single_odom, 0); 

    for(int i = 1; i <= seParams_.num_odom_sources; i++){
        std::string frame, topic;
        nhp_.param<std::string>("odom" + std::to_string(i) + "_frame", frame, "57"); 
        seParams_.odom_frames.push_back(frame);

        nhp_.param<std::string>("odom" + std::to_string(i) + "_base_link", topic, "57"); 
        seParams_.odom_links[frame] = topic;

        nhp_.param<std::string>("odom" + std::to_string(i) + "_topic", topic, "57"); 
        seParams_.odom_topics[frame] = topic;
    }
    nhp_.param<std::string>("vicon_pose_topic", seParams_.vicon_pose_topic, "57"); 
    nhp_.param<std::string>("cheetah_frame", seParams_.cheetah_frame, "57"); 
    ROS_INFO("STATE ESTIMATOR ACQUIRED FRAMES");


    

    // get path parameters
    nhp_.param<float>("path/increment_period", pParams_.path_increment_period, 57.0);
    nhp_.param<float>("path/publish_period", pParams_.path_publish_period, 57.0);
} 

// ----------------------------------------------------------------------------


}