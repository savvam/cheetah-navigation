/**
 * @file Navigation.cpp
 * @brief ROS wrapper for the overarching navigation stack
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */



#include "cheetah_navigation/Navigation.h"


namespace cheetah {

Navigation::Navigation(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{
  // fetch map parameters
  initParams();

  // lcm_comm_ = LCM_comm();

  // std::string updm;
  // nhp_.param<std::string>("lcm/updm", updm, "udpm://239.255.76.67:7667?ttl=1");

  // lcm_comm_ = LCM_comm(updm);

  // local map initalization - partial, the rest is done in Mapper constructor
  {
    if (mParams_.init_local_map){
      //define maps
      map_.setFrameId(mParams_.map_frame);
      map_.setGeometry( grid_map::Length(mParams_.local_map_x, mParams_.local_map_y), mParams_.local_map_resolution);
      //add layers

      float height;
      nhp_.param<float>("underneath/height", height, -0.25);
      map_.add("elevation", height); // map_.clear("elevation"); 
      map_.add("new_z", 0); map_.clear("new_z"); // new measurements // unused
      map_.add("new_z_occupancy", 0); map_.clear("new_z_occupancy"); // new measurements // unused
      map_.add("filtered_elevation", 0); map_.clear("filtered_elevation"); 
      map_.add("traversability", 0); map_.clear("traversability"); 
      map_.add("delta_t", 0.0); // unused

      ROS_INFO( "Created a local gridmap with size %f x %f m, resolution: %f m/cell (%i x %i cells).", map_.getLength().x(), map_.getLength().y(), map_.getResolution(), map_.getSize()(0), map_.getSize()(1));  
      status_.record_local_map = true; // update status
    }
    else{
      ROS_INFO("Local Map not requested.");
      status_.record_local_map = false; // update status
    }
  }

  // global map initalization - partial, the rest is done in Mapper constructor
  {
    if (mParams_.init_global_map){
      global_map_.setFrameId(mParams_.map_frame);
      global_map_.setGeometry( grid_map::Length(mParams_.global_map_x, mParams_.global_map_y), mParams_.global_map_resolution);
      // clearing asssigns NaN to the layer
      global_map_.add("elevation", 0); global_map_.clear("elevation"); 
      global_map_.add("new_z", 0); global_map_.clear("new_z"); // new measurements
      global_map_.add("new_z_occupancy", 0); global_map_.clear("new_z_occupancy"); // new measurements
      global_map_.add("filtered_elevation", 0); global_map_.clear("filtered_elevation"); 

      global_map_.add("delta_t", 0.0);

      global_map_.add("sd_nbhd", -1); //global_map_.clear("elevation");
      global_map_.add("cluster_inited", 0); global_map_.clear("cluster_inited");

      global_map_.add("dbscan_type", -1); 
      global_map_.add("cluster_tag", 0);

      // the following are only used for trajectory adjustment
      global_map_.add("cluster_gauss", 0);
      global_map_.add("gx", 0); 
      global_map_.add("gy", 0); 
      
      ROS_INFO( "Created a global gridmap with size %f x %f m, resolution: %f m/cell (%i x %i cells).", global_map_.getLength().x(), global_map_.getLength().y(), global_map_.getResolution(), global_map_.getSize()(0), global_map_.getSize()(1));
      status_.record_global_map = true; // update status
    }
    else{
      ROS_INFO("Global Map not requested.");
      status_.record_global_map = false; // update status
    }
    ROS_INFO( "----------");
  }

  // global map initalization - partial, the rest is done in Mapper constructor
  {
    if (mParams_.init_world_map){
      world_map_.setFrameId(mParams_.map_frame);
      world_map_.setGeometry( grid_map::Length(mParams_.world_map_x, mParams_.world_map_y), mParams_.world_map_resolution);
      // clearing asssigns NaN to the layer
      world_map_.add("elevation", 0); world_map_.clear("elevation"); 
      world_map_.add("new_z", 0); world_map_.clear("new_z"); // new measurements
      world_map_.add("new_z_occupancy", 0); world_map_.clear("new_z_occupancy"); // new measurements
      world_map_.add("filtered_elevation", 0); world_map_.clear("filtered_elevation"); 

      world_map_.add("delta_t", 0.0);

      ROS_INFO( "Created a world gridmap with size %f x %f m, resolution: %f m/cell (%i x %i cells).", world_map_.getLength().x(), world_map_.getLength().y(), world_map_.getResolution(), world_map_.getSize()(0), world_map_.getSize()(1));
      status_.record_world_map = true; // update status
    }
    else{
      ROS_INFO("World Map not requested.");
      status_.record_world_map = false; // update status
    }
    ROS_INFO( "----------");
  }

  nhp_.param<bool>("record_paths", status_.record_path, true);
  nhp_.param<bool>("record_pose", status_.record_pose, true);

  
  

  // advertise reset everything service
  srv_resetAll_ = nh_.advertiseService("resetAll", &Navigation::resetAll, this);

  srv_velCMD_ = nh_.advertiseService("velCMD", &Navigation::sendVelCMD, this);

  srv_arm_ = nh_.advertiseService("arm", &Navigation::arm, this);

  srv_verbose_lcm_ = nh_.advertiseService("verboseLCM", &Navigation::verboseLCM, this);

  // start the listener
  tf_listener_ptr_.reset(new tf2_ros::TransformListener(tf_buffer_));
  if (status_.vicon_inited){
    ros::Time temp = ros::Time::now();
    while ( (ros::Time::now() - temp).toSec() < 0.3 ){
      // do literally nothing 
    }
  }

  // start the state estimator
  stateEstimator_.reset(new StateEstimator(nh_, nhp_, &status_, &tf_buffer_ ) );
  if (status_.vicon_inited){
    ROS_WARN("REQUSTING A WORLD TO ODOM CALIBRATION");
    stateEstimator_->calibrateWorldToMap();
    ROS_WARN("CALIBRATED!");
  }
  else if (status_.vicon_test_calib){
    ROS_WARN("TEST CALIBRATION REQUESTED");
    stateEstimator_->need_calibration_ = true;
    stateEstimator_->testCalibration();
    ROS_WARN("CALIBRATED!");
  }
  else{
    ROS_WARN("VICON NOT INITED, USING DEFAULT ROBOT ELEVATION HEIGHT");
  }


  // start the mapper
  mapper_.reset(new Mapper(nh_, nhp_, &status_, &tf_buffer_, &map_, &global_map_, &world_map_ ) );

  bool initPlanner = true;
  nhp_.param<bool>("init_planner", initPlanner, true);
  // start the planner
  if (initPlanner) 
    planner_.reset(new Planner(nh_, nhp_, &status_, &global_map_, &tf_buffer_ ) );

  LCM_comm::setupPublishers( &nh_ );
  bool verbose_lcm = false;
  nhp_.param<bool>("lcm/verbose", verbose_lcm, false);
  if (verbose_lcm){
    LCM_comm::verbose();
    ROS_INFO("LCM IS VERBOSED");
  }
  else{
    LCM_comm::deverbose();
    ROS_INFO("LCM IS NOT VERBOSED");
  }

  bool rec = false;
  nhp_.param<bool>("c1632/recIMU", rec, false);
  LCM_comm::recIMU_ = rec;
  nhp_.param<bool>("c1632/recRF", rec, false);
  LCM_comm::recRF_ = rec;

  float lcmHandlePeriod = 0.001;
  nhp_.param<float>("lcm/handle_period", lcmHandlePeriod, 0.001);
  lcmHandleTimer_ = nh_.createTimer(ros::Duration( lcmHandlePeriod ), 
                                  std::bind(&Navigation::lcmTimerHandleCallback, this));

  // lcm::LCM * lcm_ = new lcm::LCM("udpm://239.255.76.67:7667?ttl=255");
  // LCM_handler handler_ = LCM_handler();
  // handler_.pub_jump_callbacks_ = nh_.advertise<std_msgs::Empty>( "lcm/jump_callback", 1, true);
  // lcm_->subscribe("JUMP_CALLBACK", &LCM_handler::jumpCallback, &handler_);









  ROS_INFO("Initialization complete!");
  ROS_INFO( "----------");

    
}

void Navigation::lcmTimerHandleCallback(){
  // because can't bind a callback to a function without an object
  LCM_comm::handle();
}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------


bool Navigation::resetAll(std_srvs::SetBool::Request &req,
                        std_srvs::SetBool::Response &res)
{
  if (req.data) {
    res.message = "Maps and paths have been reset";
    mapper_->clearTheMap();
    stateEstimator_->initPaths();
  }
  else{
    res.message = "Wrong input";
  }
  res.success = true;
  return true;
}

bool Navigation::arm(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
{
  if (req.data) {
    res.message = "ARMING: LCM is transmitting";
    LCM_comm::arm();
  }
  else{
    res.message = "DISARMING: LCM is not transmitting";
    LCM_comm::disarm();
  }
  res.success = true;
  return true;
}

bool Navigation::verboseLCM(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
{
  if (req.data) {
    LCM_comm::verbose();
    res.message = "LCM is verbosed";
  }
  else{
    LCM_comm::deverbose();
    res.message = "LCM is NOT verbosed";
  }
  res.success = true;
  return true;
}


// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

void Navigation::initParams()
{
  // fetch map parameters
  {
    nhp_.param<bool>("init_local_map", mParams_.init_local_map, false);
    nhp_.param<bool>("init_global_map", mParams_.init_global_map, false);
    nhp_.param<bool>("init_world_map", mParams_.init_world_map, false);

    nhp_.param<std::string>("map_frame", mParams_.map_frame, "57");
    nhp_.param<std::string>("tracking_frame", mParams_.tracking_frame, "57");

    nhp_.param<float>("grid_map/local_map_y", mParams_.local_map_y, 57.0);
    nhp_.param<float>("grid_map/local_map_x", mParams_.local_map_x, 57.0);
    nhp_.param<float>("grid_map/local_resolution", mParams_.local_map_resolution, 57.0);

    nhp_.param<float>("grid_map/global_map_y", mParams_.global_map_y, 57.0);
    nhp_.param<float>("grid_map/global_map_x", mParams_.global_map_x, 57.0);
    nhp_.param<float>("grid_map/global_resolution", mParams_.global_map_resolution, 57.0);

    nhp_.param<float>("grid_map/world_map_y", mParams_.world_map_y, 57.0);
    nhp_.param<float>("grid_map/world_map_x", mParams_.world_map_x, 57.0);
    nhp_.param<float>("grid_map/world_resolution", mParams_.world_map_resolution, 57.0);

    nhp_.param<float>("grid_map/publish_period", mParams_.map_publish_period, 1.0);
  }
  // fetch the status variable
  nhp_.param<bool>("pc/publishFilteredPc", status_.publish_filtered_pc, false);

  nhp_.param<bool>("vicon_inited", status_.vicon_inited, false);
  nhp_.param<bool>("vicon_test_calib", status_.vicon_test_calib, false);

  

  
}

// ----------------------------------------------------------------------------

bool Navigation::sendVelCMD(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res){
  // based on current implementation - resize to be the right size
  LCM_comm::sendVelCMD(req.data[0], req.data[1], req.data[2]);
  res.message = "COMPLETED";
  res.success = true;
  return true;
}



} // ns cheetah
