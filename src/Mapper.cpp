/**
 * @file <Mapper>.cpp
 * @brief ROS wrapper for state estimation, odometry, and path components
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */


// TO DO: need hole filling for global maps mate

#include "cheetah_navigation/Mapper.h"

namespace cheetah {

Mapper::Mapper(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, tf2_ros::Buffer *tf_buffer, grid_map::GridMap * map, grid_map::GridMap * global_map, grid_map::GridMap * world_map)
: nh_(nh), nhp_(nhp), status_(status), tf_buffer_(tf_buffer), map_(map), global_map_(global_map), world_map_(world_map)
{
  initParams();
  // pcManager_.setParams(pcParams_.pc_frames, pcParams_.maxTimeDeltaBtwnPC, pcParams_.num_pc_sources);
  ROS_INFO( "STARTING THE MAPPER" );

  // initialize point cloud sources 
  {

    if (pcParams_.num_pc_sources == 0){
      ROS_INFO( "No pointcloud sources provided - mapping is not used." );
    }
    else{
      ROS_INFO("%i point cloud sources provided.", pcParams_.num_pc_sources );
      for (unsigned i=0; i < pcParams_.pc_frames.size(); i++){
        std::string frame = pcParams_.pc_frames[i];
        std::string topic = pcParams_.pc_topics[frame];
        std::string map_insert_type;

        nhp_.param<std::string>("pc" + std::to_string(i+1), map_insert_type, "57");
        if ( map_insert_type == "both" ){
          ROS_INFO("BOTH for %s", topic.c_str());
          sub_pc_local_[frame] = nh_.subscribe(topic, 1, &Mapper::localPcCallback, this);
          pub_pc_local_[frame] = nh_.advertise<sensor_msgs::PointCloud2>(frame + std::string("/filtered"), 1, true);
          sub_pc_global_[frame] = nh_.subscribe(std::string("/cheetah/") + frame + std::string("/filtered"), 1, &Mapper::globalFilteredPcCallback, this);
        }
        else if ( map_insert_type == "local" ){
          ROS_INFO("Local only for %s", topic.c_str());
          sub_pc_local_[frame] = nh_.subscribe(topic, 1, &Mapper::localPcCallback, this);
        }
        else if ( map_insert_type == "global" ){
          ROS_INFO("Global only for %s", topic.c_str());
          sub_pc_global_[frame] = nh_.subscribe(topic, 1, &Mapper::globalUnfilteredPcCallback, this);
        }
        else{
          ROS_ERROR("COULDN'T SUBSCRIBE");
        }
      }
      pub_cluster_global_ = nh_.advertise<std_msgs::Empty>("cluster_request", 1, true);
      sub_cluster_global_ = nh_.subscribe("/cheetah/cluster_request", 1, &Mapper::globalClusterCallback, this);

      pub_commit_maintenance_req_ = nh_.advertise<std_msgs::Empty>("commit_maintenance_req", 1, true);
      // if (status_->publish_filtered_pc){
      //   pub_filtered_pc_ = nh_.advertise<sensor_msgs::PointCloud2>("filtered_pc", 1, true);
      //   ROS_INFO( "Publishing filtered pointclouds on topic:\t filtered_pc");
        // sub_filtered_pc_ = nh_.subscribe("/cheetah/filtered_pc", 1, &Mapper::subFilteredPC, this);
      // } 
    }
    ROS_INFO( "----------");

  }
  
  {

    if (mParams_.init_local_map){
      ROS_INFO( "Publishing local gridmap on topic: grid_map" );
      pub_gridmap_ = nh_.advertise<grid_map_msgs::GridMap>("grid_map", 1, true);

      mapTimer_ = nh_.createTimer(ros::Duration( mParams_.map_publish_period ), 
                                  std::bind(&Mapper::publishMap, this));
    } 
  }

  {
    if (mParams_.init_world_map){
      ROS_INFO( "Publishing world gridmap on topic: world_map" );
      pub_world_gridmap_ = nh_.advertise<grid_map_msgs::GridMap>("world_map", 1, true);
      if (!mParams_.init_local_map){
        mapTimer_ = nh_.createTimer(ros::Duration( mParams_.map_publish_period ), 
                                    std::bind(&Mapper::publishMap, this));
      }
    } 
  }

  {
    if (mParams_.init_global_map){
      ROS_INFO( "Publishing global gridmap on topic: global_grid_map");
      pub_global_gridmap_ = nh_.advertise<grid_map_msgs::GridMap>("global_grid_map", 1, true);

      // srv_recordGlobal_ = nh_.advertiseService("recGlobalMap", &Mapper::recordGlobalMap, this);
      // set up the timer but only if it wasn't set up already
      if (!mParams_.init_local_map && !mParams_.init_world_map){
        mapTimer_ = nh_.createTimer(ros::Duration( mParams_.map_publish_period ), 
                                    std::bind(&Mapper::publishMap, this));
      }
    } 

  }

  srv_changeOffset_ = nh_.advertiseService("changeOffset", &Mapper::changeOffset, this);

  ROS_INFO("------------");
  ROS_INFO("NOTE: pointcloud timestamps are delayed in practice; delta of %f seconds is subtracted", (float)m_.timestamp_delta/1000000000);
  


}

void Mapper::localPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
  if (pcParams_.verbosePC){
    ROS_INFO("LD435: ---------------------------");
    ROS_INFO("LD435: Local D435 received" ); 
  }
  const ros::WallTime callbackStart(ros::WallTime::now());

  // -----------------------------------------
  // FIRST: Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud.

  pcl::PCLPointCloud2 pcl_pc;
  pcl_conversions::toPCL(*msg, pcl_pc);

  // it's a pointer, bear this in mind
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudInput(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromPCLPointCloud2(pcl_pc, *pointCloudInput);

  // timestamp offsetting
  ros::Time timeStamp = msg->header.stamp;
  if (timeStamp.nsec >= m_.timestamp_delta){
    timeStamp.nsec -= m_.timestamp_delta;
  }
  else {
    timeStamp.sec -= 1;
    timeStamp.nsec += 1000000000 - m_.timestamp_delta;
  }

  const ros::WallTime afterConversion(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: CONVERSION took \t%f", (afterConversion-callbackStart).toSec() );

  int numPts = static_cast<int>(pointCloudInput->size());

  // -----------------------------------------
  // SECOND: Point cloud filtering
  if (m_.filterLocalPC)
    filterPointCloud(pointCloudInput, 1);

  const ros::WallTime afterFiltering(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: FILTERING from %i down to %i points took \t%f", numPts, static_cast<int>(pointCloudInput->size()),  (afterFiltering-afterConversion).toSec() );


  // -----------------------------------------
  // THIRD: transform the point cloud 
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudRotated(new pcl::PointCloud<pcl::PointXYZRGB>);
  // if ( !transformPointCloud(pointCloudInput, pointCloudInput, mParams_.map_frame) ){
  if ( !transformPointCloud(pointCloudInput, pointCloudRotated, mParams_.map_frame) ){
    //transform failed - tf2 couldn't fetch the tranformation. Time to stop execution.
    return;
  }

  const ros::WallTime afterTransforming(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: TRANSFORMING took \t%f", (afterTransforming-afterFiltering).toSec() );


  sensor_msgs::PointCloud2 filtered_ROS_pc;
  pcl::toROSMsg(*pointCloudRotated.get(), filtered_ROS_pc );
  pub_pc_local_[msg->header.frame_id].publish (filtered_ROS_pc);

  // ROS_WARN("LD435: SENT REQUEST TO GLOBAL");

  // -----------------------------------------
  // FOURTH: move the map
  grid_map::Position new_map_position;

  if (!getDisplacement(ros::Time(0), &new_map_position)){
    //transform failed - tf2 couldn't fetch the tranformation. Time to stop execution.
    ROS_ERROR("COUlDN'T GET MOVING TRANSFORM, EXITTING");
    return;
  }
  map_->move( new_map_position, globalNewRegions_ ); // move the map and grab the new values
  // zero out the new values appropriately (we don't want NaNs)
  for(std::vector<grid_map::BufferRegion>::iterator bufIt = globalNewRegions_.begin(); bufIt != globalNewRegions_.end(); bufIt++ )    {
    for (grid_map::SubmapIterator iterator(*map_, *bufIt); !iterator.isPastEnd(); ++iterator) {
      // map_->at("elevation", *iterator) = mParams_.localDefaultHeight;
      map_->at("elevation", *iterator) = 0.0;
    }
  }
  globalNewRegions_.clear(); // clean up
  
      
  const ros::WallTime afterMoving(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: MOVING/PUBLISHING took \t%f", (afterMoving-afterTransforming).toSec() );


  // -----------------------------------------
  // FIFTH: add the new points to the maps
  insertPointCloudLocal(pointCloudRotated, map_, "Local");

  const ros::WallTime afterInserting(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: INSERTION of %i points took \t%f", static_cast<int>(pointCloudRotated->size()),  (afterInserting-afterMoving).toSec() );
  // avInserting += (afterInserting-afterMoving).toSec();

  filterLocalMap();

  const ros::WallTime afterMapFiltering(ros::WallTime::now());
  if (pcParams_.verbosePC)
    ROS_INFO("LD435: type %i MAP FILTERING took \t%f", m_.localFilterType,  (afterMapFiltering-afterInserting).toSec() );

  traversabilityOverLocalMap();

  const ros::WallTime afterTrav(ros::WallTime::now());

  if (pcParams_.verbosePC)
    ROS_INFO("Traversability calculation took \t%f",  (afterTrav-afterMapFiltering).toSec() );
      
  // -----------------------------------------
  map_->setTimestamp(timeStamp.toNSec());
  // LCM_comm::sendMap(map_);
  LCM_comm::sendMap(map_, m_.send_local_size);

  const ros::WallTime afterSent(ros::WallTime::now());

  if (pcParams_.verbosePC)
    ROS_INFO("LD435: sending the map took \t%f", (afterSent-afterTrav).toSec() );

  const ros::WallDuration duration = ros::WallTime::now() - callbackStart;
  if (pcParams_.verbosePC){
    ROS_INFO("LD435: CALLBACK took \t%f", duration.toSec());
    ROS_INFO("LD435: -------------------------");
  }
}


bool Mapper::changeOffset(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res){
  // based on current implementation - resize to be the right size
  mParams_.elevationOffset = req.data[0];
  res.message = "new offset updated";
  res.success = true;
  return true;
}

// ----------------------------------------------------------------------------

void Mapper::globalFilteredPcCallback( const sensor_msgs::PointCloud2& msg ){
  if (!status_->update_global_map){
    // we aren't supposed to be recording them global maps
    return;
  }
  if (d435_mutex_.try_lock()){
    if (pcParams_.verbosePC){
      ROS_INFO("GD435: ---------------------------");
      ROS_INFO("GD435: Global D435 received" );
    }
    const ros::WallTime callbackStart(ros::WallTime::now());

    // -----------------------------------------
    // FIRST: Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud.
    pcl::PCLPointCloud2 pcl_pc;
    pcl_conversions::toPCL(msg, pcl_pc);

    // it's a pointer, bear this in mind
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudInput(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::fromPCLPointCloud2(pcl_pc, *pointCloudInput);
    // timestamp offsetting
    ros::Time timeStamp = msg.header.stamp;
    if (timeStamp.nsec >= m_.timestamp_delta){
      timeStamp.nsec -= m_.timestamp_delta;
    }
    else {
      timeStamp.sec -= 1;
      timeStamp.nsec += 1000000000 - m_.timestamp_delta;
    }

    // const ros::WallTime afterConversion(ros::WallTime::now());
    // ROS_INFO("GD435: CONVERSION took \t%f", (afterConversion-callbackStart).toSec() );

    // -----------------------------------------
    // FOURTH: move the map
    grid_map::Position new_map_position;
    //transform failed - tf2 couldn't fetch the tranformation. Time to stop execution.
    if (!getDisplacement(timeStamp, &new_map_position)){
      d435_mutex_.unlock();
      return;
    }

    // trasforms acquired successfully - move the map
    global_map_->move( new_map_position, globalNewRegions_ ); // move the map and grab the new values
    // zero out the new values appropriately (we don't want NaNs)
    for(std::vector<grid_map::BufferRegion>::iterator bufIt = globalNewRegions_.begin(); bufIt != globalNewRegions_.end(); bufIt++ )    {
      for (grid_map::SubmapIterator iterator(*global_map_, *bufIt); !iterator.isPastEnd(); ++iterator) {
        global_map_->at("sd_nbhd", *iterator) = -1;
        global_map_->at("dbscan_type", *iterator) = -1;
        global_map_->at("cluster_tag", *iterator) = 0;
      }
    }

    // const ros::WallTime afterMoving(ros::WallTime::now());
    // ROS_INFO("GD435: MOVING took \t%f", (afterMoving-afterConversion).toSec() );

    // -----------------------------------------
    // FIFTH: add the new points to the maps
    insertPointCloud(pointCloudInput, global_map_, "Global");

    // const ros::WallTime afterInserting(ros::WallTime::now());
    // ROS_INFO("GD435: INSERTION of %i points took \t%f", static_cast<int>(pointCloudInput->size()),  (afterInserting-afterMoving).toSec() );

    if ( mParams_.uAtStart && !originalUnderneathSet_){
      setUnderneathTerrain();
    }

    // -----------------------------------------
    global_map_->setTimestamp(timeStamp.toNSec());

    const ros::WallDuration duration = ros::WallTime::now() - callbackStart;
    if (pcParams_.verbosePC){
      ROS_INFO("GD435: CALLBACK took \t%f", duration.toSec());
      ROS_INFO("GD435: -------------------------");
    }

    d435_mutex_.unlock();
    d435_count_ += 1;
    d_count_ += 1;
    if ( d_count_ >= 3){
      std_msgs::Empty msg;
      pub_cluster_global_.publish(msg);
    }

  }
  else{
    if (pcParams_.verbosePC)
      ROS_WARN("GD435: GLOBAL RECEIVED D435, COULDN'T GET MUTEX");
  } 
}

// ----------------------------------------------------------------------------

void Mapper::globalUnfilteredPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
  if (!status_->update_global_map){
    // we aren't supposed to be recording them global maps
    return;
  }
  if (d455_mutex_.try_lock()){
    if (pcParams_.verbosePC){
      ROS_INFO("GD455: ---------------------------");
      ROS_INFO("GD455: GLOBAL D455 received" );
    }
    const ros::WallTime callbackStart(ros::WallTime::now());

    // -----------------------------------------
    // FIRST: Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud.

    pcl::PCLPointCloud2 pcl_pc;
    pcl_conversions::toPCL(*msg, pcl_pc);

    // it's a pointer, bear this in mind
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudInput(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::fromPCLPointCloud2(pcl_pc, *pointCloudInput);
    // timestamp offsetting
    ros::Time timeStamp = msg->header.stamp;
    if (timeStamp.nsec >= m_.timestamp_delta){
      timeStamp.nsec -= m_.timestamp_delta;
    }
    else {
      timeStamp.sec -= 1;
      timeStamp.nsec += 1000000000 - m_.timestamp_delta;
    }

    // const ros::WallTime afterConversion(ros::WallTime::now());
    // ROS_INFO("GD455: CONVERSION took \t%f", (afterConversion-callbackStart).toSec() );

    int numPts = static_cast<int>(pointCloudInput->size());

    // -----------------------------------------
    // SECOND: Point cloud filtering
    filterPointCloud(pointCloudInput, 2);

    
    // const ros::WallTime afterFiltering(ros::WallTime::now());
    // ROS_INFO("GD455: FILTERING from %i down to %i points took \t%f", numPts, static_cast<int>(pointCloudInput->size()),  (afterFiltering-afterConversion).toSec() );
    // avFiltering += (afterFiltering-afterConversion).toSec();


    // -----------------------------------------
    // THIRD: transform the point cloud 
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudRotated(new pcl::PointCloud<pcl::PointXYZRGB>);
    // if ( !transformPointCloud(pointCloudInput, pointCloudInput, mParams_.map_frame) ){
    if ( !transformPointCloud(pointCloudInput, pointCloudRotated, mParams_.map_frame) ){
      //transform failed - tf2 couldn't fetch the tranformation. Time to stop execution.
      d455_mutex_.unlock();
      return;
    }

    // const ros::WallTime afterTransforming(ros::WallTime::now());
    // ROS_INFO("GD455: TRANSFORMING took \t%f", (afterTransforming-afterFiltering).toSec() );

    // -----------------------------------------
    // FOURTH: move the map
    
    grid_map::Position new_map_position;
    //transform failed - tf2 couldn't fetch the tranformation. Time to stop execution.
    if (!getDisplacement(timeStamp, &new_map_position)){
      d455_mutex_.unlock();
      return;
    }
      

    // trasforms acquired successfully - move the map
    global_map_->move( new_map_position, globalNewRegions_ ); // move the map and grab the new values
    // zero out the new values appropriately (we don't want NaNs)
    for(std::vector<grid_map::BufferRegion>::iterator bufIt = globalNewRegions_.begin(); bufIt != globalNewRegions_.end(); bufIt++ )    {
      for (grid_map::SubmapIterator iterator(*global_map_, *bufIt); !iterator.isPastEnd(); ++iterator) {
        global_map_->at("sd_nbhd", *iterator) = -1;
        global_map_->at("dbscan_type", *iterator) = -1;
        global_map_->at("cluster_tag", *iterator) = 0;
      }
    }

    // const ros::WallTime afterMoving(ros::WallTime::now());
    // ROS_INFO("GD455: MOVING took \t%f", (afterMoving-afterTransforming).toSec() );

    // -----------------------------------------
    // FIFTH: add the new points to the maps
    insertPointCloud(pointCloudRotated, global_map_, "Global");


    if ( mParams_.uAtStart && !originalUnderneathSet_){
      setUnderneathTerrain();
    }

    // const ros::WallTime afterInserting(ros::WallTime::now());
    // ROS_INFO("GD455: INSERTION of %i points took \t%f", static_cast<int>(pointCloudRotated->size()),  (afterInserting-afterMoving).toSec() );

    // -----------------------------------------
    global_map_->setTimestamp(timeStamp.toNSec());


    const ros::WallDuration duration = ros::WallTime::now() - callbackStart;
    if (pcParams_.verbosePC){
      ROS_INFO("GD455: CALLBACK took \t%f", duration.toSec());
      ROS_INFO("GD455: -------------------------");
    }


    d455_mutex_.unlock();
    d455_count_ += 1;
    d_count_ += 1;

    // if ( d435_count_ >= 1 && d455_count_ >= 1 ){
    if ( d_count_ >= 3){
      //publish osmething
      std_msgs::Empty msg;
      pub_cluster_global_.publish(msg);
    }

  }
  else{
    if (pcParams_.verbosePC)
      ROS_WARN("GD455: GLOBAL RECEIVED D455, COULDN'T GET MUTEX");
  }
}

// ----------------------------------------------------------------------------

void Mapper::globalClusterCallback(std_msgs::Empty msg){
  if (! ( d_count_ >= pcParams_.collectBeforeClustering ) ){
    if (pcParams_.verbosePC)
      ROS_ERROR("GLOBAL: CALL TO CLUSTERING UNNECESSARY");
    return;
  }
  
  const ros::WallTime callbackStart(ros::WallTime::now());
  d435_mutex_.lock();
  d455_mutex_.lock();
  // ROS_INFO( "PROCESSED: total %i \t D435 %i \t D455 %i ", d_count_, d435_count_, d455_count_ );
  d_count_ = 0;
  d435_count_ = 0;
  d455_count_ = 0;

  // const ros::WallTime mutexAcquired(ros::WallTime::now());
  // ROS_INFO("GLOBAL: -------------------------");
  // ROS_INFO("GLOBAL: ACQUIRING mutexes took \t%f", (mutexAcquired-callbackStart).toSec() );

  // -----------------------------------------
  // SIXTH: filter the maps
  // filterAMap(global_map_, "Local");
  filterGlobalMap();

  // TO DO: move the map to most recent position

  clusterGlobalMap();
  std_msgs::Empty empty_msg;
  pub_commit_maintenance_req_.publish(empty_msg);  
  if (pcParams_.verbosePC)
    ROS_INFO("PUBLISHED TRAJECTORY MAINTENANCE REQUEST");

  // const ros::WallTime finished(ros::WallTime::now());
  // ROS_INFO("GLOBAL: FILTERING and CLUSTERING took \t%f", (finished-mutexAcquired).toSec() );
  // ROS_INFO("GLOBAL: -------------------------");


  // do cleaning
  global_map_->clear("new_z"); 
  global_map_->clear("new_z_occupancy"); 

  d455_mutex_.unlock();
  d435_mutex_.unlock();
}
// ----------------------------------------------------------------------------


void Mapper::setUnderneathTerrain(){

  // extract yaw of the robot
  geometry_msgs::TransformStamped odom_to_base;
  try{
    odom_to_base = tf_buffer_->lookupTransform("world", "base_link", ros::Time(0) ); // ros::Time(0): get most recent
  }
  catch (tf2::TransformException &ex) {
    // either something is properly wrong, or it's one of the first few measurements
    ROS_ERROR("PLANNER COULDN'T GET THE STATE TRANSFORM:\n%s",ex.what());
    return;
  }
  // extracting yaw from the quaternion
  Eigen::Vector2d pos( odom_to_base.transform.translation.x, odom_to_base.transform.translation.y );
  tf2::Quaternion quat(
        odom_to_base.transform.rotation.x,
        odom_to_base.transform.rotation.y,
        odom_to_base.transform.rotation.z,
        odom_to_base.transform.rotation.w);
  tf2::Matrix3x3 mat(quat);
  double roll, pitch, yaw; // don't care about roll / pitch
  mat.getRPY(roll, pitch, yaw);

  Eigen::Vector2d v_parl(1,0);
  v_parl = Geometry::rotate(v_parl, yaw); // we wanna put stuff relative to the robot's direction
  Eigen::Vector2d v_perp = Geometry::rotate(v_parl, M_PI/2);

  // inserting map piece right underneath the robot
  grid_map::Polygon underPol;
  underPol.setFrameId(global_map_->getFrameId());
  grid_map::Position temp = pos;
  temp += mParams_.uFrontDist * v_parl + mParams_.uSideDist * v_perp; underPol.addVertex(temp);
  temp += (-2*mParams_.uSideDist * v_perp); underPol.addVertex(temp);
  temp += (- (mParams_.uFrontDist + mParams_.uBackDist ) * v_parl); underPol.addVertex(temp);
  temp += (2*mParams_.uSideDist * v_perp); underPol.addVertex(temp);

  for (grid_map::PolygonIterator iterator(*global_map_, underPol); !iterator.isPastEnd(); ++iterator){
    global_map_->at("new_z", *iterator) = mParams_.uHeight ;
    global_map_->at("new_z_occupancy", *iterator) = 1.0 ;
    global_map_->at("delta_t", *iterator) = m_.t_inc; // increase temporal certainty
  }
    

  originalUnderneathSet_ = true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Mapper::clusterGlobalMap(){
  const ros::WallTime methodStartTime(ros::WallTime::now());
  global_map_->clear("cluster_inited");
  
  // one last pass; goal of this pass is to cluster the points into planes

  cluster_count_ = 1; // added a ten to distinguish the colors from 0
  cluster_size_ = 0;
  for (grid_map::GridMapIterator it(*global_map_); !it.isPastEnd(); ++it) {
    // we are only interested in non-NAN points
    if ( global_map_->isValid( *it, "elevation") ){
      // we only need to expand points that haven't been expanded yet - NANs
      if ( ! global_map_->isValid( *it, "cluster_inited") ){

        if (! expandClusterPoint(*it)){
          // returned false - this means that the point is a noise point
          global_map_->at("dbscan_type", *it) = 0; //noise
          global_map_->at("cluster_inited", *it) = 1; // point inited
          global_map_->at("cluster_tag", *it) = 0; // belongs to noise cluster tag
        }
        else{
          // another cluster has been initialized
          // check is this cluster is too small to be considered a cluster
          if (cluster_size_ < m_.minimum_cluster_size){
            removeCluster(*it);
            // ROS_WARN("CLUSTER %i TOO SMALL, removing", cluster_count_);
          }
          else{
            cluster_count_ += 1; // nope it's large alright, incrment cluster count
          }
          
          cluster_size_ = 0; // reset cluster size
        }
      }

    }
  }

  const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
}

// ----------------------------------------------------------------------------

void Mapper::removeCluster(grid_map::Index index){
  // if core - deleteCluster on all the points around us
  // if not core - 

  // case 1 - point is core
  if ( global_map_->at("dbscan_type", index) > 1.9 ) {
    // make the point into noise
    global_map_->at("dbscan_type", index) = 0;
    global_map_->at("cluster_tag", index) = 0;

    // for each neighbor
    grid_map::Index nb_index;
    grid_map::Position pos;
    global_map_->getPosition(index, pos);
    for(int i = -1; i <= 1; i++){
      for(int j = -1; j <= 1; j++){
        if (i != 0 || j != 0){
          if ( global_map_->getIndex(pos + grid_map::Position(i*mParams_.global_map_resolution, j*mParams_.global_map_resolution), nb_index) ){
            // if part of the same cluster - deleteCluster on it
            if ( std::fabs(global_map_->at("cluster_tag", nb_index) - cluster_count_) < 0.01 )
              removeCluster(nb_index);
          }
          // nb_index(0) = index(0)+i; nb_index(1) = index(1)+j;
        }
      }
    }
  }
  else{
    // case 2 - points not core (border). Make into noise 
    global_map_->at("dbscan_type", index) = 0;
    global_map_->at("cluster_tag", index) = 0;
  }
}

// ----------------------------------------------------------------------------

bool Mapper::expandClusterPoint(grid_map::Index index){
  // if core - expandClusterPoint on all the points around us
  // if not core - return false; 
  // note that in this case whoever is receiving false must init the point

  grid_map::Index nb_index;
  grid_map::Position pos;
  global_map_->getPosition(index, pos);
  // this isn't a new point, we don't need to do any checks
  if ( !global_map_->isValid(index, "new_z_occupancy")) {
    if ( global_map_->at("dbscan_type", index) > 1.9) { // it is a core point
      global_map_->at("cluster_tag", index) = cluster_count_; // change its cluster type
      global_map_->at("cluster_inited", index) = 1; // initialize
    }
    else{
      return false; // not a core - return false, per usual
    }
  }
  else{ // this is a new point, give it proper treatment
    int count = 0;
    for(int i = -1; i <= 1; i++){
      for(int j = -1; j <= 1; j++){
        if ( global_map_->getIndex(pos + grid_map::Position(i*mParams_.global_map_resolution, j*mParams_.global_map_resolution), nb_index) ){
          // nb_index(0) = index(0)+i; nb_index(1) = index(1)+j;
          // every point nearby must be: valid, sd must be low, height diff must be low
          // including yourself
          if ( !global_map_->isValid(nb_index, "sd_nbhd") ||
            global_map_->at("sd_nbhd", nb_index) > m_.cluster_sd_max ||
            std::fabs( global_map_->at("elevation", nb_index) - global_map_->at("elevation", index) ) > m_.cluster_height_delta_max ){
            count+=1;
            if (count > 9 - m_.minimum_cluster_neighbors){
              return false;
            }
          }
        } 
      }
    }
    // great, all pints in the vicinity match the requirements to be consider a core point!
    global_map_->at("cluster_inited", index) = 1;
    global_map_->at("dbscan_type", index) = 2; // core type
    global_map_->at("cluster_tag", index) = cluster_count_;
  }

  // this is a core point - add it to the cluster size
  cluster_size_ += 1;
  
  // process all the points recursively
  for(int i = -1; i <= 1; i++){
    for(int j = -1; j <= 1; j++){
      if (i != 0 || j != 0){
        if ( global_map_->getIndex(pos + grid_map::Position(i*mParams_.global_map_resolution, j*mParams_.global_map_resolution), nb_index) ){
        // nb_index(0) = index(0)+i; nb_index(1) = index(1)+j;
          if ( global_map_->isValid(nb_index, "sd_nbhd") &&
            global_map_->at("sd_nbhd", nb_index) < m_.cluster_sd_max &&
            std::fabs( global_map_->at("elevation", nb_index) - global_map_->at("elevation", index) ) < m_.cluster_height_delta_max ){
            
            // if unclassified (aka not inited on this run)
            if ( !global_map_->isValid(nb_index, "cluster_inited") ) {
              // if ( global_map_->at("dbscan_type", nb_index ) < -0.9){
              // expand the unclassified point
              if (!expandClusterPoint(nb_index)){ // if not core
                global_map_->at("dbscan_type", nb_index) = 1; // make it a border type
                global_map_->at("cluster_tag", nb_index) = cluster_count_; // it's now part of a cluster
                global_map_->at("cluster_inited", nb_index) = 1;
                cluster_size_ += 1; // core will add itself
              }
            }  // else if it's been inited (whenever we init we set dbscan type to 0-1-2)
            else {
              // if it's unclassified, something is wrong
              // if (global_map_->at("dbscan_type", nb_index ) < -0.1){
              //   ROS_ERROR("Inited point is unclassified?");
              // } 
              // if it's noise, make it into a cluster part
              if (global_map_->at("dbscan_type", nb_index ) < 0.1){ 
                global_map_->at("dbscan_type", nb_index) = 1; // make it into the border type
                global_map_->at("cluster_tag", nb_index) = cluster_count_; // it's part of a cluster
                cluster_size_ += 1;
              }
            }
            // both an unclassified or a noise point now belong to our cluster
          }
        }
      }
    }
  }
  return true;

}

// ----------------------------------------------------------------------------

bool Mapper::getDisplacement(ros::Time timeStamp, grid_map::Position *position) {
  geometry_msgs::TransformStamped map_to_tracking;
  try{
    // ros::Time(0) - most recent
    map_to_tracking = tf_buffer_->lookupTransform(mParams_.map_frame, mParams_.tracking_frame, timeStamp, ros::Duration(0.001) );
  }
  catch (tf2::TransformException &ex) {
    // either something is properly wrong, or it's one of the first few measurements
    ROS_ERROR("COULDN'T GET THE MAPPING TRANSFORM:\n%s", ex.what());
    return false;
  }
  *position = grid_map::Position(map_to_tracking.transform.translation.x, map_to_tracking.transform.translation.y);
  return true;
}

// ----------------------------------------------------------------------------

void Mapper::publishMap(){
  ros::Time time = ros::Time::now();
  // recording the local map
  if (status_->record_local_map){
    grid_map_msgs::GridMap msg;
    grid_map::GridMapRosConverter::toMessage(*map_, msg); 
    pub_gridmap_.publish(msg);
    ROS_DEBUG("Local Grid map (timestamp %f) published.", msg.info.header.stamp.toSec());
  }
  // recording the global map 
  if (status_->record_global_map){
    grid_map_msgs::GridMap msg2;
    grid_map::GridMapRosConverter::toMessage(*global_map_, msg2); 
    pub_global_gridmap_.publish(msg2);
    ROS_DEBUG("Global grid map (timestamp %f) published.", msg2.info.header.stamp.toSec());
  }

  // recording the global map 
  if (status_->record_world_map){
    grid_map_msgs::GridMap msg;
    grid_map::GridMapRosConverter::toMessage(*world_map_, msg); 
    pub_world_gridmap_.publish(msg);
    ROS_DEBUG("World grid map (timestamp %f) published.", msg.info.header.stamp.toSec());
  }
}

// ----------------------------------------------------------------------------

bool Mapper::transformPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud,
                                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudTransformed,
                                  const std::string& targetFrame) {
  const ros::WallTime methodStartTime(ros::WallTime::now());

  ros::Time timeStamp;
  //timestamp offsetting
  timeStamp.fromNSec(1000 * pointCloud->header.stamp);
  if (timeStamp.nsec >= m_.timestamp_delta){
    timeStamp.nsec -= m_.timestamp_delta;
  }
  else
  {
    timeStamp.sec -= 1;
    timeStamp.nsec += 1000000000 - m_.timestamp_delta;
  }

  const std::string inputFrameId(pointCloud->header.frame_id); //frame ID of the pointcloud

  geometry_msgs::TransformStamped transformTf;
  try{
    transformTf = tf_buffer_->lookupTransform(targetFrame, inputFrameId, timeStamp, ros::Duration(0.001) );
  }
  catch (tf2::TransformException &ex) {
    ROS_ERROR("COULDN'T GET THE POINTCLOUD TRANSFORM:\n%s",ex.what());
    return false;
  }

  // TO DO: this is uncivilized, write a covert method for it
  geometry_msgs::Pose pose;
  pose.position.x = transformTf.transform.translation.x;
  pose.position.y = transformTf.transform.translation.y;
  pose.position.z = transformTf.transform.translation.z;
  pose.orientation.x = transformTf.transform.rotation.x;
  pose.orientation.y = transformTf.transform.rotation.y;
  pose.orientation.z = transformTf.transform.rotation.z;
  pose.orientation.w = transformTf.transform.rotation.w;

  Eigen::Affine3d transform;
  tf2::fromMsg(pose, transform);

  // trasnform the pointcloud
  pcl::transformPointCloud(*pointCloud, *pointCloudTransformed, transform.cast<float>());
  // the pointcloud is now in the odom frame, frame of the map
  pointCloudTransformed->header.frame_id = targetFrame;

  const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
  // ROS_INFO_THROTTLE(5, "Transforming the Pointcloud took %f s.", duration.toSec());
  return true;
}

bool Mapper::transformPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud,
                                  const std::string& targetFrame) {
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudTransformed(new pcl::PointCloud<pcl::PointXYZRGB>);
  bool res = transformPointCloud(pointCloud, pointCloudTransformed, targetFrame);
  if (res)
    pointCloud->swap(*pointCloudTransformed);
  return res;
  
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

void Mapper::filterGlobalMap(){
  const ros::WallTime methodStartTime(ros::WallTime::now());
  // TO DO: FLOOD FILL
  // but somehow also smarter? find a convex polygon arouund? the space?

  // THIRD: one final pass through the map, 
  // the goal of this pass is to filter out the elevation and to assign sd_nbhd
  
  // originally filtered elevation is identical to elevation - because we need to conserve the rest of the map that's not being filtered
  (*global_map_)["filtered_elevation"] = (*global_map_)["elevation"];
  for (grid_map::GridMapIterator it(*global_map_); !it.isPastEnd(); ++it) {
    const grid_map::Index index(*it);
    grid_map::Position pos;
    global_map_->getPosition(index, pos);
    grid_map::Index nb_index;


    //we're only interested in points around the new measurements
    // TO DO: FLOOD FILL changes this if statement
    if ( global_map_->isValid(*it, "new_z_occupancy") ){
      float num = 0;
      float denom = 0;
      float sum = 0;
      float sum_sq =0;
      int n = 0;
      for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++){
          // grid_map::Index nb_index;
          if ( global_map_->getIndex(pos + grid_map::Position(i*mParams_.global_map_resolution, j*mParams_.global_map_resolution), nb_index) ){
          //check that we aren't crossing the border (TO DO: does this work??)
          // if (index(0)+i >= 0 && index(0)+i < global_map_->getSize()(0)-1 && index(1)+j >= 0 && index(1)+j < global_map_->getSize()(1)-1){
            // grid_map::Index nb_index( index(0)+i, index(1)+j );
            //check that elevation isn't a nan
            if (!std::isnan(global_map_->at("elevation",nb_index))){
              // updates for sd
              sum += global_map_->at("elevation",nb_index);
              sum_sq += std::pow(global_map_->at("elevation",nb_index), 2);
              n+=1;
              if ( abs(global_map_->at("elevation",nb_index) - global_map_->at("elevation",index) ) < fParams_.neighb_delta_height ) {
                // updates for temproal filtering
                //neighbors weigh more, and the point itself weighs even more
                int w = 1;
                if (i == 0)
                  w += 1;
                if (j == 0)
                  w += 1;
                // the weighted sum
                num += w * global_map_->at("delta_t",nb_index) * global_map_->at("elevation",nb_index);
                denom += w * global_map_->at("delta_t",nb_index);
              }
            }
          }
        }
      }
      global_map_->at("sd_nbhd", index) = std::sqrt(sum_sq/n - std::pow(sum/n,2));
      global_map_->at("filtered_elevation", index) = num / denom;
    }
  }
  //swap elevation with filtered elevation
  (*global_map_)["elevation"] = (*global_map_)["filtered_elevation"];
  const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
  // ROS_INFO_THROTTLE(5,  "Global map has been filtered in %f s.", duration.toSec());
}

void Mapper::filterLocalMap(){
  // const ros::WallTime filterStart(ros::WallTime::now());
  // cv::Mat elevation_cv(cv::Size(map_->getSize().x(), map_->getSize().y()), CV_32FC1);

  // grid_map::GridMapCvConverter::toImage<float, 1>(*map_, "elevation", CV_32FC1, elevation_cv);

  grid_map::Matrix elevation = map_->get("elevation");
  cv::Mat elevation_cv(elevation.rows(), elevation.cols(), CV_32FC1);
  cv::eigen2cv(elevation, elevation_cv);

  cv::Mat res(cv::Size(map_->getSize().x(), map_->getSize().y()), CV_32FC1);

  // grid_map::GridMapCvConverter::toImage<float, 1>(*map_, "elevation", CV_32F, -2, 2, elevation_cv);

  //check if any element is a nan

  cv::Mat mask = cv::Mat(elevation_cv != elevation_cv);
  if ( cv::sum( mask )[0] > 0 ){
    ROS_ERROR("elevation cv has nans!");
  }
  
  if (m_.localFilterType == 1){
    int erosion_size = m_.localFilterParam; //1 or 2;
    static cv::Mat erosion_element = cv::getStructuringElement(
        cv::MORPH_ELLIPSE, cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ), 
        cv::Point( erosion_size, erosion_size ) );
    int dilation_size = m_.localFilterParam; //1 or 2;
    static cv::Mat dilation_element = cv::getStructuringElement(
        cv::MORPH_ELLIPSE, cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ), 
        cv::Point( dilation_size, dilation_size ) );

    cv::erode( elevation_cv, elevation_cv, erosion_element );

    cv::dilate( elevation_cv, elevation_cv, dilation_element );
    grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(elevation_cv, "filtered_elevation", *map_);
  }
  else if (m_.localFilterType == 2){
    // std::cout << "elevation_cv type\t" << elevation_cv.type() << "\t" <<  type2str(elevation_cv.type()) << "\n\n";
    int d = m_.localFilterParam;
    cv::bilateralFilter(elevation_cv, res, d, 10, 10);
    grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(res, "filtered_elevation", *map_);
  }
  // grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(res, "filtered_elevation", *map_);

  // const ros::WallDuration duration = ros::WallTime::now() - filterStart;
  // ROS_INFO("Filtering Local took %f s", duration.toSec());
}

std::string Mapper::type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

void Mapper::traversabilityOverLocalMap(){
  const int cvType = CV_32F;

  // get the elevation map and copy it into a cv type
  grid_map::Matrix elevation = map_->get("filtered_elevation");
  cv::Mat elevation_cv(elevation.rows(), elevation.cols(), cvType);
  cv::eigen2cv(elevation, elevation_cv);

  // calculate the gradients
  cv::Mat grad_x, grad_y;
  cv::Sobel(elevation_cv, grad_x, cvType, 1,0,3,1,0, cv::BORDER_DEFAULT);
  cv::Sobel(elevation_cv, grad_y, cvType, 0,1,3,1,0, cv::BORDER_DEFAULT);
  cv::Mat abs_grad_x = abs(grad_x);
  cv::Mat abs_grad_y = abs(grad_y);

  // take the max of the gradient
  cv::Mat grad_max = max(abs_grad_x, abs_grad_y);

  cv::Mat no_step_mat;
  cv::threshold(grad_max, no_step_mat, 0.1, 1, 0);
  // copy back into grid_map data structure
  grid_map::GridMapCvConverter::addLayerFromImage<float, 1>(no_step_mat, "traversability", *map_);

  // copy back into grid_map data structure
  // for (int r = 0; r < elevation.rows(); ++r) {
  //   for (int c = 0; c < elevation.cols(); ++c) {
  //     grid_map::Index idx(r, c);
  //     map_->at("traversability", idx) = no_step_mat.at<float>(r,c);
  //   }
  // }
}

// ----------------------------------------------------------------------------

void Mapper::filterPointCloud(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud, int mapType) {
  // const ros::WallTime methodStartTime(ros::WallTime::now());

  pcl::PointCloud<pcl::PointXYZRGB> tempPointCloud;


  // Remove nan points.
  std::vector<int> indices;
  if (!pointCloud->is_dense) {
    pcl::removeNaNFromPointCloud(*pointCloud, tempPointCloud, indices);
    tempPointCloud.is_dense = true;
    pointCloud->swap(tempPointCloud);
  }

  if (pcParams_.appplyBoxFilter){
    pcl::CropBox<pcl::PointXYZRGB> boxFilter;
    boxFilter.setMin(Eigen::Vector4f(pcParams_.box_minX, pcParams_.box_minY, pcParams_.box_minZ, 1.0));
    boxFilter.setMax(Eigen::Vector4f(pcParams_.box_maxX, pcParams_.box_maxY, pcParams_.box_maxZ, 1.0));
    boxFilter.setInputCloud(pointCloud);
    boxFilter.filter(tempPointCloud);
    pointCloud->swap(tempPointCloud);
  }

  // if (pcParams_.applyBilateralFilter == 1){
  //   pcl::FastBilateralFilter<pcl::PointXYZRGB> bilateralFilter;
  //   bilateralFilter.setInputCloud(pointCloud); 
  //   bilateralFilter.setSigmaR(pcParams_.bilateralHalfSize);
  //   bilateralFilter.setSigmaS(pcParams_.bilateralStdDev);
  //   // bilateralFilter.setStdDev(pcParams_.bilateralStdDev);
  //   bilateralFilter.filter( tempPointCloud ); 
  //   pointCloud->swap(tempPointCloud);
  // }

  // Reduce points using VoxelGrid filter.
  if (pcParams_.applyVoxelGridFilter) {
    pcl::VoxelGrid<pcl::PointXYZRGB> voxelGridFilter;
    voxelGridFilter.setInputCloud(pointCloud);
    if (mapType == 1)
      voxelGridFilter.setLeafSize(mParams_.local_map_resolution, mParams_.local_map_resolution, mParams_.local_map_resolution);
    else if (mapType == 2)
      voxelGridFilter.setLeafSize(mParams_.global_map_resolution, mParams_.global_map_resolution, mParams_.global_map_resolution);
      // voxelGridFilter.setLeafSize(pcParams_.voxelGridFilterSize, pcParams_.voxelGridFilterSize, pcParams_.voxelGridFilterSize);
    voxelGridFilter.filter(tempPointCloud);
    pointCloud->swap(tempPointCloud);
  }

  // if (pcParams_.applyBilateralFilter == 2){
  //   pcl::FastBilateralFilter<pcl::PointXYZRGB> bilateralFilter;
  //   bilateralFilter.setInputCloud(pointCloud); 
  //   bilateralFilter.setSigmaR(pcParams_.bilateralHalfSize);
  //   bilateralFilter.setSigmaS(pcParams_.bilateralStdDev);
  //   // bilateralFilter.setStdDev(pcParams_.bilateralStdDev);
  //   bilateralFilter.filter( tempPointCloud ); 
  //   pointCloud->swap(tempPointCloud);
  // }
  
  // this is unofortunately too computationally expensive, also probably not as important
  // Remove outliers using Outlier removal filter
  // if (pcParams_.applyOutlierRemovalFilter) {
  //   pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sorFilter;
  //   sorFilter.setInputCloud(pointCloud);
  //   sorFilter.setMeanK(pcParams_.outlierMeanK);
  //   sorFilter.setStddevMulThresh(pcParams_.outlierStddevMulThresh);
  //   sorFilter.filter(tempPointCloud);
  //   pointCloud->swap(tempPointCloud);
  // }

  // const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
  // ROS_INFO_THROTTLE(5,  "Filtering the point cloud took %f s.", duration.toSec());
}

// ----------------------------------------------------------------------------

void Mapper::insertPointCloudLocal(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud, grid_map::GridMap * map, std::string mapName){
  const ros::WallTime methodStartTime(ros::WallTime::now());


  if ( m_.local_mapping_type == 0 ){
    auto& elevationLayer = (*map)["elevation"];
    auto& delta_tLayer = (*map)["delta_t"];

    // do cleaning after the last time
    map->clear("new_z"); 
    // map->clear("new_z_occupancy"); 
  

    // --------------------------------------
    // FIRST: do a pass through the pointcloud, get the maximums, put them into new z.
    int step = 1;
    if ( m_.dumbDownsample ){
      step = int( pointCloud->size()/m_.downNumPoints ); 
    }

    for (unsigned int i = 0; i < pointCloud->size(); i += step) {
      auto& point = pointCloud->points[i];
      if (point.z  >  m_.max_height)
        continue;// Skip this point if it's too high up

      grid_map::Index index;
      grid_map::Position position(point.x, point.y);
      if (!map->getIndex(position, index))
        continue;  // Skip this point if it does not lie within the elevation map.
    
      // check if the cell is initialized yet
      if ( !map->isValid(index, "new_z") ){
        map->at("new_z", index ) = point.z;
      }
      else{
        // TO DO: should I be capping the height at some maximum threshold value?
        if (point.z > map->at("new_z", index))
          map->at("new_z", index) = point.z;
      }
    }

    // --------------------------------------
    // SECOND: do a pass through the map, but only through the points with new measurements.
    // the goal of this pass is to update measurements temporally
    // filtering will be done in a later pass
    for (grid_map::GridMapIterator it(*map); !it.isPastEnd(); ++it) {
      // const grid_map::Index index(*it);

      // decrement delta_t from the entire space
      if ( ! map->isValid( *it, "delta_t") )
        map->at("delta_t", *it) = 0.0;

      if ( map->at("delta_t", *it) > m_.min_t )
        map->at("delta_t", *it) = map->at("delta_t", *it) - m_.t_dec;

      //we're only interested in points for which new measurements were acquired
      if ( map->isValid(*it, "new_z") ){
        // --------------------------------------
        {
          // check if that's the first time we observe elevation at this point
          if ( map->isValid(*it, "elevation") ){ 
            // delta_t = std::max(delta_t, m_.min_t); // make sure that delta_t is no smaller than minimum
            map->at("elevation", *it) = map->at("elevation", *it) * map->at("delta_t", *it) / (map->at("delta_t", *it) + m_.t_inc) + (map->at("new_z", *it) - mParams_.elevationOffset ) * m_.t_inc / (map->at("delta_t", *it) + m_.t_inc);
            map->at("delta_t", *it) = std::min(map->at("delta_t", *it) + m_.t_inc, m_.max_t); // increase temporal certainty
          }
          else{
            // first time elevation measurement
            map->at("elevation", *it) = map->at("new_z", *it) - mParams_.elevationOffset;
            map->at("delta_t", *it) = m_.t_inc;
          }
        }

      }
    }

    const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
    // ROS_INFO_THROTTLE(5,  "%s map has been updated with a new point cloud in %f s.", mapName.c_str(), duration.toSec());
  }
  else if (m_.local_mapping_type == 1){
    // do cleaning after the last time
    map->clear("new_z"); 
    // map->clear("new_z_occupancy"); 
  

    // --------------------------------------
    // FIRST: do a pass through the pointcloud, get the maximums, put them into new z.
    int step = 1;
    if ( m_.dumbDownsample ){
      step = int( pointCloud->size()/m_.downNumPoints ); 
    }
    
    for (unsigned int i = 0; i < pointCloud->size(); i += step) {
      auto& point = pointCloud->points[i];
      if (point.z  >  m_.max_height)
        continue;// Skip this point if it's too high up

      grid_map::Index index;
      grid_map::Position position(point.x, point.y);
      if (!map->getIndex(position, index))
        continue;  // Skip this point if it does not lie within the elevation map.
    
      // check if the cell is initialized yet
      if ( !map->isValid(index, "new_z") ){
        map->at("new_z", index ) = point.z- mParams_.elevationOffset;
      }
      else{
        // TO DO: should I be capping the height at some maximum threshold value?
        if (point.z- mParams_.elevationOffset > map->at("new_z", index))
          map->at("new_z", index) = point.z- mParams_.elevationOffset;
      }
    }

    // --------------------------------------
    // SECOND: do a pass through the map, but only through the points with new measurements.
    // the goal of this pass is to update measurements temporally
    // filtering will be done in a later pass
    for (grid_map::GridMapIterator it(*map); !it.isPastEnd(); ++it) {
      //we're only interested in points for which new measurements were acquired
      if ( map->isValid(*it, "new_z") ){
        if ( fabs(map->at("elevation", *it) - map->at("new_z", *it)) < m_.local_filter_thresh ){ // first time point inited
          map->at("elevation", *it) = map->at("elevation", *it) * m_.filter_use_old + map->at("new_z", *it) * (1 - m_.filter_use_old);
        }
        else{
          map->at("elevation", *it) = map->at("new_z", *it);
        }
      }
    }

    const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;


  }
}

// ----------------------------------------------------------------------------

void Mapper::insertPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud, grid_map::GridMap * map, std::string mapName){
  const ros::WallTime methodStartTime(ros::WallTime::now());

  auto& elevationLayer = (*map)["elevation"];
  auto& delta_tLayer = (*map)["delta_t"];

  

  auto& new_zLayer = (*map)["new_z"];
  auto& new_z_occupancyLayer = (*map)["new_z_occupancy"];    

  // --------------------------------------
  // FIRST: do a pass through the pointcloud, get the maximums, put them into new z.
  for (unsigned int i = 0; i < pointCloud->size(); ++i) {
    auto& point = pointCloud->points[i];
    if (point.z  >  m_.max_height)
      continue;// Skip this point if it's too high up

    grid_map::Index index;
    grid_map::Position position(point.x, point.y);
    if (!map->getIndex(position, index))
      continue;  // Skip this point if it does not lie within the elevation map.

    auto& new_z = new_zLayer(index(0), index(1));
    auto& new_z_occupancy = new_z_occupancyLayer(index(0), index(1));
  
    // check if the cell is initialized yet
    if ( !map->isValid(index, "new_z") ){
      // no - save newly received there
      new_z = point.z - mParams_.elevationOffset;
      // fill out occupancy in the neighbourhood
      // would have done this with an iterator but iterators loop
      //TO DO: fix me
      grid_map::Index nb_index;
      for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++){
          if ( map->getIndex(position + grid_map::Position(i*mParams_.global_map_resolution, j*mParams_.global_map_resolution), nb_index) ){
            map->at("new_z_occupancy", nb_index ) = 1.0;
          }
        }
      }

    }
    else{
      // TO DO: should I be capping the height at some maximum threshold value?
      if (point.z- mParams_.elevationOffset > new_z)
        new_z = point.z - mParams_.elevationOffset;
    }
  }

  // --------------------------------------
  // SECOND: do a pass through the map, but only through the points with new measurements.
  // the goal of this pass is to update measurements temporally
  // filtering will be done in a later pass
  for (grid_map::GridMapIterator it(*map); !it.isPastEnd(); ++it) {
    const grid_map::Index index(*it);
    // decrement delta_t from the entire space
    if ( ! map->isValid( *it, "delta_t") )
      map->at("delta_t", *it) = 0.0;

    if ( map->at("delta_t", *it) > m_.min_t )
      map->at("delta_t", *it) = map->at("delta_t", *it) - m_.t_dec;

    //we're only interested in points for which new measurements were acquired
    if ( map->isValid(index, "new_z") ){
      // --------------------------------------
      // temporal update
      auto& new_z = new_zLayer(index(0), index(1));
      auto& delta_t = delta_tLayer(index(0), index(1));
      auto& elevation = elevationLayer(index(0), index(1));
      {
        // check if that's the first time we observe elevation at this point
        if ( map->isValid(index, "elevation") ){ 
          // delta_t = std::max(delta_t, m_.min_t); // make sure that delta_t is no smaller than minimum
          elevation = elevation * delta_t / (delta_t + m_.t_inc) + new_z * m_.t_inc / (delta_t + m_.t_inc);
          delta_t = std::min(delta_t + m_.t_inc, m_.max_t); // increase temporal certainty
        }
        else{
          // first time elevation measurement
          elevation = new_z;
          delta_t = m_.t_inc;
        }
      }

    }
  }

  const ros::WallDuration duration = ros::WallTime::now() - methodStartTime;
  // ROS_INFO_THROTTLE(5,  "%s map has been updated with a new point cloud in %f s.", mapName.c_str(), duration.toSec());
}

// ----------------------------------------------------------------------------

void Mapper::clearTheMap(){
  if (mParams_.init_local_map){
    map_->clearAll();
    map_->resetTimestamp();
  }  
  if (mParams_.init_global_map){
    global_map_->clearAll();
    global_map_->resetTimestamp();
  }
}

// ----------------------------------------------------------------------------

void Mapper::zeroTheMap(){
  if (mParams_.init_local_map){
    for (grid_map::GridMapIterator it(*map_); !it.isPastEnd(); ++it) {
      map_->at("elevation", *it) = 0.0;
    }
  }
  if (mParams_.init_global_map){
    for (grid_map::GridMapIterator it(*global_map_); !it.isPastEnd(); ++it) {
      global_map_->at("elevation", *it) = 0.0;
    }
  }
}

// ----------------------------------------------------------------------------

void Mapper::initParams()
{

  // fetch pointcloud parameters
  {
    nhp_.param<int>("num_pc_sources", pcParams_.num_pc_sources, 0.0);

    for(int i = 1; i <= pcParams_.num_pc_sources; i++){
      std::string frame, topic;
      // first: acquire all point cloud frames
      nhp_.param<std::string>("pc" + std::to_string(i) + "_frame", frame, "57"); 
      pcParams_.pc_frames.push_back(frame);

      // then - acuiqre all point cluod topics
      nhp_.param<std::string>("pc" + std::to_string(i) + "_topic", topic, "57"); 
      pcParams_.pc_topics[frame] = topic;

      //finally, set up pointcloud gotPc arrays
      // pcParams_.ph1_gotPc[frame] = false;
      // pcParams_.ph2_gotPc[frame] = false;
    }

    // nhp_.param<int>("local_pc_insert_from", pcParams_.local_pc_insert_from, 1);


    nhp_.param<float>("min_variance", pcParams_.min_variance, 57.0);

    nhp_.param<bool>("pc/voxel_grid_filter", pcParams_.voxel_grid_filter, true);
    nhp_.param<bool>("pc/applyVoxelGridFilter", pcParams_.applyVoxelGridFilter, false);
    nhp_.param<float>("pc/voxelGridFilterSize", pcParams_.voxelGridFilterSize, 57.0);

    nhp_.param<bool>("pc/applyOutlierRemovalFilter", pcParams_.applyOutlierRemovalFilter, true);
    nhp_.param<int>("pc/outlierMeanK", pcParams_.outlierMeanK, 50);
    nhp_.param<float>("pc/outlierStddevMulThresh", pcParams_.outlierStddevMulThresh, 1.0);

    nhp_.param<float>("pc/box/minX", pcParams_.box_minX, 0.0);
    nhp_.param<float>("pc/box/minY", pcParams_.box_minY, 0.0);
    nhp_.param<float>("pc/box/minZ", pcParams_.box_minZ, 0.0);

    nhp_.param<float>("pc/box/maxX", pcParams_.box_maxX, 0.0);
    nhp_.param<float>("pc/box/maxY", pcParams_.box_maxY, 0.0);
    nhp_.param<float>("pc/box/maxZ", pcParams_.box_maxZ, 0.0);
    nhp_.param<bool>("pc/box/appplyBoxFilter", pcParams_.appplyBoxFilter, false);
    nhp_.param<float>("pc/maxTimeDeltaBtwnPC", pcParams_.maxTimeDeltaBtwnPC, 0.0);

    nhp_.param<float>("pc/bilateralHalfSize", pcParams_.bilateralHalfSize, 0.0);
    nhp_.param<float>("pc/bilateralStdDev", pcParams_.bilateralStdDev, 0.0);
    nhp_.param<int>("pc/applyBilateralFilter", pcParams_.applyBilateralFilter, 0);

    nhp_.param<int>("pc/collectBeforeClustering", pcParams_.collectBeforeClustering, 0);
    nhp_.param<bool>("verbose/pc", pcParams_.verbosePC, false);


  }


  // fetch map parameters
  {
    nhp_.param<bool>("init_global_map", mParams_.init_global_map, false);
    nhp_.param<bool>("init_local_map", mParams_.init_local_map, false);
    nhp_.param<bool>("init_world_map", mParams_.init_world_map, false);

    nhp_.param<std::string>("map_frame", mParams_.map_frame, "57");
    nhp_.param<std::string>("tracking_frame", mParams_.tracking_frame, "57");

    nhp_.param<float>("grid_map/local_map_y", mParams_.local_map_y, 57.0);
    nhp_.param<float>("grid_map/local_map_x", mParams_.local_map_x, 57.0);
    nhp_.param<float>("grid_map/local_resolution", mParams_.local_map_resolution, 57.0);

    nhp_.param<float>("grid_map/global_map_y", mParams_.global_map_y, 57.0);
    nhp_.param<float>("grid_map/global_map_x", mParams_.global_map_x, 57.0);
    nhp_.param<float>("grid_map/global_resolution", mParams_.global_map_resolution, 57.0);

    nhp_.param<float>("grid_map/world_map_y", mParams_.world_map_y, 57.0);
    nhp_.param<float>("grid_map/world_map_x", mParams_.world_map_x, 57.0);
    nhp_.param<float>("grid_map/world_resolution", mParams_.world_map_resolution, 57.0);

    nhp_.param<float>("grid_map/publish_period", mParams_.map_publish_period, 1.0);
    

    nhp_.param<bool>("grid_map/spit_map", mParams_.spit_map, false);
    nhp_.param<std::string>("grid_map/spit_path", mParams_.spit_path, "~/Desktop/");

    nhp_.param<bool>("underneath/atStart", mParams_.uAtStart, true);
    nhp_.param<bool>("underneath/atFail", mParams_.uAtFail, true);
    nhp_.param<float>("underneath/height", mParams_.uHeight, 1.0);
    nhp_.param<float>("underneath/sideDist", mParams_.uSideDist, 1.0);
    nhp_.param<float>("underneath/frontDist", mParams_.uFrontDist, 1.0);
    nhp_.param<float>("underneath/backDist", mParams_.uBackDist, 1.0);

    nhp_.param<float>("elevationOffset", mParams_.elevationOffset, 1.0);



  }

  // fetch filtering parameters
  {
    nhp_.param<float>("filtering/height_threshold", fParams_.height_threshold, 57.0);
    nhp_.param<float>("filtering/neighb_delta_height", fParams_.neighb_delta_height, 57.0);
    nhp_.param<float>("filtering/local_neighb_delta_height", fParams_.local_neighb_delta_height, 57.0);
  }

  // mapping parameters (insertion of values into the cells)
  {
    nhp_.param<int>("mapping/N", m_.N, 30);

    nhp_.param<float>("mapping/min_t", m_.min_t, 1);
    nhp_.param<float>("mapping/max_t", m_.max_t, 20);
    nhp_.param<float>("mapping/t_inc", m_.t_inc, 3);
    nhp_.param<float>("mapping/t_dec", m_.t_dec, 1);
    nhp_.param<std::string>("mapping/dec_type", m_.dec_type, "subtract");

    nhp_.param<float>("mapping/start_alt_var_thresh", m_.start_alt_var_thresh, 0.2);
    m_.start_alt_var_thresh = m_.start_alt_var_thresh * m_.start_alt_var_thresh; // make it from SD to variance
    nhp_.param<float>("mapping/switch_to_alt_var_thresh", m_.switch_to_alt_var_thresh, 0.1);
    m_.switch_to_alt_var_thresh = m_.switch_to_alt_var_thresh * m_.switch_to_alt_var_thresh; // make it from SD to variance
    nhp_.param<float>("mapping/min_var", m_.min_var, 0.0001);

    nhp_.param<int>("mapping/nbhd", m_.nbhd, 1);
    nhp_.param<int>("pc/timestamp_delta", m_.timestamp_delta, 50000000);

    nhp_.param<float>("mapping/cluster_sd_max", m_.cluster_sd_max, 0.05);
    nhp_.param<float>("mapping/cluster_height_delta_max", m_.cluster_height_delta_max, 0.1);
    nhp_.param<int>("mapping/minimum_cluster_size", m_.minimum_cluster_size, 50);
    nhp_.param<int>("mapping/minimum_cluster_neighbors", m_.minimum_cluster_neighbors, 9);

    nhp_.param<float>("mapping/max_height", m_.max_height, 2.0);

    nhp_.param<int>("mapping/local_mapping_type", m_.local_mapping_type, 0);
    nhp_.param<float>("mapping/local_filter_thresh", m_.local_filter_thresh, 0);
    nhp_.param<float>("mapping/filter_use_old", m_.filter_use_old, 0);
    nhp_.param<float>("mapping/send_local_size", m_.send_local_size, 0);

    nhp_.param<bool>("mapping/dumbDownsample", m_.dumbDownsample, false);
    nhp_.param<int>("mapping/downNumPoints", m_.downNumPoints, 5000);

    nhp_.param<bool>("mapping/filterLocalPC", m_.filterLocalPC, false);

    nhp_.param<int>("mapping/localFilterType", m_.localFilterType, 1);
    nhp_.param<int>("mapping/localFilterParam", m_.localFilterParam, 1);

    

    





  }

}

}