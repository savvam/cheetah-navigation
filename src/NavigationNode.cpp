/**
 * @file NavigationNode.cpp
 * @brief Entry point for nnovigation ROS node
 * @author Savva Morozov <savva@mit.edu>
 * @date 10 Nov 2020
 */

#include <ros/ros.h>
#include "cheetah_navigation/Navigation.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "cheetah");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");

  cheetah::Navigation node(nhtopics, nhparams);

  // setup multiple callback threads, spin
  ros::AsyncSpinner spinner(nhparams.param("num_callback_threads", 2));  // Use n threads
  spinner.start();
  ros::waitForShutdown();
  return 0;
}