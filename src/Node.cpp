/**
 * @file Node.cpp
 * @brief RRT Node implementation
 * @author Savva Morozov <savva@mit.edu>
 * @date 20 Jan 2021
 */


#include "cheetah_navigation/Node.h"

namespace cheetah {

  // static variables must be defined, not just declared
  grid_map::GridMap * Node::map_ = nullptr;
  Planner_Params * Node::p_ = nullptr;
 
Node::Node() {
  // TO DO: uhm i'm gonna be running this line a hundred times does this act global
  std::srand((unsigned)std::time(0)); 
}

void Node::copyPosFrom( Node * other ){
  pos_ = other->pos_;
  index_ = other->index_;
}

void Node::makeAJump( Node * grampa, Node * dad, TrajectorySegment * segment){
  // assumption: grampa already initialized
  dad->setPos(segment->startPos_);
  dad->setFather(grampa);
  dad->pathTypeToFather_ = 'w';

  setPos(segment->finishPos_);
  setFather(dad);
  pathTypeToFather_ = 'j';
}

void Node::makeAWalk( Node * dad){
  // assumption: dad already initialized
  setFather(dad);
  pathTypeToFather_ = 'w';
}

bool Node::checkWalkabilityFrom(Node * other){
  // return checkWalkabilityFrom(other, false);

  if (! (other)) 
    return false;
   
  return checkWalkabilityBtwn(other->pos_, pos_);
}

bool Node::checkWalkabilityFrom(Node * other, bool nanHandling){
  if (! (other)) 
    return false;

  // checking walkability from other->pos_ to pos_
  return checkWalkabilityBtwn(other->pos_, pos_, nanHandling);
}

bool Node::checkWalkabilityBtwn(grid_map::Position pos1, grid_map::Position pos2){
  // backward compatibility aka i don't wanna rewrite code
  // return checkWalkabilityBtwn(pos1, pos2, false);

  grid_map::Position v_parl = (pos2 - pos1) /  (pos2-pos1).norm() ;
  grid_map::Position v_perp = Geometry::rotate(v_parl, M_PI/2);

  grid_map::Index start; map_->getIndex( pos1, start );
  grid_map::Index finish; map_->getIndex( pos2, finish );


  if ( belongToSameCluster(start, finish) && belongsToACluster(start) ){
    // all the point should belong to this cluster
    int cluster = map_->at("cluster_tag", start);

    if ( fabs(cluster) < 0.01 ){
      ROS_ERROR("walk check, start cluster is 0 but i'm doing the check");
      return false;
    }

    // simple traverability chcek - over 3 lines
    // TO DO: need to figure this out, what's the best way to handle front / back of the robot
    if (p_->simplifiedTraversabilityCheck){
      pos1 -= (p_->walk_front_delta) * v_parl;
      pos2 += (p_->walk_front_delta) * v_parl;
      // pos1 -= (p_->walk_front_delta + p_->robot_length/2 ) * v_parl;
      // pos2 += (p_->walk_front_delta + p_->robot_length/2) * v_parl;
      if ( !checkLineBelongsToClusterPosition(pos1 , pos2, cluster ) )
        return false;
      if ( !checkLineBelongsToClusterPosition(pos1 + p_->robot_width/2 * v_perp , pos2 + p_->robot_width/2 * v_perp, cluster) )
        return false;
      if ( !checkLineBelongsToClusterPosition(pos1 - p_->robot_width/2 * v_perp , pos2 - p_->robot_width/2 * v_perp, cluster) )
        return false;
      return true;
    }

    else{ // complete traverabilty check over entire space of the robot
      // get the polygon for checking
      grid_map::Polygon traversing;
      traversing.setFrameId(map_->getFrameId());
      grid_map::Position temp = pos2;
      // going clockwise
      temp += p_->walk_front_delta * v_parl - p_->robot_width/2 * v_perp; traversing.addVertex(temp);
      temp += -(distanceBtwn(pos1, pos2) + 2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);
      temp += p_->robot_width * v_perp; traversing.addVertex(temp);
      temp += (distanceBtwn(pos1, pos2)+2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);

      //let's go checking; if any point is not in the cluster - quit
      for (grid_map::PolygonIterator iterator(*map_, traversing); !iterator.isPastEnd(); ++iterator)
        if (! belongsToCluster(*iterator, cluster) )
          return false;
      // everything checked, we are victorious
      return true;
    }
  }

  // return false by default
  return false;
}

bool Node::checkWalkabilityBtwn(grid_map::Position pos1, grid_map::Position pos2, bool nanHandling){
  // ASSUMPTION:
  //    -   nanHandling applies to the first node, not the second; i.e. pos2 must be non-nan, pos 1 can be nan
  //        if nanHandling true, there is a clear sense of directionality

  // grid_map::Position v_parl = (pos1 - pos2) /  (pos1-pos2).norm() ;
  
  grid_map::Position v_parl = (pos2 - pos1) /  (pos2-pos1).norm() ;
  // if (p_->rotAngle < 0)
  //   v_parl = (pos1 - pos2) /  (pos1-pos2).norm();
  grid_map::Position v_perp = Geometry::rotate(v_parl, M_PI/2);

  grid_map::Index start; map_->getIndex( pos1, start );
  grid_map::Index finish; map_->getIndex( pos2, finish );

  // part of clusters
  if ( nanHandling ){
    // finish must be a part of a cluster though
    if ( belongsToACluster(finish) ){
      int cluster = map_->at("cluster_tag", finish);
      if (p_->simplifiedTraversabilityCheck){
        // pos1 -= p_->walk_front_delta * v_parl;
        pos2 += p_->walk_front_delta * v_parl;
        if ( !checkLineBelongsToClusterPosition(pos1 , pos2, cluster, nanHandling) )
          return false;
        if ( !checkLineBelongsToClusterPosition(pos1 + p_->robot_width/2 * v_perp , pos2 + p_->robot_width/2 * v_perp, cluster, nanHandling) )
          return false;
        if ( !checkLineBelongsToClusterPosition(pos1 - p_->robot_width/2 * v_perp , pos2 - p_->robot_width/2 * v_perp, cluster, nanHandling) )
          return false;
      }
      else{ // complete traverabilty check over entire space of the robot
        // get the polygon for checking
        grid_map::Polygon traversing;
        traversing.setFrameId(map_->getFrameId());
        grid_map::Position temp = pos2;
        temp += p_->walk_front_delta * v_parl - p_->robot_width/2 * v_perp; traversing.addVertex(temp);
        temp += -(distanceBtwn(pos1, pos2) + 2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);
        temp += p_->robot_width * v_perp; traversing.addVertex(temp);
        temp += (distanceBtwn(pos1, pos2)+2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);

        //let's go checking; if any point has elevation and is not in the cluster - quit
        for (grid_map::PolygonIterator iterator(*map_, traversing); !iterator.isPastEnd(); ++iterator)
          if ( map_->isValid(*iterator, "elevation") && !belongsToCluster(*iterator, cluster) )
            return false;
      }
    }
    else{
      return false;
    }
    return true;
  }

  if ( belongToSameCluster(start, finish) && belongsToACluster(start) ){
    // all the point should belong to this cluster
    int cluster = map_->at("cluster_tag", start);

    // simple traverability chcek - over 3 lines
    if (p_->simplifiedTraversabilityCheck){
      pos1 -= p_->walk_front_delta * v_parl;
      pos2 += p_->walk_front_delta * v_parl;
      if ( !checkLineBelongsToClusterPosition(pos1 , pos2, cluster ) )
        return false;
      if ( !checkLineBelongsToClusterPosition(pos1 + p_->robot_width/2 * v_perp , pos2 + p_->robot_width/2 * v_perp, cluster) )
        return false;
      if ( !checkLineBelongsToClusterPosition(pos1 - p_->robot_width/2 * v_perp , pos2 - p_->robot_width/2 * v_perp, cluster) )
        return false;
      return true;
    }
    else{ // complete traverabilty check over entire space of the robot
      // get the polygon for checking
      grid_map::Polygon traversing;
      traversing.setFrameId(map_->getFrameId());
      grid_map::Position temp = pos2;
      temp += p_->walk_front_delta * v_parl - p_->robot_width/2 * v_perp; traversing.addVertex(temp);
      temp += -(distanceBtwn(pos1, pos2) + 2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);
      temp += p_->robot_width * v_perp; traversing.addVertex(temp);
      temp += (distanceBtwn(pos1, pos2)+2*p_->walk_front_delta) * v_parl; traversing.addVertex(temp);

      //let's go checking; if any point is not in the cluster - quit
      for (grid_map::PolygonIterator iterator(*map_, traversing); !iterator.isPastEnd(); ++iterator)
        if (! belongsToCluster(*iterator, cluster) )
          return false;
    }
  }
  else{
    return false;
  }

  return false;
}

bool Node::checkTraversabilityOverLine(grid_map::Position pos1, grid_map::Position pos2){
  return checkTraversabilityOverLine(pos1, pos2, false);
  // grid_map::Index start; grid_map::Index finish;
  // // check that points are even inside the map
  // if (! map_->getIndex( pos1, start ) || ! map_->getIndex( pos2, finish ))
  //   return false;

  // if ( belongToSameCluster(start, finish) && belongsToACluster(start) ){
  //   return checkLineBelongsToClusterIndex( start, finish, map_->at("cluster_tag", start));
  // }
  // else{
  //   return false;
  // }
}

bool Node::checkTraversabilityOverLine(grid_map::Position pos1, grid_map::Position pos2, bool nanHandling){
  grid_map::Index start; grid_map::Index finish;
  // check that points are even inside the map
  if (! map_->getIndex( pos1, start ) || ! map_->getIndex( pos2, finish ))
    return false;

  return checkLineBelongsToClusterIndex( start, finish, map_->at("cluster_tag", finish), nanHandling);
}

bool Node::checkLineBelongsToClusterIndex(grid_map::Index start, grid_map::Index finish, int cluster){
  return checkLineBelongsToClusterIndex(start, finish, cluster, false);
  // grid_map::LineIterator iterator(*map_, start, finish);
  // while ( !iterator.isPastEnd() ){
  //   // check that each point belong to the same cluster as start / finish
  //   if (! belongsToCluster(*iterator, cluster) )
  //     return false;
  //   ++iterator;
  // }
  // return true;
}

bool Node::checkLineBelongsToClusterIndex(grid_map::Index start, grid_map::Index finish, int cluster, bool nanHandling){
  grid_map::LineIterator iterator(*map_, start, finish);
  while ( !iterator.isPastEnd() ){
    // check that each point belongs to the same cluster as the provided cluster
    // not that if nanHandling is on, skip the points that have invalid elevation
    // if ( ! belongsToCluster(*iterator, cluster) && !( nanHandling && !map_->isValid(*iterator, "elevation") )  )

    //if map is undefined at that point or doesn't belong to a cluster
    if ( !map_->isValid(*iterator, "elevation") || !belongsToCluster(*iterator, cluster) )
      return false;
    ++iterator;
  }
  return true;
}

bool Node::checkLineBelongsToClusterPosition(grid_map::Position pos1, grid_map::Position pos2, int cluster){
  return checkLineBelongsToClusterPosition(pos1, pos2, cluster, false);
  
  // grid_map::Index start, finish;
  // if (! map_->getIndex( pos1, start ) || ! map_->getIndex( pos2, finish ))
  //   return false;
  // return checkLineBelongsToClusterIndex(start, finish, cluster);
  // grid_map::LineIterator iterator(*map_, start, finish);
  // while ( !iterator.isPastEnd() ){
  //   // check that each point belong to the same cluster as start / finish
  //   if (! belongsToCluster(*iterator, cluster) )
  //     return false;
  //   ++iterator;
  // }
  // return true;
}

bool Node::checkLineBelongsToClusterPosition(grid_map::Position pos1, grid_map::Position pos2, int cluster, bool nanHandling){
  grid_map::Index start, finish;
  if (! map_->getIndex( pos1, start ) || ! map_->getIndex( pos2, finish ))
    return false;
  return checkLineBelongsToClusterIndex(start, finish, cluster, nanHandling);
  // grid_map::LineIterator iterator(*map_, start, finish);
  // while ( !iterator.isPastEnd() ){
  //   // check that each point belong to the same cluster as start / finish
  //   if (! belongsToCluster(*iterator, cluster) )
  //     return false;
  //   ++iterator;
  // }
  // return true;
}


bool Node::checkValidityTo(Node * gramps){
  if (pathTypeToFather_ = 'w'){
    if ( checkWalkabilityFrom( gramps ) ){
      return true;
    }
  }
  else if ( pathTypeToFather_ = 'j' ){
    // grid_map::Index start; map_->getIndex( gramps->pos_, start );
    // grid_map::Index finish; map_->getIndex( pos_, finish );
    // float takeoffCluster = map_->at("cluster_tag", start);
    // float landingCluster = map_->at("cluster_tag", finish);
    float takeoffCluster = map_->atPosition("cluster_tag", gramps->pos_);
    float landingCluster = map_->atPosition("cluster_tag", pos_);

    // float jumpAngle = std::atan( (finish(1) - start(1)) / (finish(0) - start(0))  );
    float jumpAngle = std::atan( (pos_(1) - gramps->pos_(1)) / (pos_(0) - gramps->pos_(0))  );

    // if (finish(0) < start(0) )
    if (pos_(0) < gramps->pos_(0) )
      jumpAngle = M_PI - jumpAngle;

    if (checkThatJumpWorks( (pos_ + gramps->pos_)/2 , jumpAngle, takeoffCluster, landingCluster)){
      return true;
    }
  }
  return false;
}


bool Node::checkJumpabilityBtwn( grid_map::Position startPos, grid_map::Position finishPos ){
  float takeoffCluster = map_->atPosition("cluster_tag", startPos);
  float landingCluster = map_->atPosition("cluster_tag", finishPos);

  // grid_map::Index start; map_->getIndex( startPos, start );
  // grid_map::Index finish; map_->getIndex( finishPos, finish );
  // float takeoffCluster = map_->at("cluster_tag", start);
  // float landingCluster = map_->at("cluster_tag", finish);

  // float jumpAngle = std::atan( (finish(1) - start(1)) / (finish(0) - start(0))  );
  float jumpAngle = std::atan( (finishPos(1) - startPos(1)) / (finishPos(0) - startPos(0))  );
  // if (finish(0) < start(0) )
  if (finishPos(0) < startPos(0) )
    jumpAngle = M_PI - jumpAngle;

  if (checkThatJumpWorks( (startPos + finishPos)/2 , jumpAngle, takeoffCluster, landingCluster))
    return true;
  return false;
}


bool Node::checkTraversabilityFrom(Node * other, TrajectorySegment * segment){
  // return checkTraversabilityFrom(other, segment, false);
  segment->reset();
  // other shouldn't be a nullptr
  if (! (other)) 
    return false;

  // TO DO: reindexing shouldn't be the issue, consider removing

  // other node has valid elevation
  if ( other->reindex() && map_->isValid(other->index_, "elevation") ) {

    //both nodes belong to some clusters - we can work with this
    if ( reindex() && belongsToACluster() && other->belongsToACluster() ){
      // nodes belong to the same cluster - check for walking
      if ( belongsToSameClusterAs(other) ){
        // check traversability by walk
        if ( checkWalkabilityFrom(other) ){
          segment->set(other->pos_, pos_, 'w');
          if (!!p_->quiet && !p_->quiet_rrt)
            std::cout << "\twalkability checked out\n";
          return true;
        }
        else{
          // try jumping
          if (p_->allowJumpOnSameSurface){
            grid_map::Index new_start; map_->getIndex( other->pos_, new_start );
            grid_map::Index new_end; map_->getIndex( pos_, new_end );
            if ( checkJumpability(new_start, new_end, segment) )
              return true;
          }
        }
      }
      else{
        // ROS_WARN("TRYING OUT JUMPS");
        // check traversability by jump
        // grid_map::Index new_start; map_->getIndex( other->pos_, new_start );
        // grid_map::Index new_end; map_->getIndex( pos_, new_end );
        if ( checkJumpability(other->index_, index_, segment) ){
          if (!!p_->quiet && !p_->quiet_rrt)
            std::cout << "\tjumpability checked out\n";
          return true;
        }
      }
    }
  }
  
  // by default return false
  return false;
}

bool Node::checkTraversabilityFrom(Node * other, TrajectorySegment * segment, bool nanHandling){
  segment->reset();
  // other shouldn't be a nullptr
  if (! (other)) 
    return false;

  if (nanHandling){
    if ( belongsToACluster() ){
      // i belong to a cluster, so we're good

      // check walkability with nan handling allowed
      if ( checkWalkabilityFrom(other, nanHandling) ){
        segment->set(other->pos_, pos_, 'w');
        return true;
      }
    }
  }
  else{
    if (belongsToACluster() && other->belongsToACluster()){
      if ( belongsToSameClusterAs(other) ){
        // check traversability by walk
        if ( checkWalkabilityFrom(other) ){
          segment->set(other->pos_, pos_, 'w');
          return true;
        }
      }
      else{
        // check traversability by jump
        grid_map::Index new_start; map_->getIndex( other->pos_, new_start );
        grid_map::Index new_end; map_->getIndex( pos_, new_end );
        if ( checkJumpability(new_start, new_end, segment) ){
          return true;
        }
      }
    }
  }
  
  // nope :(
  return false;
}

bool Node::belongToSameCluster( grid_map::Index start, grid_map::Index finish ){
  return ( belongsToACluster(start) &&  std::fabs( map_->at("cluster_tag", start) - map_->at("cluster_tag", finish) ) < 0.01 );
}

bool Node::belongsToSameClusterAs(Node * other){
  return belongToSameCluster(index_, other->index_);
}

bool Node::belongsToACluster(){
  if (reindex())
    return belongsToACluster(index_);
  return false;
}

bool Node::belongsToACluster( grid_map::Index point){
  return ( map_->at("cluster_tag", point) > 0.1 ); // doesn't belong to a 0 cluster aka noise
}

bool Node::belongsToCluster( grid_map::Index point, int cluster){
  return ( std::fabs( map_->at("cluster_tag", point) - cluster ) < 0.01 ); // doesn't belong to a 0 cluster aka noise
}

bool Node::checkThatJumpWorks(grid_map::Position jumpCenter, float jumpAngle, float takeoffCluster, float landingCluster){

  // there are a total of 4 checks here:
  // first: check that every point in the landing position belongs to the landingCluster
  // second: check that every point in the takeoff position belongs to the takeoffCluster
  // third: check that the difference between landing and takeoff is relatively small
  // fourth: check that cells in flight are not too high 

  grid_map::Position v_parl(1,0); v_parl = Geometry::rotate(v_parl, jumpAngle);
  grid_map::Position v_perp(0,1); v_perp = Geometry::rotate(v_perp, jumpAngle);
  
  // CHECK ONE
  // get the polygon for the landing location

  float landingMean = 0; int landingN = 0;
  grid_map::Polygon landingLoc;
  landingLoc.setFrameId(map_->getFrameId());

  // going clockwise
  grid_map::Position temp = jumpCenter;
  temp += p_->jump_distance/2 * v_parl + p_->robot_width/2 * v_perp; landingLoc.addVertex(temp);
  temp += p_->robot_width * v_parl; landingLoc.addVertex(temp);
  temp += -p_->robot_width * v_perp; landingLoc.addVertex(temp);
  temp += -p_->robot_width * v_parl; landingLoc.addVertex(temp);
  // check that each grid cell inside the landing location polygon belongs to the cluster
  for (grid_map::PolygonIterator iterator(*map_, landingLoc); !iterator.isPastEnd(); ++iterator){
    if (! belongsToCluster(*iterator, landingCluster) ){
      if(!!p_->quiet && !p_->quiet_rrt)
        ROS_WARN("BAD LANDING");
      return false;
    }
    landingMean += map_->at("elevation", *iterator); 
    landingN += 1;
  }
  landingMean /= landingN;
  

  // CHECK TWO
  // get the polygon for the takeoff location
  // takeoff mean
  float takeoffMean = 0; int takeoffN = 0;
  grid_map::Polygon takeoffLoc;
  takeoffLoc.setFrameId(map_->getFrameId());
  temp += -p_->jump_distance * v_parl; takeoffLoc.addVertex(temp);
  temp += -p_->robot_width * v_parl; takeoffLoc.addVertex(temp);
  temp += p_->robot_width * v_perp; takeoffLoc.addVertex(temp);
  temp += p_->robot_width * v_parl; takeoffLoc.addVertex(temp);
  // check that each grid cell inside the takoff location polygon belongs to the cluster
  for (grid_map::PolygonIterator iterator(*map_, takeoffLoc); !iterator.isPastEnd(); ++iterator){
    if (! belongsToCluster(*iterator, takeoffCluster) ){
      if(!!p_->quiet && !p_->quiet_rrt)
        ROS_WARN("BAD TAKEOFF");
      return false;
    }
    takeoffMean += map_->at("elevation", *iterator); 
    takeoffN += 1;
  }
  takeoffMean /= takeoffN;

  // CHECK THREE
  // check that difference between takeoffMean and landingMean isn't too high
  if ( std::fabs(landingMean - takeoffMean) > p_->jump_height_delta ){
    if(!!p_->quiet && !p_->quiet_rrt)
      ROS_WARN("BAD HEIGHT DELTA");
    return false;
  }

  // CHECK FOUR
  // check all point in between: check that they aren't higher than something
  // get the polygon for the in between location
  grid_map::Polygon inFlight;
  inFlight.setFrameId(map_->getFrameId());
  inFlight.addVertex(temp);
  temp += p_->jump_distance * v_parl; inFlight.addVertex(temp);
  temp += -p_->robot_width * v_perp; inFlight.addVertex(temp);
  temp += -p_->jump_distance * v_parl; inFlight.addVertex(temp);
  for (grid_map::PolygonIterator iterator(*map_, inFlight); !iterator.isPastEnd(); ++iterator){
    if ( map_->isValid(*iterator, "elevation") ){
      // if elevation - takeoff elevation more than our jump capabilities - return false
      if ( fabs(map_->at("elevation", *iterator) - std::min(takeoffMean,landingMean) ) > p_->jump_height_delta){
        if(!!p_->quiet && !p_->quiet_rrt)
          ROS_WARN("ELEVATION TOO HIGH IN DELTA");
        return false;
      }
    }
  }
  return true;
}

void function_cx_1_func(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr){
  // func = c[0] / ( 1 + exp( - 65 * (x[0] - c[1] - c[2]*x[1] )   ) ) + c[3]*x[0];
  func = c[0] / ( 1 + exp( - 65 * (x[0] - c[1] - c[2]*x[1] )   ) ) + c[3]*x[0] + c[4]*x[1];
}

void function_cx_1_grad(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void *ptr) {
    double f_exp = exp( - 65 * (x[0] - c[1] - c[2]*x[1] )   );
    // func = c[0] / ( 1 + f_exp ) + c[3]*x[0] ;
    func = c[0] / ( 1 + f_exp ) + c[3]*x[0] + c[4]*x[1];
    grad[0] = 1 / ( 1 + f_exp );
    grad[1] = - 65 * c[0] * f_exp  / pow(f_exp + 1, 2);
    grad[2] = x[1] * grad[1];
    grad[3] = x[0];
    grad[4] = x[1];
}

bool Node::robotIsFreeOfObstaclesAt( grid_map::Position position, float orientation ){
  grid_map::Position v_parl(1,0); v_parl = Geometry::rotate(v_parl, orientation);
  grid_map::Position v_perp(0,1); v_perp = Geometry::rotate(v_perp, orientation);

  grid_map::Index ind; 
  if ( !map_->getIndex(position, ind) )
    return false;
  float cluster = map_->at("cluster_tag", ind);

  if (p_->simplifiedTraversabilityCheck){
    grid_map::Position pos1 = position - p_->robot_length * v_parl/2;
    grid_map::Position pos2 = position + p_->robot_length * v_parl/2;
    // if ( !checkLineBelongsToClusterPosition(pos1 , pos2,  cluster) ){
    //   return false;
    // }
    if ( !checkLineBelongsToClusterPosition(pos1 + p_->robot_width/2 * v_perp , pos2 + p_->robot_width/2 * v_perp, cluster ) ){
      return false;
    }
    if ( !checkLineBelongsToClusterPosition(pos1 - p_->robot_width/2 * v_perp , pos2 - p_->robot_width/2 * v_perp, cluster) ){
      return false;
    }
    return true;
  }
  
  grid_map::Polygon robotLoc;
  robotLoc.setFrameId(map_->getFrameId());
  grid_map::Position temp = position;
  // going clockwise
  temp += p_->robot_width/2 * v_perp - p_->robot_width/2 * v_parl; robotLoc.addVertex(temp); 
  temp += p_->robot_length * v_parl; robotLoc.addVertex(temp);
  temp += -p_->robot_width * v_perp; robotLoc.addVertex(temp);
  temp += -p_->robot_length * v_parl; robotLoc.addVertex(temp);
  // check that each grid cell inside the landing location polygon belongs to some cluster; 
  // assumption is that they can't belong to different clustrs without some error points them
  for (grid_map::PolygonIterator iterator(*map_, robotLoc); !iterator.isPastEnd(); ++iterator)
    if (! belongsToCluster(*iterator, cluster) )
      return false;
  return true;
}

bool Node::squareIsFreeOfObstaclesAt( grid_map::Position position, float orientation ){
  grid_map::Position v_parl(1,0); v_parl = Geometry::rotate(v_parl, orientation);
  grid_map::Position v_perp(0,1); v_perp = Geometry::rotate(v_perp, orientation);

  grid_map::Index ind; 
  if ( !map_->getIndex(position, ind) )
    return false;
  float cluster = map_->at("cluster_tag", ind);
  
  grid_map::Polygon robotLoc;
  robotLoc.setFrameId(map_->getFrameId());
  grid_map::Position temp = position;
  // going clockwise
  temp += p_->robot_width/2 * v_perp - p_->robot_width/2 * v_parl; robotLoc.addVertex(temp); 
  temp += p_->robot_width * v_parl; robotLoc.addVertex(temp);
  temp += -p_->robot_width * v_perp; robotLoc.addVertex(temp);
  temp += -p_->robot_width * v_parl; robotLoc.addVertex(temp);
  // check that each grid cell inside the landing location polygon belongs to some cluster; 
  // assumption is that they can't belong to different clustrs without some error points them
  for (grid_map::PolygonIterator iterator(*map_, robotLoc); !iterator.isPastEnd(); ++iterator)
    if (! belongsToCluster(*iterator, cluster) )
      return false;
  return true;
}



bool Node::checkJumpability(grid_map::Index start, grid_map::Index finish, TrajectorySegment * segment){

  if ( map_->at( "elevation", finish ) - map_->at( "elevation", start ) < 0 ){
    bool res = checkJumpability(finish, start, segment);
    segment->swap();
    return res; 
  }

  
  // both point belong to a cluster, we can work with that
  if ( belongsToACluster(start) && belongsToACluster(finish) ){
    
    if ( belongToSameCluster(start, finish) ){
      ROS_ERROR( "CECKING JUMPABILITY FOR SAME CLUSTERS WTF" );
      return false;
    }

    // check if the distance is too high between the two
    if ( fabs( map_->at("elevation", finish) - map_->at("elevation", start) ) > p_->jump_height_delta ){
      if (!!p_->quiet && !p_->quiet_rrt)
        ROS_ERROR("FAILED in checkJumpability, too much diff");
      return false;
    }

    float takeoffCluster = map_->at("cluster_tag", start);
    float landingCluster = map_->at("cluster_tag", finish);

    float takeoffElevation = map_->at("elevation", start );
    float landingElevation = map_->at("elevation", finish );

    grid_map::Position startPos, finishPos;
    map_->getPosition(start, startPos);
    map_->getPosition(finish, finishPos);

    if ( !checkIfAcceptable(startPos) || !checkIfAcceptable(finishPos) )
      return false;

    // float jumpAngle = atan2( (finishPos(1) - startPos(1)), (finishPos(0) - startPos(0)) );
    float jumpAngle = Geometry::angleToVector( finishPos-startPos );

    Eigen::Vector2d parl(1,0); parl = Geometry::rotate(parl, jumpAngle);
    Eigen::Vector2d perp(0,1); perp = Geometry::rotate(perp, jumpAngle);

    // initial guess for some of the params
    float distToObst = 0.6*(finishPos-startPos).norm(); // 0.55;
    float heightDiff = map_->at( "elevation", finish ) - map_->at( "elevation", start );

    // use alglib to solve for the jump
    alglib::real_1d_array c; c.setlength(5);
    c(0) = heightDiff; c(1) = distToObst; c(2) = 0.0; c(3) = 0.0; c(4) = 0.0;

    // first time
    // SWITCH VERBOSE TO FALSE
    if ( ! evaluateTerrainAt(startPos, jumpAngle, c, &c, false, true) )
      return false;
    // check that the height difference isn't too high
    if (c[0] > p_->jump_height_delta || c[0] < 0 || c[1] > 1.2 || c[1] < 0.08 ) 
      return false;

    // get the best jump direction
    float bestJumpAngle = jumpAngle - atan(c[2]);

    if (!!p_->quiet && !p_->quiet_rrt){
      ROS_INFO("Finished solving FIRST");
      std::cout << "JUMPANGLE\t" << 180/M_PI*jumpAngle << "\tBEST\t" << 180/M_PI*bestJumpAngle << "\t-atan\t" << -atan(c[2])*180/M_PI << "\n";
      std::cout << "heightDiff\t" << c(0) << "\tdistToObst\t" << c(1) << "\tbestJumpAngle\t" << 180/M_PI*bestJumpAngle << "\n";
    }
    // if (p_->doTwoJumpsChecks){
    // second time
    // SWITCH VERBOSE TO FALSE


    // if ( ! evaluateTerrainAt(startPos, jumpAngle, c, &c, false, true) )
    //   return false;
    // bestJumpAngle = bestJumpAngle - atan(c[2]);

    // Geometry::makeAngleProper(bestJumpAngle);

    if (!!p_->quiet && !p_->quiet_rrt){
      ROS_INFO("Finished solving SECOND");
      std::cout << "\tBEST\t" << 180/M_PI*bestJumpAngle << "\t-atan\t" << -atan(c[2])*180/M_PI << "\n";
      std::cout << "heightDiff\t" << c(0) << "\tdistToObst\t" << c(1) << "\tbestJumpAngle\t" << 180/M_PI*bestJumpAngle << "\n";
    }

    Eigen::Vector2d borderPosition = startPos + parl * c[1];
    

    // TO DO: this doesn't sample jumps by distance
    Eigen::Vector2d bestParl(1,0); bestParl = Geometry::rotate(bestParl, bestJumpAngle); // parallel to the jump
    Eigen::Vector2d bestPerp(0,1); bestPerp = Geometry::rotate(bestPerp, bestJumpAngle); // parallel to the step
    float jRes = p_->jump_num_samples_resolution;
    int pts = int(p_->jump_sampling_dist_perp / jRes);
    Eigen::Vector2d testPosition;
    std::vector<int> acceptedJumps; acceptedJumps.reserve(7);
    for (int i = -pts; i<=pts; i++){
      testPosition = borderPosition + bestPerp * i * jRes;
      // TO DO: checkThatJumpWorks should be the one checking multiple distances (?)
      // TO DO: test this first: does it work without me sampling extra distances?
      if (checkThatJumpWorks( testPosition, bestJumpAngle, takeoffCluster, landingCluster ) ){
        acceptedJumps.push_back(i);
      }
    }
    if (!!p_->quiet && !p_->quiet_rrt)
      ROS_INFO("finished looking for jumps; we got %i acceptable jumps", int(acceptedJumps.size()) );
    // at this stage, we are interested in collision avoidance - hence best jump choice is the middle of all jump choices
    // in theory this could produce an error jump; highly unlikely
    if (acceptedJumps.size() > 0){
      float finalJumpChoice = 0;
      for(int i = 0;i<acceptedJumps.size();i++)
        finalJumpChoice += acceptedJumps[i];
      finalJumpChoice /= acceptedJumps.size();
      if (!!p_->quiet && !p_->quiet_rrt)
        ROS_INFO("sending a jump, jumpchoice %f", finalJumpChoice);

      grid_map::Position jumpStart, jumpFinish;  
      // CLUTTERED ENVIRONMENT CHANGES
      jumpStart = borderPosition + bestPerp * finalJumpChoice * jRes - bestParl * (p_->jump_distance + p_->robot_width) / 2;
      jumpFinish = borderPosition + bestPerp * finalJumpChoice * jRes + bestParl * (p_->jump_distance + p_->robot_width) / 2;
    

      grid_map::Index valCheck;
      if ( !map_->getIndex(jumpStart, valCheck) || !map_->getIndex(jumpFinish, valCheck) ){
        ROS_ERROR("ONE OF THE JUMP VALUES ARE OUTSIDE THE MAP WTF WASN'T SUPPOSED TO HAPPEN");
        return false;
      }

      segment->set(jumpStart, jumpFinish, 'j');
      return true;
    }
    else{
      return false;
    }
  }
  return false;
}

bool Node::evaluateTerrainAt(Eigen::Vector2d pos, float angle, alglib::real_1d_array c0, alglib::real_1d_array * cresult, bool removeFloorPastLine, bool verbose){
  // TO DO: SHOULD this return false???
  // removeFloorPastLine - boolean

  Eigen::Vector2d parl(1,0); parl = Geometry::rotate(parl, angle);
  Eigen::Vector2d perp(0,1); perp = Geometry::rotate(perp, angle);

  float totalXDist = c0[1] + p_->terrain_param_xFrontExtra;
  float sideYDist = p_->terrain_param_sideYDist;
  float resDist = p_->terrain_param_resolution;
  float maxDiff = p_->terrain_param_maxDiff;

  int numX = int(totalXDist/resDist);
  int numY = int(2*sideYDist/resDist);
  float z0 = map_->atPosition( "elevation", pos);

  Eigen::Vector2d borderPos1 = pos + parl * c0[1];
  Eigen::Vector2d borderPos2 = borderPos1 + perp;
  bool properSide = Geometry::isLeft(borderPos1, borderPos2, pos);


  std::vector<Eigen::Vector3d>pointsToAdd; pointsToAdd.reserve((numX+1)*(numY+1));
  // collect the points
  
  grid_map::Index ind;
  float pointZ;
  grid_map::Position pointPos;
  for ( int i = 0; i <= numX; i++ ){
    for ( int j = 0; j <= numY; j++ ){
      pointPos = pos + parl * (i*resDist) + perp * (j*resDist-sideYDist);
      // point is in the map
      if ( map_->getIndex(pointPos, ind ) ){
        // point has elevation
        if ( map_->isValid( ind, "elevation") && belongsToACluster(ind) ){  
        // if ( map_->isValid( ind, "elevation") && belongsToACluster(ind) ){  
          pointZ = map_->at("elevation", ind)-z0;
          // filtering:
          // point is within a certain radius of 0 or previously attained step height
          // point is either closer to previously attained height or closer to 0, but then it must be on the proper side
          if ( ( ( fabs(pointZ) <= maxDiff ) ||  (fabs(pointZ - c0[0]) <= maxDiff) ) ){
            if (removeFloorPastLine){
              if (fabs(pointZ) > fabs(pointZ-c0[0]) ) 
                pointsToAdd.push_back( Eigen::Vector3d(i*resDist,j*resDist-sideYDist,pointZ) );
              if ( (fabs(pointZ) < fabs(pointZ-c0[0])) && properSide == Geometry::isLeft(borderPos1, borderPos2, pointPos ) )
                pointsToAdd.push_back( Eigen::Vector3d(i*resDist,j*resDist-sideYDist,pointZ) );
            }
            else{
              pointsToAdd.push_back( Eigen::Vector3d(i*resDist,j*resDist-sideYDist,pointZ) );
            }
          }
        }
      }
    }
  }

  alglib::real_2d_array xy;
  alglib::real_1d_array z;
  alglib::real_1d_array w;
  xy.setlength(pointsToAdd.size(), 2);
  z.setlength(pointsToAdd.size());
  w.setlength(pointsToAdd.size());

  for (int i=0;i<pointsToAdd.size();i++){
    xy(i,0) = pointsToAdd[i](0);
    xy(i,1) = pointsToAdd[i](1);
    z(i) = pointsToAdd[i](2);
    w(i) = 1;
    if ( fabs(z(i)) > fabs(z(i)-c0[0]) )
      w(i) = p_->terrain_param_landingWeight;
  }

  alglib::real_1d_array c; c.setlength(5);

  c(0) = c0(0); c(1) = c0(1); c(2) = c0(2); c(3) = c0(3); c(4) = c0(4);
  

  // hight, distance to obstacle, tan of angle, x and y corrections
  alglib::real_1d_array s = "[1.0e-1, 5.0e-1, 1.0, 0.1, 0.1]";
  //upper and lower bounds on the variables
  alglib::real_1d_array lb = "[ -0.5, 0.15, -INF, -0.1, -0.1 ]";
  alglib::real_1d_array ub = "[ 0.5, 1.0, +INF, 0.1, 0.1 ]";

  double epsx = 0.001;
  alglib::ae_int_t maxits = 50;
  alglib::ae_int_t info;
  alglib::lsfitstate state;
  alglib::lsfitreport rep;
  double diffstep = 1.0e-5;

  alglib::lsfitcreatewfg(xy, z, w, c, true, state);
  alglib::lsfitsetcond(state, epsx, maxits);
  alglib::lsfitsetscale(state, s);
  alglib::lsfitsetbc(state, lb, ub);
  alglib::lsfitfit(state, function_cx_1_func, function_cx_1_grad);
  alglib::lsfitresults(state, info, c, rep);

  if (verbose && !!p_->quiet && !p_->quiet_rrt){
    std::cout << "\n\nxy = " << xy.tostring(3).c_str() << "\n";
    std::cout << "z = " << z.tostring(3).c_str() << "\n";
    std::cout << "res = " << c.tostring(3).c_str() << "\n\n\n";
  }

  if ( c.length() != 5 ){
    return false;
  }
  *cresult = c;
  return true;
}

void Node::steer( Node * other, float dist){
  //unclear if i should check if steering is necessary here or in the planner
  pos_ = other->pos_ + (pos_ - other->pos_) / distanceTo(other) * dist;
  // pos_ = other->pos_ + (pos_ - other->pos_) / distanceTo(other) * p_->steerDistance;
  map_->getIndex(pos_, index_);
}

void Node::sampleRandomPoint(){
  reset();
  float x = ((float) std::rand() / (RAND_MAX)) * map_->getLength().x() - map_->getLength().x()/2;
  float y = ((float) std::rand() / (RAND_MAX)) * map_->getLength().y() - map_->getLength().y()/2;
  pos_ = grid_map::Position(x,y) + map_->getPosition();
  map_->getIndex(pos_, index_);
  // int x = (int) (((float) std::rand() / (RAND_MAX)) * map_->getSize()(0));
  // int y = (int) (((float) std::rand() / (RAND_MAX)) * map_->getSize()(1));
  // index_ = grid_map::Index(x, y);
  // map_->getPosition(index_, pos_);
}

bool Node::checkIfAcceptable(){
  return checkIfAcceptable( pos_ );
  // if ( reindex() && map_->isValid(index_, "elevation") ){
  //   float cluster = map_->at("cluster_tag", index_);
  //   for ( grid_map::CircleIterator iterator(*map_, pos_, p_->acceptableRad); !iterator.isPastEnd(); ++iterator ){
  //     if (!map_->isValid(*iterator, "elevation"))
  //       return false;
  //     if (!belongsToCluster(*iterator, cluster))
  //       return false;
  //   }
  //   return true;
  // }
  // return false;
}

bool Node::checkIfAcceptable( grid_map::Position pos ){
  grid_map::Index ind;
  if ( map_->getIndex(pos, ind) && map_->isValid(ind, "elevation") ){
    float cluster = map_->at("cluster_tag", ind);
    for ( grid_map::CircleIterator iterator(*map_, pos, p_->acceptableRad); !iterator.isPastEnd(); ++iterator ){
      if (!map_->isValid(*iterator, "elevation"))
        return false;
      if (!belongsToCluster(*iterator, cluster))
        return false;
    }
    return true;
  }
  return false;
}

bool Node::checkIfElevationIsNan( grid_map::Position pos ){
  grid_map::Index ind;
  if ( !map_->getIndex(pos, ind) )
    return false;
  return !map_->isValid(ind, "elevation");

}

float Node::distanceTo(Node * other){
  // should be depricated?
  return distanceBtwn(pos_, other->pos_);
}

float Node::distanceBtwn(grid_map::Position pos1, grid_map::Position pos2){
  return (pos2 - pos1).norm();
}

float Node::distanceTo(grid_map::Position other_pos){
  // should be deprecated?
  return distanceBtwn(pos_, other_pos);
}

void Node::setFather(Node * DarthVader){
  father_ = DarthVader;
}

void Node::setPos(grid_map::Position pos){
  pos_ = pos;
  reindex();
}

void Node::setFatherWithShortCutting(Node * DarthVader){
  // assumption: already traversable to DarthVader
  // std::unique_ptr<Node> grampa( DarthVader );
  Node * grampa = DarthVader;
  // grampa has a father and traversable to grampa's father 

  // basic idea: we don't rewire if this requires a jump (cause that requries inserting an extra node), 
  // or if resultant edge is too long, or if non traversable (duh)
  // we do rewire if it's just walking
  // TrajectorySegment segment = TrajectorySegment();
  while ( grampa->father_ && distanceTo(grampa->father_) < p_->maxEdgeLength && checkWalkabilityFrom(grampa->father_) ){
    // grampa is now grampa's father
    grampa = grampa->father_;
  }
  setFather(grampa);
}

void Node::reset(){
  pathTypeToFather_ = 'x';
  pos_ = map_->getPosition();
  reindex();
}

void Node::setAsOrigin(){
  setAsOrigin( map_->getPosition() );
}

void Node::setAsOrigin(grid_map::Position origin){
  father_ = nullptr;

  pos_ = origin;
  map_->getIndex(pos_, index_);
}

bool Node::reindex(){
  if( !map_->getIndex(pos_, index_) ){
    ROS_ERROR("The value of the node is beyound the map");
    return false;
  }
  return true;
}

}