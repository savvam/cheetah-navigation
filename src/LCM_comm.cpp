/**
 * @file LCM_comm.cpp
 * @brief ROS wrapper for LCM Communication
 * @author Savva Morozov <savva@mit.edu>
 * @date 12 Feb 2021
 */



#include "cheetah_navigation/LCM_comm.h"


namespace cheetah {

  foo::lcm::LCM * LCM_comm::lcm_ = new foo::lcm::LCM("udpm://239.255.76.67:7667?ttl=255");
  bool LCM_comm::armed_ = false;
  float LCM_comm::maxSpeed_ = 0.8;
  bool LCM_comm::verbose_ = false;
  LCM_handler LCM_comm::handler_ = LCM_handler();
  bool LCM_comm::recIMU_ = true;
  bool LCM_comm::recRF_ = false;


LCM_handler::LCM_handler(){
  pub_jump_callbacks_ = ros::Publisher();
}

void LCM_handler::jumpFinishedCallback(const foo::lcm::ReceiveBuffer* rbuf,
                               const std::string& chan, 
                               const jump_callback_lcmt* msg){
  // oi mate
  ROS_WARN("LCM received a jump finished callback");
  std_msgs::Empty msg2;
  pub_jump_callbacks_.publish(msg2);
  std::cout << "lcm published a jump finished callback\n";
}


void LCM_handler::jumpComputedCallback(const foo::lcm::ReceiveBuffer* rbuf,
                               const std::string& chan, 
                               const qkdif_outputs_lcmt* msg){
  // oi mate
  ROS_WARN("LCM received a jump computed callback");
  std_msgs::Empty msg2;
  pub_jump_computed_callbacks_.publish(msg2);
  std::cout << "lcm published a jump computed callback\n";
}

void LCM_handler::imuCallback(const foo::lcm::ReceiveBuffer* rbuf,
                               const std::string& chan, 
                               const microstrain_lcmt* msg){
  // oi mate
  if (LCM_comm::recIMU_){
    ROS_WARN_THROTTLE(2.0, "LCM received IMU DATA.");
    
    cheetah_msgs::imu ros_msg;
    ros_msg.header.stamp = ros::Time::now();
    ros_msg.header.frame_id = "imu";

    std_msgs::Float64MultiArray array; array.data.clear();
    array.data.push_back(msg->quat[0]); array.data.push_back(msg->quat[1]); array.data.push_back(msg->quat[2]); array.data.push_back(msg->quat[3]);
    ros_msg.imu_quat = array;

    array.data.clear();
    array.data.push_back(msg->rpy[0]); array.data.push_back(msg->rpy[1]); array.data.push_back(msg->rpy[2]); 
    ros_msg.imu_rpy = array;

    array.data.clear();
    array.data.push_back(msg->omega[0]); array.data.push_back(msg->omega[1]); array.data.push_back(msg->omega[2]); 
    ros_msg.imu_omega = array;

    array.data.clear();
    array.data.push_back(msg->acc[0]); array.data.push_back(msg->acc[1]); array.data.push_back(msg->acc[2]); 
    ros_msg.imu_acc = array;

    ros_msg.lcm_published_timestamp = msg->lcm_published_timestamp;
    pub_imu_.publish(ros_msg);
  }
}

void LCM_handler::rfCallback(const foo::lcm::ReceiveBuffer* rbuf,
                               const std::string& chan, 
                               const state_estimator_lcmt* msg){
  // oi mate
  if (LCM_comm::recRF_){
    ROS_WARN_THROTTLE(2.0, "LCM received R and FORCE DATA:");

    cheetah_msgs::state_estimator ros_msg;
    ros_msg.header.stamp = ros::Time::now();
    ros_msg.header.frame_id = "se";

    std_msgs::Float64MultiArray array; array.data.clear();
    array.data.push_back(msg->p[0]); array.data.push_back(msg->p[1]); array.data.push_back(msg->p[2]); 
    ros_msg.p = array;

    array.data.clear();
    array.data.push_back(msg->vWorld[0]); array.data.push_back(msg->vWorld[1]); array.data.push_back(msg->vWorld[2]); 
    ros_msg.vWorld = array;

    array.data.clear();
    array.data.push_back(msg->vBody[0]); array.data.push_back(msg->vBody[1]); array.data.push_back(msg->vBody[2]); 
    ros_msg.vBody = array;

    array.data.clear();
    array.data.push_back(msg->rpy[0]); array.data.push_back(msg->rpy[1]); array.data.push_back(msg->rpy[2]); 
    ros_msg.e_rpy = array;

    array.data.clear();
    array.data.push_back(msg->omegaBody[0]); array.data.push_back(msg->omegaBody[1]); array.data.push_back(msg->omegaBody[2]); 
    ros_msg.omegaBody = array;

    array.data.clear();
    array.data.push_back(msg->omegaWorld[0]); array.data.push_back(msg->omegaWorld[1]); array.data.push_back(msg->omegaWorld[2]); 
    ros_msg.omegaWorld = array;

    array.data.clear();
    array.data.push_back(msg->quat[0]); array.data.push_back(msg->quat[1]); array.data.push_back(msg->quat[2]); 
    ros_msg.e_quat = array;

    array.data.clear();
    array.data.push_back(msg->contact_estimate[0]); array.data.push_back(msg->contact_estimate[1]); array.data.push_back(msg->contact_estimate[2]); 
    ros_msg.contact_estimate = array;

    // depending on which state_estimator_lcmt is beig used, commment or uncomment these lines

    array.data.clear();
    for(int i = 0; i < 12; i++)
      array.data.push_back(msg->pfoot_rel_body[i]);
    ros_msg.pfoot_rel_body = array;

    array.data.clear();
    for(int i = 0; i < 12; i++)
      array.data.push_back(msg->vfoot[i]);
    ros_msg.vfoot = array;

    ros_msg.lcm_published_timestamp = msg->lcm_published_timestamp;

    // depending on which state_estimator_lcmt is beig used, commment or uncomment these lines

    pub_state_estimator_.publish(ros_msg);
  }
}




bool LCM_comm::reinit(std::string updm){
  lcm_ = new foo::lcm::LCM(updm);
  // pub_jump_callbacks_ = nh->advertise<std_msgs::Empty>( "lcm/jump_callback", 1, true);

  if(!lcm_->good()){
    ROS_ERROR("----------------------------------------");
    ROS_ERROR("----------------------------------------");
    ROS_ERROR("LCM NOT INITIALIZED PROPERLY SMTH IS OFF");
    ROS_ERROR("----------------------------------------");
    ROS_ERROR("----------------------------------------");
  }
  else{
    ROS_INFO("--------------");
    ROS_INFO("LCM has been restarted on %s", updm.c_str());
    ROS_INFO("--------------");
  }
}

void LCM_comm::setupPublishers( ros::NodeHandle * nh ){
  handler_.pub_jump_callbacks_ = nh->advertise<std_msgs::Empty>( "lcm/jump_callback", 1, true);
  handler_.pub_jump_computed_callbacks_ = nh->advertise<std_msgs::Empty>( "lcm/jump_computed_callback", 1, true);
  handler_.pub_state_estimator_ = nh->advertise<cheetah_msgs::state_estimator>( "lcm/state_estimator", 1, true);
  handler_.pub_imu_ = nh->advertise<cheetah_msgs::imu>( "lcm/imu", 1, true);

  lcm_->subscribe("JUMP_CALLBACK", &LCM_handler::jumpFinishedCallback, &handler_);
  lcm_->subscribe("CONTROLLER_qkdif_outputs", &LCM_handler::jumpComputedCallback, &handler_);
  lcm_->subscribe("microstrain", &LCM_handler::imuCallback, &handler_);
  lcm_->subscribe("state_estimator_data", &LCM_handler::rfCallback, &handler_);
  ROS_INFO("LCM publish / subscribe setup successful");
}

void LCM_comm::handle(){ 
  lcm_->handle();
}



bool LCM_comm::good(){
  if(!lcm_->good()){
    ROS_ERROR("----------------------------------------");
    ROS_ERROR("LCM NOT INITIALIZED PROPERLY SMTH IS OFF");
    ROS_ERROR("----------------------------------------");
  }
  return lcm_->good();
}

bool LCM_comm::sendVelCMD(float v_x, float v_y, float v_psi){

  // if (fabs(v_psi) > maxYawSpeed_){
  //   v_psi = std::copysign( maxYawSpeed, v_psi );
  // }
  if (armed_){
    // std::cout << "LCM1 sent\t" << v_x << "\t" << v_y << "\t" << v_psi << "\n";
    if ( std::isnan(v_x)  || std::isnan(v_y) || std::isnan(v_psi) ){
      ROS_ERROR("LCM ERROR: NAN COMMANDS REQUESTED");
      v_x = 0.0; v_y = 0.0; v_psi = 0.0;
    }
    if (fabs(v_psi) > 0.8){
      v_psi = std::copysign( 0.8, v_psi );
    }
    float temp = (v_x * v_x + v_y*v_y) / maxSpeed_*maxSpeed_;
    if ( temp > 1 ){
      v_x *= std::sqrt(1/temp);
      v_y *= std::sqrt(1/temp);
    }
    vel_cmd msg;
    msg.v_x = v_x;
    msg.v_y = v_y;
    msg.v_psi = -v_psi; // because wires got crossed at SKETCH SKETCH SKETCH
    lcm_->publish( "VEL_CMD", &msg );
    if (verbose_){
      std::cout << "LCM2 sent\t" << v_x << "\t" << v_y << "\t" << v_psi << "\n";
    }
    return true;
  }
  else{
    return false;
  }
}

void LCM_comm::zeroOut(){
  vel_cmd msg;
  msg.v_x = 0.0;
  msg.v_y = 0.0;
  msg.v_psi = 0.0; // because wires got crossed at SKETCH SKETCH SKETCH
  lcm_->publish( "VEL_CMD", &msg );
  if (verbose_)
    std::cout << "LCM2 sent a 0 command\n";
}


bool LCM_comm::sendT265( int t265, unsigned long lcm_published_timestamp, geometry_msgs::TransformStamped odom_to_base){
  // Eigen::Vector2d pos( odom_to_base.transform.translation.x, odom_to_base.transform.translation.y );
  tf2::Quaternion quat(
        odom_to_base.transform.rotation.x,
        odom_to_base.transform.rotation.y,
        odom_to_base.transform.rotation.z,
        odom_to_base.transform.rotation.w);
  tf2::Matrix3x3 mat(quat);
  double roll, pitch, yaw;
  mat.getRPY(roll, pitch, yaw);

  t265_lcmt t265LCM;
  t265LCM.camera = t265;
  t265LCM.lcm_published_timestamp = lcm_published_timestamp;
  t265LCM.quat[0] = odom_to_base.transform.rotation.x;
  t265LCM.quat[1] = odom_to_base.transform.rotation.y;
  t265LCM.quat[2] = odom_to_base.transform.rotation.z;
  t265LCM.quat[3] = odom_to_base.transform.rotation.w;
  t265LCM.rpy[0] = roll;
  t265LCM.rpy[1] = pitch;
  t265LCM.rpy[2] = yaw;
  t265LCM.pos[0] = odom_to_base.transform.translation.x;
  t265LCM.pos[1] = odom_to_base.transform.translation.y;
  t265LCM.pos[2] = odom_to_base.transform.translation.z;
  lcm_->publish( "T265", &t265LCM );
}

void LCM_comm::sendMap(grid_map::GridMap * map, float len){
  const grid_map::Position pos = map->getPosition();

  // copy over global loc so the sim knows where to render the map
  grid_map_lcmt gridMapLCM;
  gridMapLCM.center[0] = pos[0];
  gridMapLCM.center[1] = pos[1];

  gridMapLCM.rows = int(len/map->getResolution());
  gridMapLCM.cols = int(len/map->getResolution());
  gridMapLCM.totalCells = gridMapLCM.rows * gridMapLCM.cols;
  gridMapLCM.resolution = map->getResolution();

  // MUST resize the vector objects to properly serialize
  gridMapLCM.heightmap.resize(gridMapLCM.totalCells);
  gridMapLCM.indexmap.resize(gridMapLCM.totalCells);

  // copy in data
  // const grid_map::Matrix elevation = map->get("elevation");

  int vert = 0;
  grid_map::Position pos_temp;
  grid_map::Index ind_temp;
  for (int c=0; c<gridMapLCM.cols; c++){ //change int for other bigger type
    for (int r=0; r<gridMapLCM.rows; r++){
      pos_temp(0) = gridMapLCM.resolution * (int(gridMapLCM.rows/2)-r)+gridMapLCM.center[0]-0.001;
      pos_temp(1) = gridMapLCM.resolution * (int(gridMapLCM.cols/2)-c)+gridMapLCM.center[1]-0.001;
      if ( map->getIndex(pos_temp, ind_temp) ){
        gridMapLCM.heightmap[vert] = map->at("filtered_elevation", ind_temp);
        gridMapLCM.indexmap[vert] = map->at("traversability", ind_temp);
      }
      else{
        gridMapLCM.heightmap[vert] = 3;
        gridMapLCM.indexmap[vert] = 1;
        std::cout << c << "\t" << r << "\tpoint\t" << pos_temp.transpose() << "\tcenter\t" << gridMapLCM.center[0] << " " << gridMapLCM.center[1] << "\n";
      }
      vert++;
    }
  }

  lcm_->publish( "HEIGHTMAP", &gridMapLCM );
  if (verbose_)
    std::cout << "LCM SENT a MAP\n";
}

void LCM_comm::sendMap(grid_map::GridMap * map){
  // ROS_INFO("SENT A MAP");
  const grid_map::Position pos = map->getPosition();

  // copy over global loc so the sim knows where to render the map
  grid_map_lcmt gridMapLCM;
  gridMapLCM.center[0] = pos[0];
  gridMapLCM.center[1] = pos[1];

  // const grid_map::Size size = _submap.getSize();
  const grid_map::Size size = map->getSize();
  gridMapLCM.rows = size[0];
  gridMapLCM.cols = size[1];
  gridMapLCM.totalCells = gridMapLCM.rows * gridMapLCM.cols;
  // gridMapLCM.resolution = _submap.getResolution();
  gridMapLCM.resolution = map->getResolution();

  // MUST resize the vector objects to properly serialize
  gridMapLCM.heightmap.resize(gridMapLCM.totalCells);
  gridMapLCM.indexmap.resize(gridMapLCM.totalCells);

  // copy in data
  const grid_map::Matrix elevation = map->get("elevation");

  int vert = 0;
  grid_map::Position pos_temp;
  grid_map::Index ind_temp;
  for (int c=0;c<gridMapLCM.cols;c++){ //change int for other bigger type
    for (int r=0; r<gridMapLCM.rows; r++){
      pos_temp(0) = gridMapLCM.resolution * (int(gridMapLCM.rows/2)-r)+gridMapLCM.center[0]-0.001;
      pos_temp(1) = gridMapLCM.resolution * (int(gridMapLCM.cols/2)-c)+gridMapLCM.center[1]-0.001;
      if ( map->getIndex(pos_temp, ind_temp) ){
        gridMapLCM.heightmap[vert] = map->at("elevation", ind_temp);
      }
      else{
        gridMapLCM.heightmap[vert] = 3;
        std::cout << c << "\t" << r << "\tpoint\t" << pos_temp.transpose() << "\tcenter\t" << gridMapLCM.center[0] << " " << gridMapLCM.center[1] << "\n";
      }
      vert++;
    }
  }
  lcm_->publish( "HEIGHTMAP", &gridMapLCM );
  if (verbose_)
    std::cout << "LCM SENT a MAP\n";
}

void LCM_comm::sendBasicJumpInfo(float height_delta, float dist_to_obst){
  basic_jump_info jumpInfo;
  jumpInfo.height_delta = height_delta;
  jumpInfo.dist_to_obst = dist_to_obst;
  lcm_->publish( "BASIC_JUMP_INFO", &jumpInfo );
  if (verbose_){
    std::cout << "LCM sent jump request\t" << height_delta << "\t" << dist_to_obst << "\n";
  }
}

void LCM_comm::sendJump3DInfo( float obs_height, float dist_to_obs, float dist_past_obs_lb, float dist_past_obs_ub, float yaw_init, float yaw_term ){
  jump3D_info_lcmt jumpInfo;
  jumpInfo.obs_height = obs_height;
  jumpInfo.dist_to_obs = dist_to_obs;
  jumpInfo.dist_past_obs_lb = dist_past_obs_lb;
  jumpInfo.dist_past_obs_ub = dist_past_obs_ub;
  jumpInfo.yaw_init = yaw_init;
  jumpInfo.yaw_term = yaw_term;

  lcm_->publish( "JUMP_3D_INFO", &jumpInfo );
  if (verbose_){
    std::cout << "LCM sent jump request\t" << obs_height << "\t" << dist_to_obs << "\t" << dist_past_obs_lb << "\t" << dist_past_obs_ub << "\t" << yaw_init << "\t" << yaw_term << "\n";
  }
}

void LCM_comm::verbose(){
  verbose_ = true;
}

void LCM_comm::deverbose(){
  verbose_ = false;
}

bool LCM_comm::verboseLevel(){
  return verbose_;
}

void LCM_comm::arm(){
  armed_ = true;
}

void LCM_comm::disarm(){
  armed_ = false;
}

bool LCM_comm::isArmed(){
  return armed_;
}

}