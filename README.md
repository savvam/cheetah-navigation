### Dependencies:

- Boost
- Eigen3
- lcm, install with apt:
  - `sudo apt install liblcm-dev`
- alglib, install with apt:
  - `sudo apt-get install libalglib-dev`
- opencv
- CGAL, install with apt:
  - `sudo apt-get install libcgal-dev`
- mason, polylabel, geometry, variant:
  -  install mason:
    - cd `cheetah/navigation/directory`
    - `mkdir ./mason`
    - `curl -sSfL https://github.com/mapbox/mason/archive/v0.23.0.tar.gz | tar -z --extract --strip-components=1 --exclude="*md" --exclude="test*" --directory=./mason`
  - install polylabel, geometry, variant:
    - `./mason/mason install polylabel 1.0.3`
    - `./mason/mason install geometry 2.0.3`
    - `./mason/mason install variant 1.2.0`
  - copy the mason cmake into the project:
    - `mkdir cmake`
    - `wget -O cmake/mason.cmake https://raw.githubusercontent.com/mapbox/mason/master/mason.cmake`

- ros dependencies:
  - cheetah_msgs
  - pcl_ros
  - cv_bridge
  - grid_map