/* Produced by CVXGEN, 2021-08-17 18:37:46 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: matrix_support.c. */
/* Description: Support functions for matrix multiplication and vector filling. */
#include "solver.h"
void multbymA(double *lhs, double *rhs) {
}
void multbymAT(double *lhs, double *rhs) {
  lhs[0] = 0;
  lhs[1] = 0;
}
void multbymG(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(params.A[0])-rhs[1]*(params.A[4]);
  lhs[1] = -rhs[0]*(params.A[1])-rhs[1]*(params.A[5]);
  lhs[2] = -rhs[0]*(params.A[2])-rhs[1]*(params.A[6]);
  lhs[3] = -rhs[0]*(params.A[3])-rhs[1]*(params.A[7]);
}
void multbymGT(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(params.A[0])-rhs[1]*(params.A[1])-rhs[2]*(params.A[2])-rhs[3]*(params.A[3]);
  lhs[1] = -rhs[0]*(params.A[4])-rhs[1]*(params.A[5])-rhs[2]*(params.A[6])-rhs[3]*(params.A[7]);
}
void multbyP(double *lhs, double *rhs) {
  /* TODO use the fact that P is symmetric? */
  /* TODO check doubling / half factor etc. */
  lhs[0] = rhs[0]*(2*params.Q[0])+rhs[1]*(2*params.Q[2]);
  lhs[1] = rhs[0]*(2*params.Q[1])+rhs[1]*(2*params.Q[3]);
}
void fillq(void) {
  work.q[0] = 0;
  work.q[1] = 0;
}
void fillh(void) {
  work.h[0] = params.b[0];
  work.h[1] = params.b[1];
  work.h[2] = params.b[2];
  work.h[3] = params.b[3];
}
void fillb(void) {
}
void pre_ops(void) {
}
