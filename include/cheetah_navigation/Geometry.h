/**
 * @file Geometry.h
 * @brief Geometry Package
 * @author Savva Morozov <savva@mit.edu>
 * @date 16 Feb 2021
 */

#pragma once

// include other cool stuff
// #include "cheetah_navigation/Node.h"


// loggin maps
#include <iostream>
#include <fstream>
#include <math.h>

// include ros

// include messages
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>

#include <ros/ros.h>

// include structs and basic libs
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/TrajectorySegment.h"


// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>



namespace cheetah {

  class Geometry {
    public:

      static Eigen::Vector2d rotate(Eigen::Vector2d vector, float angle);

      static float distBtwn(Eigen::Vector2d p1, Eigen::Vector2d p2);

      static float norm(Eigen::Vector2d p);

      static Eigen::Vector2d getParl(Eigen::Vector2d p1, Eigen::Vector2d p2);

      static Eigen::Vector2d getPerp(Eigen::Vector2d p1, Eigen::Vector2d p2);

      static Eigen::Vector2d getPerpFromParl(Eigen::Vector2d parl);

      static Eigen::Vector3d lineThroughTwoPoints(Eigen::Vector2d p1, Eigen::Vector2d p2);

      static bool linesIntersect(Eigen::Vector3d line1, Eigen::Vector3d line2);

      static Eigen::Vector2d lineIntersection(Eigen::Vector3d line1, Eigen::Vector3d line2);

      static bool colinear(Eigen::Vector2d p1, Eigen::Vector2d p2, Eigen::Vector2d p3);

      static Eigen::Vector3d linePerpThroughPoint( Eigen::Vector3d line, Eigen::Vector2d p );

      static float distToLine( Eigen::Vector3d line, Eigen::Vector2d p );

      static bool isRight( Eigen::Vector2d a, Eigen::Vector2d b, Eigen::Vector2d test );

      static bool isLeft( Eigen::Vector2d a, Eigen::Vector2d b, Eigen::Vector2d test );

      static Eigen::Vector2d perpOnLine( Eigen::Vector3d line, Eigen::Vector2d p);

      static float angleBetweenTwoVectors( Eigen::Vector2d a, Eigen::Vector2d b );

      static float angleToVector( Eigen::Vector2d a );

      static float tubeSizeAt( Eigen::Vector3d trajLine, Eigen::Vector3d tubeLine, Eigen::Vector2d p );

      static float pointToTubeRatio( Eigen::Vector3d trajLine, Eigen::Vector3d tubeLine, Eigen::Vector2d p );

      static void makeAngleProper(float & angle);

      static float getProperAngle(float angle);

      static float properAngleDiff( float angle1, float angle2 );

      static float evalPointOnLine( Eigen::Vector2d p, Eigen::Vector3d line );

      static Eigen::Vector3d getQPConstraintValues( Eigen::Vector2d p, Eigen::Vector3d line );

      static float getDistToEllipse( float a, float b, Eigen::Vector2d p );


  };



}