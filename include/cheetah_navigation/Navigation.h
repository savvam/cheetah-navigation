/**
 * @file Navigation.h
 * @brief ROS wrapper for the overarching navigation component
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */

#pragma once

//include other cheetah stuff
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/Mapper.h"
#include "cheetah_navigation/StateEstimator.h"
#include "cheetah_navigation/Planner.h"
#include "cheetah_navigation/LCM_comm.h"

// messages
// #include <grid_map_msgs/GridMap.h> 
#include <std_srvs/SetBool.h>
#include <cheetah_msgs/FloatArr.h>

// ros
#include <ros/ros.h>

// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>
#include "grid_map_ros/GridMapRosConverter.hpp"
// #include <grid_map_cv/grid_map_cv.hpp>

// TF
// #include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>


namespace cheetah {

  class Navigation {
  public:
    Navigation(const ros::NodeHandle nh, const ros::NodeHandle nhp);
    ~Navigation() = default;

  private:
    // LCM_comm lcm_comm_;

    // ros variables
    ros::NodeHandle nh_, nhp_;
    ros::ServiceServer srv_resetAll_, srv_velCMD_, srv_arm_, srv_verbose_lcm_;

    //parameters
    Map_Params mParams_;
    Status status_;

    // maps
    // TO DO: WORLD / GLOBAL change
    grid_map::GridMap map_;
    grid_map::GridMap global_map_;
    grid_map::GridMap world_map_;

    ros::Timer lcmHandleTimer_;

    // tf2 variables
    tf2_ros::Buffer tf_buffer_;
    std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr_;

    // navigation variables
    std::unique_ptr<StateEstimator> stateEstimator_;
    std::unique_ptr<Mapper> mapper_;
    std::unique_ptr<Planner> planner_;


    void lcmTimerHandleCallback();
    
    /**
     * @brief      grab the parameters from the param file with the nodehandle 
     */
    void initParams();

    /**
     * @brief      ROS Service: reset paths and maps
     * 
     * @param[in] std_srvs::SetBool::Request &req - boolean true 
     *            std_srvs::SetBool::Response &res - result of the operation
     * 
     * @return  boolean true / false: success
     */
    bool resetAll(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);

    bool arm(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);

    bool sendVelCMD(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res);

    bool verboseLCM(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);

  };

} // cheetah
