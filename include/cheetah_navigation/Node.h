/**
 * @file Node.h
 * @brief RRT Node implementation
 * @author Savva Morozov <savva@mit.edu>
 * @date 20 Jan 2021
 */

#pragma once

// include ros
#include <ros/ros.h>

// include structs and basic libs
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/TrajectorySegment.h"

// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>

#include "cheetah_navigation/Geometry.h"
// #include "grid_map_ros/GridMapRosConverter.hpp"

// alglib related
#include <math.h>
#include "interpolation.h"
#include <stdlib.h>
#include <chrono>
// useful alglib links:
// http://forum.alglib.net/viewtopic.php?f=2&t=60
// https://stackoverflow.com/questions/51855264/undefined-symbols-for-architecture-x86-64-alglibspline2dcalcalglibspline2
// https://www.alglib.net/interpolation/leastsquares.php#header21
// https://www.alglib.net/translator/man/manual.cpp.html#example_lsfit_d_nlfg
// https://github.com/S-Dafarra/alglib-cmake


namespace cheetah {

  class Node {
  public:
    Node();
    ~Node() = default;

    void makeAJump( Node * grampa, Node * dad, TrajectorySegment * segment);

    void makeAWalk( Node * dad);

    static bool robotIsFreeOfObstaclesAt( grid_map::Position position, float orientation );

    static bool squareIsFreeOfObstaclesAt( grid_map::Position position, float orientation );

    static bool checkIfElevationIsNan( grid_map::Position pos );

    bool checkTraversabilityFrom(Node * other, TrajectorySegment * segment); // this is probably a lot more than bool

    bool checkTraversabilityFrom(Node * other, TrajectorySegment * segment, bool nanHandling);

    bool checkWalkabilityFrom(Node * other, bool nanHandling);

    static bool checkWalkabilityBtwn(grid_map::Position pos1, grid_map::Position pos2, bool nanHandling);

    static bool checkWalkabilityBtwn(grid_map::Position pos1, grid_map::Position pos2);

    static bool checkJumpabilityBtwn( grid_map::Position startPos, grid_map::Position finishPos );

    bool checkValidityTo(Node * gramps);

    bool checkWalkabilityFrom(Node * other);

    static bool checkTraversabilityOverLine(grid_map::Position pos1, grid_map::Position pos2);

    static bool checkTraversabilityOverLine(grid_map::Position pos1, grid_map::Position pos2, bool nanHandling);

    static bool checkThatJumpWorks(grid_map::Position jumpCenter, float jumpAngle, float takeoffCluster, float landingCluster);

    static bool checkJumpability(grid_map::Index start, grid_map::Index finish, TrajectorySegment * segment);


    static bool evaluateTerrainAt(Eigen::Vector2d pos, float angle, alglib::real_1d_array c0, alglib::real_1d_array * cresult, bool removeFloorPastLine, bool verbose);
    // static alglib::real_1d_array evaluateTerrainAt(Eigen::Vector2d pos, float angle, alglib::real_1d_array c0, bool removeFloorPastLine, bool verbose);

    static bool checkLineBelongsToClusterIndex(grid_map::Index start, grid_map::Index finish, int cluster);

    static bool checkLineBelongsToClusterIndex(grid_map::Index start, grid_map::Index finish, int cluster, bool nanHandling);

    static bool checkLineBelongsToClusterPosition(grid_map::Position start, grid_map::Position finish, int cluster);

    static bool checkLineBelongsToClusterPosition(grid_map::Position start, grid_map::Position finish, int cluster, bool nanHandling);

    static bool belongsToCluster( grid_map::Index point, int cluster);

    static bool belongsToACluster( grid_map::Index point);

    bool belongsToACluster();
    
    static bool belongToSameCluster( grid_map::Index start, grid_map::Index finish );

    bool belongsToSameClusterAs(Node * other);

    // return 1 if fully traversable to the point, 0 if traversable to some point in between, -1 otherwise.

    void steer( Node * other, float dist);

    float distanceTo(Node * other);

    float distanceTo(grid_map::Position other_pos);

    static float distanceBtwn(grid_map::Position pos1, grid_map::Position pos2);

    bool reindex();

    void setFather(Node * DarthVader);

    void setFatherWithShortCutting(Node * DarthVader);
    
    void reset();

    void setAsOrigin();

    void setAsOrigin(grid_map::Position origin);

    void sampleRandomPoint();

    bool checkIfAcceptable();

    static bool checkIfAcceptable( grid_map::Position pos );

    void setPos(grid_map::Position pos);

    void copyPosFrom( Node * other );



  public:
    grid_map::Position pos_;
    grid_map::Index index_;
    
    Node * father_; //pointer to the father node
    char pathTypeToFather_;

    int ind_;
    static grid_map::GridMap * map_; //
    static Planner_Params * p_;
  };

}