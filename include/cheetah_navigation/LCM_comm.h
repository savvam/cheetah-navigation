/**
 * @file LCM_comm.h
 * @brief ROS wrapper for LCM Communication
 * @author Savva Morozov <savva@mit.edu>
 * @date 12 Feb 2021
 */

#pragma once

//include other cheetah stuff
#include <cstdint>
#include <iostream>
#include <cmath>
#include <mutex>
#include <thread>
#include <Eigen/Dense>
#include <chrono>
#include <vector>
#include <map>
#include <memory>


// this is not pretty, but necessary because gmxx library or whatever is redeclaring lcm
// https://stackoverflow.com/questions/62270003/c-multiple-libraries-define-same-class-name
namespace foo {
  #include <lcm/lcm-cpp.hpp>  
}


#include "vel_cmd.hpp"
#include "grid_map_lcmt.hpp"
#include "basic_jump_info.hpp"
#include "jump3D_info_lcmt.hpp"
#include "jump_callback_lcmt.hpp"
#include "microstrain_lcmt.hpp"
#include "state_estimator_lcmt.hpp"
#include "t265_lcmt.hpp"
#include "qkdif_outputs_lcmt.hpp"

// ros - only for ROS_INFO / ROS_ERROR
#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <grid_map_core/GridMap.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_eigen/tf2_eigen.h>
#include <geometry_msgs/TransformStamped.h>
#include <cheetah_msgs/imu.h>
// #include <cheetah_msgs/t265.h>
#include <cheetah_msgs/state_estimator.h>


namespace cheetah {

  class LCM_handler {
    public:
      LCM_handler();
      ~LCM_handler() = default;

      void jumpFinishedCallback(const foo::lcm::ReceiveBuffer* rbuf,
                            const std::string& chan, 
                            const jump_callback_lcmt* msg);
      
      void jumpComputedCallback(const foo::lcm::ReceiveBuffer* rbuf,
                            const std::string& chan, 
                            const qkdif_outputs_lcmt* msg);

      void imuCallback(const foo::lcm::ReceiveBuffer* rbuf,
                            const std::string& chan, 
                            const microstrain_lcmt* msg);
      
      void rfCallback(const foo::lcm::ReceiveBuffer* rbuf,
                            const std::string& chan, 
                            const state_estimator_lcmt* msg);

    public:
      ros::Publisher pub_jump_callbacks_, pub_state_estimator_, pub_imu_, pub_jump_computed_callbacks_;
  };

  class LCM_comm {

  public:
    static bool sendT265( int t265, unsigned long lcm_published_timestamp, geometry_msgs::TransformStamped odom_to_base);
    static bool sendVelCMD(float v_x, float v_y, float v_psi);
    static void sendMap(grid_map::GridMap * map);
    static void sendMap(grid_map::GridMap * map, float len);
    static void sendBasicJumpInfo(float height_delta, float dist_to_obst);
    static void sendExtendedJumpInfo(float c0, float c1, float c2, float c3, float c4, float yaw);
    static void sendJump3DInfo( float obs_height, float dist_to_obs, float dist_past_obs_lb, float dist_past_obs_ub, float yaw_init, float yaw_term );
    static bool good();
    static void zeroOut();

    // static bool reinit(std::string updm);
    static bool reinit(std::string updm);
    static void setupPublishers( ros::NodeHandle * nh );

    static void arm();
    static void disarm();
    static bool isArmed();
    static void verbose();
    static void deverbose();
    static bool verboseLevel();
    static void handle();
    static bool recIMU_;
    static bool recRF_;

  private:
    // static ros::Publisher pub_jump_callbacks_;
    static LCM_handler handler_;
    static foo::lcm::LCM * lcm_;
    static bool armed_;
    static float maxSpeed_;
    static bool verbose_;
    // static float mapOffset_;
    
  };

} // cheetah
