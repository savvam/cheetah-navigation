/**
 * @file TrajectorySegment.h
 * @brief RRT Trajectory Segment implementation
 * @author Savva Morozov <savva@mit.edu>
 * @date 27 Jan 2021
 */

#pragma once

// include ros
#include <ros/ros.h>

// include structs and basic libs
#include "cheetah_navigation/cheetah_structs.h"


// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>
// #include "grid_map_ros/GridMapRosConverter.hpp"

namespace cheetah {

  class TrajectorySegment {
  public:
    TrajectorySegment();
    TrajectorySegment(grid_map::Position start, grid_map::Position finish, char type);
    void set(grid_map::Position start, grid_map::Position finish, char type);
    bool inited();
    bool isJump();
    bool isWalk();
    void reset();
    void swap();

    char type_;
    bool inited_ = false;
    
    grid_map::Position startPos_, finishPos_;
    
  };
}