/**
 * @file cheetah_structs.h
 * @brief Common cheetah structures used in the stack
 * @author Savva Morozov <savva@mit.edu>
 * @date 10 Jan 2021
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <cmath>
#include <mutex>
#include <thread>
#include <Eigen/Dense>
#include <chrono>
#include <vector>
#include <map>
#include <memory>


namespace cheetah {

  struct Map_Params{
    // maps are inited in nav_ros hence map_params are shared
    bool init_global_map = false;
    bool init_local_map = false;
    bool init_world_map = false;

    std::string map_frame;
    std::string tracking_frame;

    float local_map_x; // map "height"
    float local_map_y; // map "width"
    float local_map_resolution; // resolution of the map

    // TO DO: GLOBAL CHANGE
    float global_map_x; // map "height"
    float global_map_y; // map "width"
    float global_map_resolution; // resolution of the map

    // TO DO: WORLD CHANGE
    float world_map_x; // map "height"
    float world_map_y; // map "width"
    float world_map_resolution; // resolution of the map
    
    float map_publish_period; // publish new map measurements at this rate

    // std::string local_map_type = "var"; // type of the map being recorded
    // std::string global_map_type = "var"; // type of the map being recorded

    bool spit_map = false;
    std::string spit_path;

    bool uAtStart = true;
    bool uAtFail = true;
    float uHeight = -0.26;
    float uSideDist = 0.20;
    float uFrontDist = 0.6;
    float uBackDist = 0.45;

    float elevationOffset = 0.27;
    // float localDefaultHeight = 0.0;


  };

  struct Planner_Params{
      int numNodes = 100;
      float runPeriod = 1;

      float goalRadius = 0.5;    // m
      float steerDistance = 0.5;
      float originSteerDistance = 0.5;
      
      int maxSampleAttempts = 15000;
      std::string layer = "elevation";

      int varianceRadius = 2; // cells
      float max_node_sd = 0.05;
      float max_traversability_delta = 0.05;
      float side_delta = 0.05;
      int num_deltas = 2;

      bool allowNanInRobotsRadius = true;
      float robotsRadius = 0.5;

      bool allowNANs = true;

      float rrtStarRadius = 0.75;
      std::string type = "rrt*";

      // bool use_clusters = true;
      bool nonClusterWalk = false;

      float jump_height_delta = 0.15;
      float jump_distance = 0.4;
      float jump_over_delta = 0.03;

      float walk_front_delta = 0.05;

      float robot_width = 0.4;
      float robot_length = 0.7;

      float jump_sampling_dist_perp = 0.4;
      float jump_sampling_dist_parl = 0.15;
      float jump_sampling_angle = 0.5;

      float jumpTubeRadius = 0.3;
      
      int num_jump_samples = 30;
      
      // trajecotry maintenance
      bool trajectoryMaintenance = false;
      float maxTrajectoryMaintenanceLength = 0.5;
      bool setFatherWithShortCutting = false;

      // displaying
      bool spit_traj = false;
      std::string save_path = "~/Desktop/";

      // trajectory improvement
      

      // trajectory following
      float yawScalingFactor = 0.05;
      float yawTimeConstant = 0.03;
      float lateralScalingFactor = 2.0;
      float forwardScalingFactor = 0.3;
      float atDistToNext = 0.25;
      float nextScalingFactor = 0.3;
      float lateralDeadBand = 0.05;
      float proportionalGain = 0.3;
      float proportionalDeadBand = 0.05;

      bool singleTrajectoryMode = false;
      bool followerModeOn = false;

      float followerPeriod = 0.01;


      bool verboseFollower = false;
      bool verboseImprovement = false;
      bool verboseTrajectory = false;
      bool verboseFollowerEssential = false;
      float p1 = 1.0;
      float p2 = 0.5;
      float p3 = 0.5;
      float p4 = 0.5;
      float p5 = 0.5;
      float countUpRad = 0.3;
      float minCountVal = 0.1;
      float maxCountTimes = 2.0;
      float sd = 2;
      
      float maxV1 = 0.2;
      float maxV2 = 0.3;
      float maxV3 = 0.4;
      float maxV4 = 0.5;

      bool simplifiedTraversabilityCheck = true;


      float minEdgeLength = 0.15;
      float maxEdgeLength = 1.00;
      float singleEdgeLimit = 0.33;
      float doubleEdgeLimit = 0.66;
      float tripleEdgeLimit = 1.00;

      int numRRTattempts = 3;
      float singleTrajTimeout = 0.05; // 50ms timeout to acquire 1 trajectory, should prolly be less

      float originRad = 0.2; // radius of the trajectory when sampled from the origin
      float commitIncrement = 1.2;
      float maxTubeSize = 0.5;

      float rotAngle = 0.0;

      float jumpStopDistRadius = 0.05;
      float jumpStopAngleRadius = 0.1;
      bool allowJumpOnSameSurface = true;

      bool usePDminSpeed = false;
      float minPDSpeed = 0.13;
      float pdDistSwitch = 0.15;

      float jump_wait_before = 0.5; // wait for this amount of time after stopping and before sending the jump command
      float jump_wait_after = 0.5; // wait for this amount of time after stopping and before sending the jump command
      bool stop_map_recording_upon_follower = false;

      float jump_max_rmse = 0.05;

      float yawIntegratorGain = 0.01;
      float integratorGain = 0.01;

      float terrain_param_sideYDist = 0.45;
      float terrain_param_xFrontExtra = 0.5;
      float terrain_param_c = 65;
      float terrain_param_resolution = 0.025;
      float terrain_param_maxDiff = 0.05;
      float terrain_param_landingWeight = 2.0;

      bool RRTCONNECT = false;

      float jump_num_samples_resolution = 0.05;
      float evalTerrain_x = 1.2;
      float evalTerrain_y = 1.2;

      float minDistToObstacle = 0.15;
      float minDistFromObstacle = 0.15;
      float acceptableRad = 0.15;

      // float angleResolution = 15; // degrees
      float angleRange;
      int numAngles;

      float minTubeRad;

      float maxDistFromObstacle = 0.7;
      float rangeDistResolution = 0.05;

      bool doTwoJumpsChecks = false; // temporary, remove

      std::string tablePath;

      float angleToEdges = M_PI/2;

      float distBackOffMultiplier = 1.0;
      float constraintReduction = 0.05;

      float yawVSDistScalingFactor = 1.0;
      float alphaShapeRadius = 0.5;

      float robustnessMetricTieRad = 0.1;

      bool sendJumpExtendedRequest = true;

      bool reset_integrators = false;
      bool errorTrackingTesting = false;
      float trackingTimeout = 1.0;
      float scaleForward = 0.8;

      float integratorXmax = 1.0;
      float integratorYmax = 1.0;
      float integratorYawMax = 1.0;

      bool quiet = false;
      bool quiet_rrt = false;
      bool log_inside = false;
      
    };


  struct Status{
    bool update_global_map = true; // udpate global map with pointcloud
    bool record_path = false; // record path of the t265 cameras
    bool record_robot_path = false;
    bool vicon_inited = false;
    bool vicon_test_calib = false;
    bool record_pose = true;
    bool record_local_map = false; // publish the local map
    bool record_global_map = false; // publish the global map
    bool record_world_map = false; // publish the world map
    bool publish_filtered_pc = false; // publish the filtered pointcloud

  };

}