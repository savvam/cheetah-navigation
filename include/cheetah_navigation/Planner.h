/**
 * @file Planner.h
 * @brief ROS wrapper for planning (RRT)
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */

#pragma once

// include other cool stuff
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/Node.h"
#include "cheetah_navigation/Geometry.h"
#include "cheetah_navigation/LCM_comm.h"

// loggin maps
#include <iostream>
#include <fstream>

#include <boost/algorithm/string.hpp>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>


// include ros
#include <ros/ros.h>

// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>
#include "grid_map_ros/GridMapRosConverter.hpp"
#include <grid_map_cv/grid_map_cv.hpp>

// include messages
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <visualization_msgs/Marker.h>
#include <interactive_markers/interactive_marker_server.h>
#include <std_srvs/SetBool.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Bool.h>
#include <cheetah_msgs/FloatArr.h>
#include <cheetah_msgs/srv_float.h>

#include <cheetah_msgs/constrainedFeasiblePolygon.h>
#include <cheetah_msgs/constrainedPolygon.h>
#include <cheetah_msgs/deltaTime.h>
#include <cheetah_msgs/robotPose.h>
#include <cheetah_msgs/event.h>
#include <cheetah_msgs/map.h>
#include <cheetah_msgs/robotXYTrajectory.h>

// TF
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>


// opencv
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/eigen.hpp>

// alglib related
#include <math.h>
#include "interpolation.h"
#include <stdlib.h>
#include <chrono>
#include <tuple>

// cgal related
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
// #include <CGAL/Simple_cartesian.h> // DON'T USE THIS - results in namespace collisions
#include <CGAL/Alpha_shape_2.h>
#include <CGAL/Alpha_shape_vertex_base_2.h>
#include <CGAL/Alpha_shape_face_base_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/algorithm.h>
#include <CGAL/assertions.h>

typedef CGAL::Simple_cartesian<double> K;
typedef K::FT                                                FT;
typedef K::Point_2                                           Point_2;
typedef K::Segment_2                                         Segment_2;
typedef CGAL::Alpha_shape_vertex_base_2<K>                   Vb;
typedef CGAL::Alpha_shape_face_base_2<K>                     Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb>          Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds>                Triangulation_2;
typedef CGAL::Alpha_shape_2<Triangulation_2>                 Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator            Alpha_shape_edges_iterator;

// mapbox include
#include <mapbox/polylabel.hpp>



namespace cheetah {

  class Planner {

  public:
    Planner(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, grid_map::GridMap * global_map, tf2_ros::Buffer *tf_buffer);
    ~Planner() = default;

    struct SVM_params{
      // int N = 4; // number of SVMs
      // int numVectors = 1900;
      // std::vector<float> Heights; // N elements height per item
      // std::vector< std::vector<float> > Alpha;  // learnt alpha parameters
      // std::vector< std::vector<int> > SVLabels; // supper vector labels
      // std::vector< std::vector< Eigen::Matrix<float, 1, 4> > > SV; // support vectors
      // std::vector<float> Scale; // scaling parameters
      // std::vector<float> Bias; // learnt bias

      // single svm
      std::vector<float> Alpha;  // learnt alpha parameters
      std::vector<int> SVLabels; // supper vector labels
      std::vector< Eigen::Matrix<float, 1, 5> > SV; // support vectors
      float Scale; // scaling parameters
      float Bias; // learnt bias
    };

    struct LPF{
      float x = 0.1;
      float y = 0.1;
      float yaw = 0.1;
    };


  private:
    SVM_params svm_;
    LPF lpf_;
    std::mutex trajectoryAccess_;
    bool incrementRequestSent_ = false;
    bool plannerHasControlOfTheCheetah_ = true;
    // ros variables
    ros::NodeHandle nh_, nhp_;
    ros::Publisher pub_trajectory_, pub_improved_trajectory_, pub_committed_trajectory_, goal_pub_, debug_marker_pub_;
    ros::Publisher pub_event_, pub_cmd_;

    ros::Publisher pub_left_tube_, pub_right_tube_;
    ros::Publisher pub_traj_increment_, pub_left_increment_, pub_right_increment_;
    ros::Timer plannerTimer_, followerTimer_;
    ros::Subscriber goal_sub_; // subscribe to the goal from rviz
    ros::Subscriber jump_callback_sub_, jump_computed_sub_;
    ros::Subscriber user_says_go_sub_;
    bool user_says_go_ = true;

    ros::ServiceServer srv_trajFollowing_, srv_floatArrSrv_, srv_get_terrain_;
    ros::Publisher pub_increment_req_;
    ros::Subscriber sub_increment_req_, sub_commit_maintenance_req_;

    // logging
    ros::Publisher pub_time_deltas_, pub_cp_, pub_cfp_, pub_robot_pose_, pub_map_, pub_trajs_;

    // parameters
    Planner_Params p_;
    std::vector< std::vector<float> > jumpTable_;
    Eigen::Vector2d jumpAngles_;
    float distPastObst_, distToObst_, height_;
    bool finished_traj_ = false;

    //tf2 
    tf2_ros::Buffer * tf_buffer_; // tf buffer is shared
    

    // trajectory-following task resolution
    // 'w' - walking, whether with

    char task_state_ = 'w';
    int task_segment_ = 0;
    Eigen::Vector2d xy_integrator_ = Eigen::Vector2d(0,0);
    float phi_integrator_ = 0;
    ros::Time PDTrackingStart_;



    // shared pointers
    Status * status_;
    grid_map::GridMap * global_map_;

    // nodes yay
    std::vector<Node> nodes_; // it's a vector only because i get its size as the param; it will be alloced

    std::vector<Node> tree1_; // it's a vector only because i get its size as the param; it will be alloced
    std::vector<Node> tree2_; // it's a vector only because i get its size as the param; it will be alloced
    
    Eigen::Vector3d last_cmd_ = Eigen::Vector3d(0,0,0);

    float max_v_delta_, max_yaw_delta_;

    // nodeTrajectory: last to first
    std::vector<Eigen::Vector2d> trajectory_, leftTube_, rightTube_;
    std::vector<char> nextEdgeType_;

    std::vector<Eigen::Vector3d> trajLines_, leftLines_, rightLines_;
    int committed_;

    int trajectoryLength_ = 0;

    grid_map::Position goal_;

    Eigen::Vector3d getCommandFromTask(Eigen::Vector2d pos, bool right, float yaw);

    std::tuple<bool, Eigen::Vector2d, float> getRobotPose();

    void publishTheMap();

    void publishDeltaTime(std::string name, float deltaTimeSecs);

    void publishConstraintPolygon( float jumpDirAngle, bool takeoff, std::vector<bool> acceptedInitial, std::vector<float> initialAngles, std::vector<Eigen::Vector2d> initialRanges);

    void publishRobotTraj(std::vector<Eigen::Vector2d> traj, std::string name);

    void evaluateTerrainAroundRobot(float angle);

    void publishEvent(std::string name, int num);

    void publishRobotPose(std::string what, Eigen::Vector2d pos, float yaw);

    void fixCMD(Eigen::Vector3d cmd, bool needFixing);

    void userGoCallback(std_msgs::Bool msg);

    void jumpRequestExtended(int jumpSegment);

    void callbackFromJump(std_msgs::Empty msg);

    void callbackFromJumpComputed(std_msgs::Empty msg);

    void commitMaintenanceCallback( std_msgs::Empty msg );

    void commitMaintenance();

    bool sampleAnRRTConnectTrajectory(const grid_map::Position origin, std::vector<grid_map::Position> * trajectory, std::vector<char> * types, float * trajCost);

    void debugBothTrees(bool firstTree, int i1, int i2);

    bool findNearestNeighbor( std::vector<Node> * tree, Node * node, int upToMaxIndex, int * nnIndex );

    float populateVectorTrajectory( Node * node, std::vector<grid_map::Position> * trajectory, std::vector<char> * types );

    void resetTreesUpToK(int i1, int i2);

    void incrementCommittedTrajectoryCallback( std_msgs::Empty msg);

    bool incrementCommittedTrajectory( bool firstTime );

    void findTubeAroundTrajectory(std::vector<grid_map::Position> * trajectory, 
                                       std::vector<char> * types,
                                       std::vector<grid_map::Position> * leftTube,
                                       std::vector<grid_map::Position> * rightTube,
                                       bool useOriginTube);
    
    void addExtraVectorsIntoTrajectory( std::vector<grid_map::Position> * trajectory, std::vector<char> * types );

  
    // new ^^
    //-------------------------

    void initParams();

    void publishTrajectory(ros::Publisher * publisher, std::vector<Eigen::Vector2d> * trajectory, int trajLen );

    void getGradients();

    // void improveTrajectoryOnce(float multiplier, std::vector<Eigen::Vector2d> * trajectory);
    void improveTrajectoryOnce(float multiplier, std::vector<Eigen::Vector2d> * trajectory, std::vector<char> * types);

    void getTubeLines();

    void scaleVelocity(Eigen::Vector2d * v, float tubeRad);

    float maxSpeedFromTube(float tubeRad);

    Eigen::Vector3d getDesiredCMD(bool * needFixing);

    bool follower(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);

    bool getTerrain(cheetah_msgs::srv_float::Request &req, cheetah_msgs::srv_float::Response &res);

    void trajectoryFollowing();

    void publishGoalMarker();

    void publishDebugMarker( Eigen::Vector2d position );

    void updateGoalPosition(const geometry_msgs::PoseStamped &msg);

    bool floatArrService(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res);

    void shortcut(std::vector<grid_map::Position> * trajectory, std::vector<char> * types);

    std::vector< std::vector<float> > read_csv(std::string fileName, int rowLength);

    bool findBestJump( Eigen::Vector2d * resAngles, float * distPastObst );
  
    float rbfKernel( Eigen::Matrix<float, 1, 5> p1, Eigen::Matrix<float, 1, 5> p2 );

    void grabSVMData();


    bool findPolygon( float jumpDirAngle, float prevDirAngle, float yawTerm, float distTerm, float height, std::vector<bool> acceptedInitial, std::vector<float> initialAngles, std::vector<Eigen::Vector2d> initialRanges, mapbox::geometry::polygon<double> * polygon );

    bool evaluateSVM( float distInit, float yawInit, float distTerm, float yawTerm, float height, bool verbose );

    bool getRobustnessMetric( Eigen::Matrix<float, 1, 5> ellipseSpec, Eigen::Matrix<float, 1, 4> con, Eigen::Vector2d * solution, float * metric );

    void printOutEllipsePoint( Eigen::Matrix<float, 1, 5> ellipseSpec, Eigen::Matrix<float, 1, 4> con, float metric  );

    bool findMatchingTableSpec( float initAngle, float finalAngle, float height, Eigen::Matrix<float, 1, 5> * ellipseSpec );


    void trajectoryFollowingTest(float walkDistance, float des_orientation);

  };
}