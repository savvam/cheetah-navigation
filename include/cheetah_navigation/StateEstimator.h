/**
 * @file StateEstimator.h
 * @brief ROS wrapper for state estimation, odometry, and path components
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */

#pragma once

// include cheetah structures
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/LCM_comm.h"

// include ros
#include <ros/ros.h>

// include ros messages
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_srvs/SetBool.h>
#include <cheetah_msgs/t265.h>


// include TF
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>

namespace cheetah {

  class StateEstimator {

  public:
    StateEstimator(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, tf2_ros::Buffer *tf_buffer);
    ~StateEstimator() = default;

    struct State_Estimator_Params{
      int num_odom_sources; 
      int use_single_odom = 0; // 0: use multisource odometry, 1 - num_odom_sources: use specific odometry source
      std::vector<std::string> odom_frames; // these are actually child frames
      std::map<std::string, std::string> odom_topics; // topic names
      std::map<std::string, std::string> odom_links; // base links

      std::string vicon_pose_topic;
      std::string cheetah_frame;
    };

    struct Path_Params{
      float path_increment_period; // add new path measurements at this rate, publish at that rate
      float path_publish_period; // add new path measurements at this rate, publish at that rate
    };

    /**
     * @brief      Initialize the paths (by resetting the array structures)
     * 
     */
    void initPaths();
    void testCalibration();
    void calibrateWorldToMap();
    bool need_calibration_ = false;

  private:
    ros::NodeHandle nh_, nhp_; // ros handles
    std::map<std::string, ros::Subscriber> sub_odom_;
    std::map<std::string, ros::Publisher> pub_path_; 
    ros::Publisher pub_odom_; 
    ros::Subscriber sub_world_;
    
    geometry_msgs::TransformStamped world_to_map_;
    nav_msgs::Path estimatedPath_, viconPath_;
    ros::Publisher pubEstimatedPath_, pubViconPath_;
    ros::Timer recEstimatedTimer_, recViconTimer_, pubEstimatedTimer_, pubViconTimer_; 
  


    ros::Timer pathTimer_, recOdomTimer_, tim_tf_broadcast_; 
    ros::ServiceServer srv_recordPath_, srv_resetPath_, srv_calibrate_; 

    bool recT265_ = false;
    ros::Publisher pub_t265_1632_;

    std::map<std::string, ros::Time> timeOfLastPathPose_; // time that paths were last published
    ros::Time timeOfLastOdom_;
    std::map<std::string, nav_msgs::Path> path_; // paths

    Path_Params pParams_; // path specific parameters
    State_Estimator_Params seParams_; // state estimator params

    tf2_ros::TransformBroadcaster tf_broadcaster_; // tf broadcaster to publish odometry

    // the following variables are shared between all files hence pointers
    Status * status_; // potentially changing status variables are shared
    tf2_ros::Buffer * tf_buffer_; // tf buffer is shared

    /**
     * @brief      ROS Callback to the odometry message; adds the message to the overall path.
     *
     * @param[in]  nav_msg::odometry message
     */
    void odomCallback(const nav_msgs::Odometry& msg);

    void recordOdom();

    void timerTFworld();


    void publishEstimatedPath();
    void recordEstimatedPath();
    void publishViconPath();
    void recordViconPath();

    bool calibrateOdomToWorld(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);
    void viconPoseCallback(const geometry_msgs::PoseStamped& msg);



    // TO DO: that's multisource odometry, spend time and thought to implement
    /**
     * @brief      calculate combined odometry from possibly multiple incoming odometry sources;
     *             publish a resultant tf2 between odom and base link.
     *
     * @param[in]  geometry_msgs::TransformStamped message
     */
    void calculateOdom(const geometry_msgs::TransformStamped& msg);



    /**
     * @brief      Publish the current paths to a topic
     */
    void publishPath();



    /**
     * @brief      ROS Service: begin / stop recording paths
     * 
     * @param[in] std_srvs::SetBool::Request &req - boolean true or false
     * 
     * @param[in] std_srvs::SetBool::Response &res - result of the operation
     * 
     * @return  boolean true / false: success
     */
    bool recordPath(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);


    /**
     * @brief      ROS Service: reset recorded path
     * 
     * @param[in] std_srvs::SetBool::Request &req - boolean true
     * 
     * @param[in] std_srvs::SetBool::Response &res - result of the operation
     * 
     * @return  boolean true / false: success
     */
    bool resetPath(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res); 


    /**
     * @brief       Grab parameters using the handlers
     */
    void initParams();
      
  };
}