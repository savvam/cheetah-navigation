/**
 * @file Mapper.h
 * @brief ROS wrapper for state estimation, odometry, and path components
 * @author Savva Morozov <savva@mit.edu>
 * @date 19 Jan 2021
 */

#pragma once

// include cheetah structures
#include "cheetah_navigation/cheetah_structs.h"
#include "cheetah_navigation/Geometry.h"
#include "cheetah_navigation/LCM_comm.h"

// #include "cheetah_navigation/Sensor.hpp"
// #include "cheetah_navigation/pcManager.h"

// include ros
#include <ros/ros.h>

// include messages
#include <sensor_msgs/PointCloud2.h>
#include <grid_map_msgs/GridMap.h> 
// #include <std_srvs/SetBool.h>
// #include <std_msgs/Int32.h>
#include <std_msgs/Empty.h>
#include <cheetah_msgs/FloatArr.h>

// grid map stuff
#include <grid_map_ros/grid_map_ros.hpp> 
#include <grid_map_core/GridMap.hpp>
#include "grid_map_ros/GridMapRosConverter.hpp"
#include <grid_map_cv/grid_map_cv.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <pcl/pcl_base.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/statistical_outlier_removal.h>
// #include <pcl/filters/bilateral.h>
#include <pcl/filters/fast_bilateral.h>

// TF
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>

#include <chrono>
#include <boost/thread.hpp>

//opencv
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/eigen.hpp>




namespace cheetah {

  class Mapper {

  public:
    Mapper(const ros::NodeHandle nh, const ros::NodeHandle nhp, Status *status, tf2_ros::Buffer *tf_buffer, grid_map::GridMap * map, grid_map::GridMap * global_map, grid_map::GridMap * world_map);
    ~Mapper() = default;

    /**
     * @brief      Set nan to every value in the map - reset the map
     */
    void clearTheMap();


    // mapper specific structures

    struct Filter_Params{
      float height_threshold;
      float neighb_delta_height = 0.05;
      float local_neighb_delta_height = 0.02;
    };

    struct Mapping_Parameters{
      int N = 30;
      // temporal params
      float min_t = 0.1;
      float max_t = 2.0;
      float t_inc = 0.3;
      float t_dec = 0.1;
      std::string dec_type = "subtract";

      float start_alt_var_thresh = 0.2;
      float switch_to_alt_var_thresh = 0.1;
      float min_var = 0.0001;

      int nbhd = 1;

      int timestamp_delta = 50000000;

      float cluster_sd_max = 0.03;
      float cluster_height_delta_max = 0.05;

      int minimum_cluster_size = 50;
      int minimum_cluster_neighbors = 9;//including yourself

      float max_height = 2.0; // TO DO: this is sketch as of now


      int local_mapping_type = 1;
      float local_filter_thresh = 0.05;
      float filter_use_old = 0.7;
      float send_local_size = 2;
      int downNumPoints = 4000;
      bool dumbDownsample = false;
      bool filterLocalPC = true;

      int localFilterType = 1;
      int localFilterParam = 2;
    };


    struct Pointcloud_Params{

      int local_pc_insert_from = 1;
      int num_pc_sources;
      std::vector<std::string> pc_frames;
      std::map<std::string, std::string> pc_topics;

      // std::map<std::string, bool> ph1_gotPc;
      // std::map<std::string, bool> ph2_gotPc;
      // pcl::PCLPointCloud2 ph1, ph2; // place holders for pointclouds
      // int ph1Counter = 0;
      // int ph2Counter = 0;
      // bool ph1Filtered = false;
      // bool ph2Filtered = false;

      // int lProc = 0; /// which placeholder is being processed by local / global maps
      // int gProc = 0;


      float min_variance = 0.0004; // default corresponds to sd of 0.02m = 2cm

      bool voxel_grid_filter = true;
      float voxelGridFilterSize;
      bool applyVoxelGridFilter;

      bool applyOutlierRemovalFilter = true;
      int outlierMeanK = 50;
      float outlierStddevMulThresh = 1.5;

      float box_minX;
      float box_minY;
      float box_minZ;
      float box_maxX;
      float box_maxY;
      float box_maxZ;
      bool appplyBoxFilter = false;
      float maxTimeDeltaBtwnPC = 0.020;

      float bilateralHalfSize = 1.0;
      float bilateralStdDev = 0.2;
      int applyBilateralFilter = 0;
      int sub_local_pc_ = 1;

      int collectBeforeClustering = 3;

      bool verbosePC = false;
    };

  private:
    // ros variables
    ros::NodeHandle nh_, nhp_;
    std::map<std::string, ros::Subscriber> sub_pc_local_;
    std::map<std::string, ros::Subscriber> sub_pc_global_;
    std::map<std::string, ros::Publisher> pub_pc_local_;
    ros::Publisher pub_cluster_global_;
    ros::Subscriber sub_cluster_global_;
    ros::Publisher pub_commit_maintenance_req_;



    std::mutex d435_mutex_;
    std::mutex d455_mutex_;
    int d435_count_ = 0;
    int d455_count_ = 0;
    int d_count_ = 0;

    // Sensor d435_;

    // ros::Subscriber sub_local_pc_;
    ros::Publisher pub_gridmap_, pub_global_gridmap_, pub_world_gridmap_, pub_filtered_pc_;
    // ros::Publisher filtered_pc_ready_;

    // ros::Subscriber sub_filtered_pc_;

    // ros::Subscriber sub_localInsertion_, sub_globalInsertion_;
    // ros::Publisher pub_localInsertion_, pub_globalInsertion_;

    ros::ServiceServer srv_changeOffset_;
    ros::Timer mapTimer_;
    int cluster_count_ = 1;
    int cluster_size_ = 0;

    // parameters
    Map_Params mParams_;
    Pointcloud_Params pcParams_;
    Filter_Params fParams_;
    Mapping_Parameters m_;
    // pcManager pcManager_;

    // shared pointers
    Status * status_;
    tf2_ros::Buffer * tf_buffer_;
    grid_map::GridMap * map_;
    grid_map::GridMap * global_map_;
    grid_map::GridMap * world_map_;
    std::vector<grid_map::BufferRegion> globalNewRegions_;

    bool originalUnderneathSet_ = false;

    // std::mutex globalMapMutex_;
    // ros::Time lastTimeMoved_

    


    

    // pcl::PCLPointCloud2 local_pc_;
    // std::chrono::time_point<std::chrono::system_clock> start;
    // std::chrono::time_point<std::chrono::system_clock> end;

    // std::chrono::time_point<std::chrono::system_clock> startPub;
    // std::chrono::time_point<std::chrono::system_clock> endPub;
    // std::chrono::duration<double> diffPub;
    // std::chrono::duration<double> diff;
    // int timeCount_ = 0;
    // int pubCount_ = 0;
    // float publishing_ = 0.0;
    // float copying_ = 0.0;

    // int map_spit_count_ = 0;

    float avFiltering = 0.0;
    int nCount = 0;
    float avInserting = 0.0;


    bool changeOffset(cheetah_msgs::FloatArr::Request &req, cheetah_msgs::FloatArr::Response &res);

    void localPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    void globalFilteredPcCallback( const sensor_msgs::PointCloud2& msg );

    void globalUnfilteredPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    void globalClusterCallback(std_msgs::Empty msg);
    
    void setUnderneathTerrain();

    void filterLocalMap();

    void traversabilityOverLocalMap();

    std::string type2str(int type);


    // void pcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    // void localPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    // void globalPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    // void localInsertionCallback(std_msgs::Int32 msg);

    // void globalInsertionCallback(std_msgs::Int32 msg);

    // bool performPointcloudFiltering(int placeholder, std::string from);

    // bool performLocalInsertion(int placeholder);

    // bool performGlobalInsertion(int placeholder);



    void clusterGlobalMap();

    bool expandClusterPoint(grid_map::Index index);

    void removeCluster(grid_map::Index index);
    
    /**
     * @brief      Callback to the pointcloud topic. 
     *
     * @param[in]  pointcloud message
     */
    void oldPcCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    // void subFilteredPC(const sensor_msgs::PointCloud2 msg);
    void subFilteredPC(const sensor_msgs::PointCloud2ConstPtr& msg);


    /**
     * @brief      Publish the current maps to a topic
     */
    void publishMap();

    void spitTheMap();


    /**
     * @brief      transform the pointcloud from one frame to another
     *
     * @param[in]  pointCloud - pcl pointer to the original pointcloud
     *             pointCloudTransformed - pcl pointer to the rotated pointcloud
     *             targetFrame - frame to which to transform the pointcloud (odom)
     * 
     * @return  boolean true / false: true is successful, false if transformation for the pointcloud doesn't exist yet
     */
    bool transformPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud,
                             pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudTransformed,
                             const std::string& targetFrame);

    bool transformPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud,
                                  const std::string& targetFrame);


    /**
     * @brief      Insert the pointcloud into the elevation map
     *
     * @param[in]  pointCloud - pointer to some pcl pointcoud
     */
    void insertPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud, grid_map::GridMap * map, std::string mapName);

    void insertPointCloudLocal(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointCloud, grid_map::GridMap * map, std::string mapName);

    /**
     * @brief      zero out every value in the elevation map
     */
    void zeroTheMap();


    /**
     * @brief      filter the point cloud by removing nans and by using a voxel grid filter
     *
     * @param[in]  pointCloud - pointer to the pointcloud to be filtered; this point cloud would be editted.
     */
    void filterPointCloud(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud, int mapType);


    /**
     * @brief      Filter the Global Map after isnerting the pointcloud
     */
    void filterGlobalMap();


    /**
     * @brief      get 2d displacement of the map based on current robot's location at a particular timestamp 
     *             and insert it into the position vector
     * 
     * @param[in]  timeStamp - timestamp for the discplacement of interest (for tf)
     *             position - pointer to the 2d position vector into which the position is to be inserted
     * 
     * @return  boolean true / false: true is successful, false if displacement at that timestamp doesn't exist
     */
    bool getDisplacement(ros::Time timeStamp, grid_map::Position *position);


    /**
     * @brief      ROS Service: begin / stop recording local map
     * 
     * @param[in] std_srvs::SetBool::Request &req - boolean true or false
     *            std_srvs::SetBool::Response &res - result of the operation
     * 
     * @return  boolean true / false: success
     */
    // bool recordLocalMap(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);
    

    /**
     * @brief      ROS Service: begin / stop recording global map
     * 
     * @param[in] std_srvs::SetBool::Request &req - boolean true or false
     *            std_srvs::SetBool::Response &res - result of the operation
     * 
     * @return  boolean true / false: success
     */
    // bool recordGlobalMap(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);


    /**
     * @brief      grab the parameters from the param file with the nodehandle 
     */
    void initParams();


  };

} // cheetah ns